package com.iillyyaa2033.ucc;

import com.iillyyaa2033.ucserver.ExportRequest;
import com.iillyyaa2033.ucserver.IServerListener;
import com.iillyyaa2033.ucserver.LocalServer;
import com.iillyyaa2033.ucserver.Module;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ucc.discord.DiscordModule;
import ucc.vk.VkModule;

import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

    static int port = 3010;
    static LocalServer localServer;
    static WSServer server;

    public static void main(String... args) throws IOException, InterruptedException, SQLException {

        if(args.length == 1){
            port = Integer.parseInt(args[0]);
        }

        ArrayList<Module> modules = new ArrayList<>();
        modules.add(new DiscordModule());
        modules.add(new VkModule());

        localServer = new LocalServer(bindServerListener(), modules);

        server = new WSServer(port);
        bindWebsocket();
        server.start();

        System.out.println();
        System.out.println("Starting server. Type anything to stop app");

        Scanner sc = new Scanner(System.in);
        sc.next();

        System.out.println("Stopping...\n");
        server.stop();
        System.out.println("Stopped.");
        System.exit(1);
    }

    /**
     * Здесь реализуем интерфейс IServerListener.
     *
     * Все его события будут отправляться json'ом напрямую клиентам.
     *
     * @return implemented interface
     */
    public static IServerListener bindServerListener(){
        return (IServerListener) java.lang.reflect.Proxy.newProxyInstance(
                IServerListener.class.getClassLoader(),
                new Class[]{IServerListener.class},
                new InvocationHandler() {

            @Override
            public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
                JSONObject json = new JSONObject();
                json.put("m", method.getName());

                JSONArray args = new JSONArray();
                for(Object object : objects){
                    args.put(object);
                }
                json.put("a", args);

                server.sendAll(json.toString());
                return null;
            }
        });
    }

    /**
     * Здесь мы проксируем запросы клиента в LocalServer
     */
    public static void bindWebsocket(){

        server.setListener(new WSServer.Listener() {

            @Override
            public void processMessage(String message) {
                try {
                    JSONArray json = new JSONArray(message);

                    String method = json.getString(0);
                    Object[] args = new Object[json.length() - 1];

                    for(int i = 1; i < json.length(); i++){
                        args[i-1] = convert(json.get(i));
                    }

                    callMethodOnServer(method, args);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Object convert(Object o) throws JSONException {

                if(o instanceof JSONObject){
                    HashMap map = new HashMap();

                    for(String key : ((JSONObject) o).keySet()){
                        map.put(key, ((JSONObject)o).get(key));
                    }

                    return map;
                } else if (o instanceof JSONArray){
                    ArrayList list = new ArrayList();

                    for(int i = 0; i < ((JSONArray) o).length(); i++) {
                        list.add(convert( ((JSONArray) o).get(i) ));
                    }

                    return list.toArray(new Object[0]);
                } else {
                    return o;
                }

            }

            void callMethodOnServer(String method, Object... args){
                Method[] methods = LocalServer.class.getDeclaredMethods();

                for(Object o : args){
                    System.out.println("\t"+o.getClass());
                }

                for(Method m : methods){
                    if(m.isAnnotationPresent(ExportRequest.class) && m.getName().equals(method)){

                        try {
                            m.invoke(localServer, args);
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }
}

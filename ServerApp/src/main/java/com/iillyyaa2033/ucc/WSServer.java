package com.iillyyaa2033.ucc;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.util.ArrayList;

public class WSServer extends WebSocketServer  {

    private ArrayList<WebSocket> sockets = new ArrayList<>();
    private Listener l;

    public WSServer(int port){
        super(new InetSocketAddress(port));
    }

    public void setListener(Listener l){
        this.l = l;
    }

    public void sendAll(String s){
        for(WebSocket socket : sockets){
            socket.send(s);
        }
    }

    // ======== //
    // LISTENER //
    // ======== //
    @Override
    public void onStart() {

    }

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        sockets.add(webSocket);
        System.out.println("Added new socket");
    }

    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        // TODO: possible error
        sockets.remove(webSocket);
        System.out.println("Removed one socket");
    }

    @Override
    public void onMessage(WebSocket webSocket, String s) {

        if(s != null){
            System.out.println("Got message "+s+"");
            if(l != null) l.processMessage(s);
        } else {
            System.out.println("Got null message");
        }
    }

    @Override
    public void onError(WebSocket webSocket, Exception e) {
        System.out.println("# Error");
        e.printStackTrace();
    }

    interface Listener {
        void processMessage(String message);
    }
}

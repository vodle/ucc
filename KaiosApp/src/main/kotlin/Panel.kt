import org.w3c.dom.HTMLElement
import org.w3c.dom.events.KeyboardEvent
import kotlin.browser.document

abstract class Panel(val rootName : String) {

    var root = null as HTMLElement?

    // Lifecycle
    open fun onload() {
        root = document.getElementById(rootName) as HTMLElement
    }

    open fun show() {
        if(root == null) throw Exception("'root' is null. Forgot to call onload()?")
        mainframe!!.appendChild(root!!)
    }

    open fun hide() {
        if(root == null) throw Exception("'root' is null. Forgot to call onload()?")
        mainframe!!.removeChild(root!!)
    }

    // Parameters
    open fun getTitle() : String? {
        return "Title"
    }

    open fun getKeyLeft() : String? {
        return "LKey"
    }

    open fun getKeyRight() : String? {
        return "RKey"
    }

    // Events
    abstract fun handleInput(event : KeyboardEvent)
}
import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.events.KeyboardEvent
import kotlin.browser.document
import kotlin.browser.window

class PanelAddAccount : Panel("panel_add_account"){

    var isAwaitingResponse = false

    override fun show() {
        super.show()

        val field = document.getElementById("panel_add_account_service") as HTMLElement
        field.focus()
    }

    override fun getTitle(): String {
        return if(isAwaitingResponse) "Awaiting response" else "Add account"
    }

    override fun getKeyLeft(): String? {
        return if(isAwaitingResponse) null else "Cancel"
    }

    override fun getKeyRight(): String? {
        return if(isAwaitingResponse) null else "Accept"
    }

    override fun handleInput(event: KeyboardEvent) {
        if(isAwaitingResponse) return

        when(event.key){
            "Control", "SoftLeft" -> { showMain() }
            "Insert", "SoftRight" -> { submitCredentials() }
        }
    }

    fun submitCredentials(){

        val service = document.getElementById("panel_add_account_service") as HTMLInputElement
        val token = document.getElementById("panel_add_account_token") as HTMLInputElement

        var clazz = ""
        val hardcodedCredentialsMap: Map<String, String>

        when(service.value.toLowerCase()) {
            "dc" -> {
                clazz = "ucc.discord.DiscordAdapter"
                hardcodedCredentialsMap = mapOf("header" to token.value)
            }
            "vk" -> {
                clazz = "ucc.vk.VkAdapter"
                hardcodedCredentialsMap = mapOf("url" to "#access_token=${token.value}&")
            }
            else -> {
                window.alert("Wrong service type.")
                return
            }
        }

        isAwaitingResponse = true
        invalidateNav()
        root!!.innerHTML = "<p>Request is sent. Please wait</p>"

        websocket!!.request("finishAuthProc", clazz, hardcodedCredentialsMap)
    }

}
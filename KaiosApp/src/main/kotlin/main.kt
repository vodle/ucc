import org.w3c.dom.HTMLElement
import org.w3c.dom.events.EventListener
import org.w3c.dom.events.KeyboardEvent
import org.w3c.dom.get
import kotlin.browser.document
import kotlin.browser.window

const val DEFAULT_SERVER = "ws://localhost:3009"

var server = ""

var mainframe = null as HTMLElement?
var websocket = null as WebsocketClient?

var accounts = mutableListOf<String>()
var currentPanel = null as Panel?

var panelIntro = PanelIntro()
var panelAddAccount = PanelAddAccount()
var panelMessages = PanelMessages()
var panelRestore = PanelRestore()

fun main() {

    clearStorage()

    var sv = window.localStorage.getItem("server")
    if(sv == null){
        sv = window.prompt("Enter server address", DEFAULT_SERVER)
        if (sv == null || sv == "") sv = DEFAULT_SERVER
        window.localStorage.setItem("server", sv)
    }
    server = sv

    val json = window.localStorage.getItem("accounts")
    if(json != null) {
        val list = JSON.parse<Array<String>>(json)
        accounts.addAll(list)
    }

    window.onload = {
        onload()
    }
}

fun onload(){
    mainframe = document.getElementById("panel") as HTMLElement

    // Initialize panels
    panelIntro.onload()
    panelRestore.onload()
    panelMessages.onload()
    panelAddAccount.onload()

    if(window.localStorage.get("skipIntro") != "true"){
        showPanel(panelIntro)
    } else {
        showMain()
    }

    document.addEventListener("keydown", EventListener {
        if(it is KeyboardEvent) {
            currentPanel?.handleInput(it)
        }
    })

    // Finally init websocket
    websocket = WebsocketClient(server)
}

fun showPanel(panel : Panel){
    currentPanel?.hide()
    currentPanel = panel
    panel.show()

    invalidateNav()
}

fun invalidateNav(){
    val title = currentPanel!!.getTitle()
    document.getElementById("header")?.textContent = title

    val left = currentPanel!!.getKeyLeft()
    val kLeft = document.getElementById("footer_left") as HTMLElement
    if(left != null) {
        kLeft.style.display = "visible"
        kLeft.textContent = left
    } else {
        kLeft.style.display = "none"
    }

    val right = currentPanel!!.getKeyRight()
    val kRight = document.getElementById("footer_right") as HTMLElement
    if(right != null) {
        kRight.style.display = "visible"
        kRight.textContent = right
    } else {
        kRight.style.display = "none"
    }
}

fun showRestore() {
    showPanel(panelRestore)
}

fun showMain() {
    if(accounts.size == 0){
        showPanel(panelAddAccount)
    } else {
        showPanel(panelMessages)
    }
}

fun clearStorage(){
    window.localStorage.clear()
}

fun addAccount(id : String){
    accounts.add(id)
    window.localStorage.setItem("accounts", JSON.stringify(accounts))
    window.alert("New account has been added")
    showMain()
}
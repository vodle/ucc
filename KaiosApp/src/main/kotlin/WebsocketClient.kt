import org.w3c.dom.WebSocket
import kotlin.js.Json

class WebsocketClient(val address : String) {
    private var websocket : WebSocket = WebSocket(address)

    init {
        websocket.onopen = {
            panelMessages.addItem(ItemMessage("WS opened", "system", "just now"))
        }
        websocket.onmessage = {
            console.log("resp: "+it.data)
            processResponse(JSON.parse(it.data.toString()));
        }
        websocket.onerror = {
            panelMessages.addItem(ItemMessage("WS error", "system", "just now"))

        }
        websocket.onclose = {
            panelMessages.addItem(ItemMessage("WS closed", "system", "just now"))

        }
    }

    private fun processResponse(resp : ServerResponse){
        console.log("Server: "+resp)
        when(resp.m){
            "onAdapterCreate" -> {addAccount(resp.a[0])}
            "onMessageUpdate" -> { }
        }
    }

    fun request(vararg input : Any){
        val arr = Array(input.size){
            _wrapSomeClasses(input[it]) // Hack
        }

        // Converting to JSON using standart JS converter
        val jsoner = js("JSON")
        val str = jsoner.stringify(arr).toString()

        websocket.send(str)
    }

    /**
     * Конвертируем некоторые типы котлина в js ones
     *
     * Эта штука требует особого пояснения.
     *
     * Дело в том, что преобразование в json у котлина сейчас немного сломано [https://stackoverflow.com/a/57733599].
     * Данная функция решает этот вопросик.
     */
    private fun _wrapSomeClasses(o : Any) : Any{

        if (o is Map<*, *>){
            val export = js("{}")
            for (pair: Pair<*, *> in o.toList()) {
                export[pair.first.toString()] = pair.second
            }

            return export
        } else {
            return o
        }
    }

    class ServerResponse(val m : String, val a : Array<String>)
}
import org.w3c.dom.events.KeyboardEvent
import org.w3c.dom.set
import kotlin.browser.window

class PanelIntro : Panel("panel_intro") {

    override fun getTitle(): String {
        return "First-launch warning"
    }

    override fun getKeyLeft(): String {
        return "Decline"
    }

    override fun getKeyRight(): String {
        return "Accept"
    }

    override fun handleInput(event: KeyboardEvent) {
        when (event.key) {
            "Control", "SoftLeft" -> {
                window.open("","_self")?.close();
                window.close()
            }
            "Insert", "SoftRight" -> {
                window.localStorage.set("skipIntro", "true")
                showRestore()
            }
        }
    }
}
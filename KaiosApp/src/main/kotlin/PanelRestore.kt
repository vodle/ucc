import org.w3c.dom.events.KeyboardEvent
import kotlin.browser.window

class PanelRestore : Panel("panel_restore"){

    override fun getTitle(): String {
        return "Restore key"
    }

    override fun getKeyLeft(): String {
        return "Pick file"
    }

    override fun getKeyRight(): String {
        return "Skip this"
    }

    override fun handleInput(event: KeyboardEvent) {
        when (event.key) {
            "Control", "SoftLeft" -> { window.alert("Unimplemented now") }
            "Insert", "SoftRight" -> { showMain() }
        }
    }
}
import org.w3c.dom.HTMLElement
import org.w3c.dom.events.Event
import org.w3c.dom.events.KeyboardEvent
import org.w3c.dom.get
import kotlin.browser.document

class PanelMessages : Panel("panel_messages") {

    var list = null as HTMLElement?
    var itemView = null as HTMLElement?
    var focusIdx = 0;

    override fun onload() {
        super.onload()
        list = root!!.children[0] as HTMLElement
        itemView = document.getElementById("pannel_messages_item") as HTMLElement
    }

    override fun getTitle(): String {
        return "#channelname"
    }

    override fun getKeyLeft(): String {
        return "Channels"
    }

    override fun getKeyRight(): String {
        return "Users"
    }

    override fun handleInput(event : KeyboardEvent) {
        event.preventDefault();

        when (event.key) {
            "Control", "SoftLeft" -> { }
            "Insert", "SoftRight", "Call" -> { }
            "ArrowUp" ->  { moveFocusOverList(-1) }
            "ArrowDown" ->  { moveFocusOverList(1) }
            "ArrowLeft" ->  { }
            "ArrowRight" ->  { }
        }
    }

    fun addItem(item : ItemMessage){
        val view = getView(item)

        list!!.appendChild(view)

        if(focusIdx == list!!.childNodes.length -2)
            moveFocusOverList(1);
    }

    fun moveFocusOverList(offset : Int){
        focusIdx += offset

        if(focusIdx > list!!.children.length) focusIdx = list!!.childNodes.length-1
        if(focusIdx < 0) focusIdx = 0

        val view = list!!.childNodes[focusIdx];
        if(view is HTMLElement) {
            view.focus()
            view.scrollIntoView()
        }
    }

    private fun getView(item: ItemMessage) : HTMLElement {
        val node = itemView!!.cloneNode(true) as HTMLElement

        node.children[0]!!.innerHTML = item.author;
        node.children[1]!!.innerHTML = item.time;
        node.children[3]!!.innerHTML = item.text;

        return node
    }
}
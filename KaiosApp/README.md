# KaiosApp

## RU
Клиент для Kaios. Для работы необходим собственный ucc-сервер (см. ServerApp).

### Gradle tasks
`collect` компилит котлин в js, копирует готовые скрипты и ресурсы в папку output для запуска из браузераю

`package` триггерит `collect`, запаковывает результаты в kaios package.

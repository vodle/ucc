# UCC
[![pipeline status](https://gitlab.com/iillyyaa2033/ucc/badges/master/pipeline.svg)](https://gitlab.com/iillyyaa2033/ucc/commits/master)

UCC is a chat-client for some popular messaging services.

This project now supports Android (as main platform) and KaiOS (experimental).


## Features
Project supports this chat service providers:
- Discord;
- VKontakte (russian social network);
- Telegram.

Full list of implemented features proided on [feature matrix page](https://gitlab.com/iillyyaa2033/ucc/blob/master/Documents/Features.md).

Client features
- __Message translation__. You  can quickly translate any message by long-press on it;
- __Image compression__. You can setup compressing image proxy server in order to save cellular data. [Read more...](https://gitlab.com/iillyyaa2033/ucc/blob/master/Documents/ImageProxy.md);
- __Extension system__. Some chat platforms will be implemented as add-ons. For now, heavy Telegram 
client extracted into single app so you don't have to download additional 40+ MB apk it if you're not 
using Telegram. 


## Download
- __Android__: [Latest CI build on master branch](https://gitlab.com/iillyyaa2033/ucc/-/jobs/artifacts/master/file/AndroidApp/build/outputs/apk/release/AndroidApp-release.apk?job=build_android);
- __KaiOS__: [Latest CI build on master branch](https://gitlab.com/iillyyaa2033/ucc/-/jobs/artifacts/master/file/KaiosApp/build/UCC-Kaios.zip?job=build_kaios);

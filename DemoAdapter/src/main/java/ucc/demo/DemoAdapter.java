package ucc.demo;

import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.EnhancedText;
import com.iillyyaa2033.ucmodel.Message;
import com.iillyyaa2033.ucmodel.User;
import com.iillyyaa2033.ucserver.Adapter;

public class DemoAdapter extends Adapter {

    @Override
    public User getAccountUser() {
        return new User(getEId(), "none", "Demo user", null);
    }

    @Override
    public String getServiceName() {
        return "Demo adapter";
    }

    @Override
    public Channel[] getChannels(String serverId) {
        return new Channel[0];
    }

    @Override
    public Message[] getLastMessages(String serverId, String channelId) {
        return new Message[0];
    }

    @Override
    public Message[] getMessagesBefore(String serverId, String channelId, String mid, long since, int count) {
        return new Message[0];
    }

    @Override
    public Message[] getMessagesSince(String serverId, String channelId, long since, int count) {
        return new Message[0];
    }

    @Override
    public void sendMessage(String sid, String cid, EnhancedText mentionedText, String[] attachments) {

    }
}

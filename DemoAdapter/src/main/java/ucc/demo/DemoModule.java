package ucc.demo;

import com.iillyyaa2033.ucmodel.ConnectionSettings;
import com.iillyyaa2033.ucserver.Adapter;
import com.iillyyaa2033.ucserver.AuthProc;
import com.iillyyaa2033.ucserver.Authenticator;
import com.iillyyaa2033.ucserver.Module;

import java.util.Map;

public class DemoModule extends Module {

    @Override
    public Descr getDescription() {
        return new Descr("ucc.demo.DemoAdapter","Demo","Just a demo extension");
    }

    @Override
    public Authenticator getAuthenticator() {
        return new Authenticator() {

            @Override
            public AuthProc getAuthProc() {
                return null;
            }

            @Override
            public ConnectionSettings finishAuthProc(Map<String, String> results) {
                return null;
            }
        };
    }

    @Override
    public Adapter getAdapter() {
        return new DemoAdapter();
    }
}

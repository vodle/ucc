package com.iillyyaa2033.ucserver

abstract class Module {

    class Descr(var className : String, var humanName : String, var description : String)

    abstract fun getDescription() : Descr
    abstract fun getAuthenticator() : Authenticator
    abstract fun getAdapter() : Adapter

    /**
     * Here module can load native libraries.
     * <p>
     *     This method triggered only at module creation.
     * </p><p>
     *     Android: app must have same sharedUserId and must be signed with same key as main app.
     * </p><p>
     *     TODO: inspect security issues.
     * </p>
     *
     * @param moduleLibsDirectory for android it is app's lib/{arch}/ folder
     */
    open fun loadNatives(moduleLibsDirectory : String){}

}

package com.iillyyaa2033.ucmodel

class ServerInfo(var key : String, var value : String?) : Comparable<ServerInfo> {

    override fun compareTo(other: ServerInfo): Int {
        return key.compareTo(other.key)
    }
}
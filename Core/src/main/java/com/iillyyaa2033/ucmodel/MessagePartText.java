package com.iillyyaa2033.ucmodel;

public class MessagePartText extends MessagePart {

    /**
     * Text of message
     */
    public EnhancedText text;

    public MessagePartText(String rawText){
        this(new EnhancedText(rawText));
    }

    public MessagePartText(EnhancedText text){
        this.text = text;
    }
}

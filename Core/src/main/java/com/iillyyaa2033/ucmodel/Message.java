package com.iillyyaa2033.ucmodel;

import java.util.Arrays;

public class Message implements Comparable<Message> {

    public static final long FLAG_DELETABLE = 1;
    public static final long FLAG_EDITABLE = 1 << 2;

    /**
     * Message is hidden (this typically means that user has been muted)
     */
    public static final long FLAG_HIDDEN = 1 << 28;
    public static final long FLAG_UNREAD = 1 << 29;
    public static final long FLAG_EDITED = 1 << 30;
    public static final long FLAG_DELETED = 1 << 31;

    /**
     * Extension (adapter) ID
     */
    public String eid;

    /**
     * Server ID
     */
    public String sid;

    /**
     * Chat/channel ID
     */
    public String cid;

    /**
     * Message ID
     */
    public String mid;

    /**
     * Author ID
     */
    public String uid;

    /**
     * Creation date in millis
     */
    public long date;

    public long flags = 0;

    public MessagePart[] parts;

    public String username = null;
    public String usericon = null;
    public int color = 0;

    @Deprecated
    public Message(String extId, String serverId, String chatId, String messageId, String authorId, long date, EnhancedText text, MessagePartAttachment[] attachments) {
        this.eid = extId;
        this.sid = serverId;
        this.cid = chatId;
        this.mid = messageId;

        this.uid = authorId;
        this.date = date;

        setParts(text, attachments);
    }


    private void setParts(EnhancedText text, MessagePartAttachment[] attachments) {

        if (text == null) {
            if (attachments == null) {
                parts = new MessagePart[0];
            } else {
                parts = attachments;
            }
        } else {
            MessagePartText mpText = new MessagePartText(text);

            if (attachments == null) {
                parts = new MessagePart[]{mpText};
            } else {
                parts = new MessagePart[attachments.length + 1];
                parts[0] = mpText;
                System.arraycopy(attachments, 0, parts, 1, attachments.length);
            }
        }
    }

    public Message(String extId, String serverId, String chatId, String messageId, String authorId, long date) {
        this(extId, serverId, chatId, messageId, authorId, date, new MessagePart[0]);
    }

    public Message(String extId, String serverId, String chatId, String messageId, String authorId, long date, MessagePart[] parts) {
        this.eid = extId;
        this.sid = serverId;
        this.cid = chatId;
        this.mid = messageId;

        this.uid = authorId;
        this.date = date;

        this.parts = parts;

        if (this.parts == null) {
            this.parts = new MessagePart[]{};
        }
    }

    /**
     * Thread unsafe
     *
     * @param messageParts parts to append
     */
    public void append(MessagePart... messageParts) {
        MessagePart[] output = new MessagePart[parts.length + messageParts.length];
        System.arraycopy(parts, 0, output, 0, parts.length);
        System.arraycopy(messageParts, 0, output, parts.length, messageParts.length);
        this.parts = output;
    }

    public EnhancedText getText() {
        if (parts.length > 0) {
            if (parts[0] instanceof MessagePartText) {
                return ((MessagePartText) parts[0]).text;
            }
        }

        return new EnhancedText("");
    }

    public String getRawText() {
        return getText().text;
    }

    @Override
    public String toString() {
        return eid + "|" + sid + "|" + cid + "|" + mid + " " + getText().toString();
    }

    @Override
    public int compareTo(Message message) {
        return Long.compare(date, message.date);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Message) {
            Message s = (Message) o;
            return (s.eid.equals(eid)) && (s.sid.equals(sid)) && (s.cid.equals(cid)) && (s.mid.equals(mid));
        } else {
            return super.equals(o);
        }
    }

    public boolean is(long flag) {
        return (flags & flag) != 0;
    }

    public void set(long flag) {
        flags = flags | flag;
    }

    public void unset(long flag) {
        flags = flags & (~flag);
    }

    public void updateByAppearance(String name, String icon, int color) {
        if (name != null) this.username = name;
        if (icon != null) this.usericon = icon;
        if (color != 0) this.color = color;
    }

    public boolean replaceFrom(Message message) {
        boolean changed = false;

        if (!eid.equals(message.eid)) {
            changed = true;
            eid = message.eid;
        }

        if (!sid.equals(message.sid)) {
            changed = true;
            sid = message.sid;
        }

        if (!cid.equals(message.cid)) {
            changed = true;
            cid = message.cid;
        }

        if (!mid.equals(message.mid)) {
            changed = true;
            mid = message.mid;
        }

        if (!uid.equals(message.uid)) {
            changed = true;
            uid = message.uid;
        }

        if (date != message.date) {
            changed = true;
            date = message.date;
        }


        if (flags != message.flags) {
            changed = true;
            flags = message.flags;
        }

        if (!Arrays.equals(parts, message.parts)) {
            changed = true;
            parts = message.parts;
        }


        if (message.username != null && !message.username.equals(username)) {
            changed = true;
            username = message.username;
        }

        if (message.usericon != null && !message.usericon.equals(usericon)) {
            changed = true;
            usericon = message.usericon;
        }

        if (color != message.color) {
            changed = true;
            color = message.color;
        }

        return changed;
    }
}

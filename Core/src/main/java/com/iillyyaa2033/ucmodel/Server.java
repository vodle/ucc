package com.iillyyaa2033.ucmodel;

public class Server implements Comparable<Server>{

    public static final long FLAG_HARDCODED = 1;
    public static final long FLAG_GONE = 1 << 1;

    /**
     * Extension (adapter) ID
     */
    public String eid;

    /**
     * Server id
     */
    public final String sid;

    /**
     * Human-readable name
     */
    public final String name;

    public int priority = 0;
    public int unread_count = 0;

    public String parentId;

    /**
     * Server icon URL
     */
    public String icon_res;

    public long flags = 0;

    public Server(String eid, String sid, String name){
        this.eid = eid;
        this.sid = sid;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Server) {
            Server s = (Server) o;
            return (s.eid.equals(eid)) && (s.sid.equals(sid));
        } else {
            return super.equals(o);
        }
    }

    public boolean is(long flag){
        return (flags & flag) != 0;
    }

    @Override
    public int compareTo(Server server) {
        if(server == null) return 1;
        else return priority - server.priority;
    }
}

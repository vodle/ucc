package com.iillyyaa2033.ucmodel;

import java.util.UUID;

public class ConnectionSettings {

    private final String eid;
    private final String target;
    private final String proxy;
    private final String[] userAgent; // Browser, Os, Device, ...
    private final String token;

    public ConnectionSettings(String target, String proxy, String[] userAgent, String token){
        this(UUID.randomUUID().toString(), target, proxy, userAgent, token);
    }

    public ConnectionSettings(String eid, String target, String proxy, String[] userAgent, String token){
        this.eid = eid;
        this.target = target;
        this.proxy = proxy;
        this.userAgent = userAgent;
        this.token = token;
    }

    public String getEid(){
        return eid;
    }

    public String getTarget(){
        return target;
    }

    // ========== //
    // USER AGENT //
    // ========== //
    public String[] getUserAgent(){
        return userAgent;
    }

    public String formatUserAgent(){
        return String.format("%s (%s, %s, %s)", userAgent[0], userAgent[1], userAgent[2], userAgent[3]);
    }

    public String formatUserAgentForDB(){
        return String.format("%s|%s|%s|%s", userAgent[0], userAgent[1], userAgent[2], userAgent[3]);
    }

    public String getUserAgentOs(){
        return userAgent[1];
    }

    public String getUserAgentBrowser(){
        return userAgent[0];
    }

    public String getUserAgentDevice(){
        return userAgent[2];
    }

    // ===== //
    // PROXY //
    // ===== //
    public boolean isUsingProxy(){
        if(proxy == null){
            return false;
        } else if(proxy.isEmpty()){
            return false;
        } else {
            return true;
        }
    }

    public String getProxy(){
        return proxy;
    }

    public String getProxyHost(){
        if(proxy != null) {
            return proxy.split(":")[0];
        } else {
            return null;
        }
    }

    public int getProxyPort(){
        try{
            return Integer.parseInt(proxy.split(":")[1]);
        } catch (Exception e) {
            return 8080;
        }
    }


    public String getToken(){
        return token;
    }
}

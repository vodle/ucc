package com.iillyyaa2033.ucmodel;

public class MessagePartAttachment extends MessagePart {

    /**
     * Type of attachment
     */
    public int type = 1;

    /**
     * Optional title of attachment
     */
    public String title = null;

    /**
     * Optional description of attachment
     */
    public String description = null;

    /**
     * Defines address of thumbnail
     */
    public String thumbUrl = null;

    /**
     * Defines url where should we go when user pressed attachment block.
     */
    public String actionUrl = null;

    /**
     * Width and height of preview image
     */
    public int w, h;

    public MessagePartAttachment[] recursive;

    public static class Type {
        public static final int IMAGE = 0;
        public static final int URL = 1;
        public static final int MUSIC = 2;
        public static final int VIDEO = 3;
        public static final int GIF = 4;
        public static final int STICKER = 6;

        public static final int RECURSIVE = 5;
    }
}

package com.iillyyaa2033.ucmodel;

public class User {

    /**
     * Extension (adapter) ID
     */
    public String eid;

    /**
     * User ID
     */
    public final String uid;

    public String username;
    public String icon_res;

    public User(String eid, String uid, String username, String icon_res){
        this.eid = eid;
        this.uid = uid;
        this.username = username;
        this.icon_res = icon_res;
    }

    @Override
    public String toString() {
        return username;
    }
}

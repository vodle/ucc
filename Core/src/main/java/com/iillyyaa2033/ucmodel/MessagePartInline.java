package com.iillyyaa2033.ucmodel;

public class MessagePartInline extends MessagePart {

    public InlineButton[] buttons = {};

    public static class InlineButton {

        public String text;
        public boolean isActive;

        public String callbackData;

    }
}

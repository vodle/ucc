package com.iillyyaa2033.ucmodel;

public class Channel implements Comparable<Channel>{

    public static final int TYPE_TEXT = 0;
    public static final int TYPE_GROUP = 1;
    public static final int TYPE_VOICE = 2;

    public static final long FLAG_UNREAD = 1;
    public static final long FLAG_HIDDEN = 1 << 2;

    /**
     * Extension (adapter) ID
     */
    public String eid;

    /**
     * Server ID
     */
    public String sid;

    /**
     * Channel ID
     */
    public String cid;

    public String title;
    public String last_msg;
    public long last_time;

    public int draw_mode = ChannelDrawMode.MINIMIZED;
    public int type = TYPE_TEXT;
    public long priority = -1;
    public String parent_id = "0";
    public String icon;

    public long flags = 0;

    /**
     * Unread messages count
     *
     * 0 for none
     * >0 for exact value
     * <0 for 'we have unknown number of unread messages'
     */
    public int unread_count = 0;

    public Channel(String extId, String sid, String cid,
                   String title, String lastMessage, long lastMessageDate){
        this.eid = extId;
        this.sid = sid;
        this.cid = cid;
        this.title = title;
        this.last_msg = lastMessage;
        this.last_time = lastMessageDate;
    }

    @Override
    public String toString() {
        return title;
    }

    @Override
    public int compareTo(Channel t1) {
        if(t1 == null) return 1;

        return Long.compare(priority, t1.priority);
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Channel){
            Channel s = (Channel) o;
            return (s.eid.equals(eid)) && (s.sid.equals(sid)) && (s.cid.equals(cid));
        } else {
            return super.equals(o);
        }
    }

    public Channel getCopy(){
        Channel ch = new Channel(eid, sid, cid, title, last_msg, last_time);

        ch.draw_mode = draw_mode;
        ch.priority = priority;
        ch.parent_id = parent_id;
        ch.icon = icon;

        return ch;
    }

    public boolean is(long flag){
        return (flags & flag) != 0;
    }

    public void set(long flag){
        flags = flags | flag;
    }

    public void removeFlag(long flag){
        flags = flags & (~flag);
    }

    public static class ChannelDrawMode {
        public static final int DEFAULT = 0;
        public static final int MINIMIZED = 1;
        public static final int CATEGORY = 2;
    }
}

package com.iillyyaa2033.ucmodel;

import java.util.HashMap;

public class EnhancedText {

    public String text;

    /**
     * Mentions
     * <p>
     *  key = user ID (adapter-specific message id);<br/>
     *  value = human-readable UID (for source text | used as fancy name if no fancyName specified)
     * </p>
     * <p>
     *   По задумке key используется для того, чтобы открывать инфу о юзере по клику/лонгклику меншена.
     *   value юзается для того, чтобы определить, какая часть текста считается меншеном юзера
     * </p>
     */
    public HashMap<String, String> mentions;

    /**
     * key = adapter-specific uid
     * value = human-readable value
     */
    public HashMap<String, String> fancyNames;

    /**
     * Other replacements
     *
     * key = from
     * value = to
     */
    public HashMap<String, String> otherReplacements;

    public EnhancedText(){
        this("");
    }

    public EnhancedText(String text) {
        this.text = text;
        mentions = new HashMap<>();
        fancyNames = new HashMap<>();
        otherReplacements = new HashMap<>();
    }

    @Override
    public String toString() {
        return text;
    }

    public String getMarkdowned(){

        String text = ""+this.text;


        if(mentions.size() > 0) {
            for (String id : mentions.keySet()) {
                String user = mentions.get(id);
                String fancyName = fancyNames.get(id);
                if (fancyName == null) fancyName = user;

                String replacement = String.format("<a href=\"%s\">_**%s**_</a>", id, fancyName);

                text = text.replace(user, replacement);
            }
        }

        if(otherReplacements.size() > 0){
            for(String from : otherReplacements.keySet()){
                String to = otherReplacements.get(from);
                text = text.replace(from, to);
            }
        }

        return text;
    }
}

package com.iillyyaa2033.ucscheduler;

import com.iillyyaa2033.ucscheduler.tasks.NotificationCheckerTask;
import com.iillyyaa2033.ucscheduler.tasks.TestTask;
import com.iillyyaa2033.ucserver.Adapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class Executor {

    /**
     * If task's launch time is within current time +- half of this threshold,
     * task will be launched right now.
     */
    public static final long LAUNCH_THRESHOLD = 1 * 60 * 1000;

    private List<Task> tasks = new ArrayList<>();

    public void addTask(Task task){
        tasks.add(task);
        task.setLastLaunchTime(System.currentTimeMillis());
    }

    public void removeTask(Task task){
        tasks.remove(task);
    }

    public Task[] getTasks(){
        return tasks.toArray(new Task[0]);
    }

    public static Task[] getAvailableTasks(){
        return new Task[]{
                new TestTask(),
                new NotificationCheckerTask()
        };
    }

    public void onSaveState(File file) throws IOException {

        System.out.println("Executor: save state");

        file.createNewFile();
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        oos.writeObject(tasks);

        oos.close();
        fos.close();
    }

    public void onRestoreState(File file) throws IOException, ClassNotFoundException {

        System.out.println("Executor: restore state");

        if(file.exists()) {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);

            List<Task> tasks = (List<Task>) ois.readObject();

            ois.close();
            fis.close();

            this.tasks = tasks;
        } else {

            if(tasks.size() == 0){
                tasks.add(new NotificationCheckerTask());
            }

        }
    }

    public long getNextWakeupTime(){
        long time = System.currentTimeMillis();

        long min = Long.MAX_VALUE;

        for(Task task : tasks){
            long t = task.getLaunchTimeAfter(time);

            if(t < min){
                min = t;
            }
        }

        return min;
    }

    public void execute(Adapter[] adapters, TaskCallback callback){
        long time = System.currentTimeMillis();

        int count = 0;

        for(Task task : tasks){
            long nlt = task.getLaunchTimeAfter(time - LAUNCH_THRESHOLD /2) - time; // Next launch time
            System.out.println(task.getClass().getName()+" NLT: "+(nlt));

            if(nlt < LAUNCH_THRESHOLD /2){
                count++;

                task.execute(adapters, callback);
            }
        }

        System.out.println("Executor: done "+count+"/"+getTasks().length+" tasks");
    }

}

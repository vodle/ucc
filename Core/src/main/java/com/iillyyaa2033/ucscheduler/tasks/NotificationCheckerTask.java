package com.iillyyaa2033.ucscheduler.tasks;

import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.Server;
import com.iillyyaa2033.ucscheduler.Task;
import com.iillyyaa2033.ucscheduler.TaskCallback;
import com.iillyyaa2033.ucserver.Adapter;

public class NotificationCheckerTask extends Task {

    @Override
    public long getLaunchTimeAfter(long millis) {
        Long interval = getParamLong(PARAM_INTERVAL);
        if(interval == null) interval = 15L;
        return getLaunchTimeForRecurringTask(millis, interval * 60 * 1000);
    }

    @Override
    public void execute(Adapter[] adapters, TaskCallback callback) {

        int unread_adapters = 0;
        int unread_servers = 0;
        int unread_channels = 0;
        int unread_messages = 0;

        for(Adapter adapter : adapters){
            boolean isAdapterUnread = false;

            for(Server server : adapter.getServers()){
                boolean isServerUnread = false;

                for(Channel channel : adapter.getChannels(server.sid)){
                    boolean isChannelUnread = false;

                    if(channel.unread_count > 0){
                        unread_messages += channel.unread_count;
                        isChannelUnread = true;
                    }

                    if(isChannelUnread){
                        isServerUnread = true;
                        unread_channels++;
                    }
                }

                if(isServerUnread){
                    isAdapterUnread = true;
                    unread_servers++;
                }
            }

            if(isAdapterUnread) unread_adapters++;
        }

        if(unread_messages > 0){
            String message = "You have "+unread_messages+" important messages on "+ unread_channels +" channels";

            callback.notification("notification check", message);
        } else {
            callback.notification("notification check", "No unread messages");
        }
    }

    @Override
    public String getDescription() {
        return "Check and notify about unread messages";
    }

    @Override
    public String[] getSupportedParams() {
        return new String[]{
                PARAM_INTERVAL
        };
    }

    @Override
    public String getDefaultValue(String param) {
        switch (param){
            case PARAM_INTERVAL: return "15";
            default: return null;
        }
    }

    @Override
    public boolean isSystem() {
        return true;
    }
}

package com.iillyyaa2033.ucscheduler.tasks;

import com.iillyyaa2033.ucscheduler.Task;
import com.iillyyaa2033.ucscheduler.TaskCallback;
import com.iillyyaa2033.ucserver.Adapter;

public class TestTask extends Task {

    @Override
    public long getLaunchTimeAfter(long millis) {
        return getLaunchTimeForRecurringTask(millis, 1 * 60 * 1000);
    }

    @Override
    public void execute(Adapter[] adapters, TaskCallback callback) {
//        callback.notification("Test task",adapter.getClass().getSimpleName()+" has "+adapter.getServers().length+" servers");
    }

    @Override
    public String getDescription() {
        return "Just test task";
    }
}

package com.iillyyaa2033.ucscheduler;

import com.iillyyaa2033.ucserver.Adapter;

import java.io.Serializable;
import java.util.HashMap;

/**
 * TODO: explanation
 *
 * <p>
 *     N.B. take a loot at Executor.LAUNCH_THRESHOLD field
 * </p>
 */
public abstract class Task implements Serializable {

    private long lastLaunchTime = 0;
    private HashMap<String, String> params = new HashMap<>();

    @Override
    public String toString() {
        return getDescription();
    }

    public final void setLastLaunchTime(long millis){
        this.lastLaunchTime = millis;
    }
    public final long getLastLaunchTime(){
        return lastLaunchTime;
    }
    public final long getLaunchTimeForRecurringTask(long after, long interval){

        float steps = 1f * (after - getLastLaunchTime()) / interval;

        long timeOffset;
        if(steps > 0){
            timeOffset = (long) (Math.floor(steps + 1) * interval);
        } else {
            timeOffset = (long) (Math.floor(steps) * interval);
        }

        return getLastLaunchTime() + timeOffset;
    }

    public final void setParams(HashMap<String, String> params){ this.params = params; }
    public final void setParam(String name, String value){params.put(name, value);}
    public final String getParam(String name){ return params.containsKey(name) ? params.get(name) : getDefaultValue(name) ; }
    public final Long getParamLong(String name) {
        try {
            return Long.parseLong(getParam(name));
        } catch (NumberFormatException e){
            return null;
        }
    }


    /**
     * Override this for system tasks
     *
     * @return true if this task required for proper work of app's basic functions
     */
    public boolean isSystem(){
        return false;
    }

    public String[] getSupportedParams(){
        return new String[0];
    }
    public String getDefaultValue(String param){ return null; }

    public abstract long getLaunchTimeAfter(long millis);
    public abstract void execute(Adapter[] adapters, TaskCallback callback);
    public abstract String getDescription();

    /**
     * Specifies adapter's uid to work on.
     */
    public static final String PARAM_ADAPTER = "adapter";

    /**
     * Task execution interval in minutes
     */
    public static final String PARAM_INTERVAL = "interval";
}

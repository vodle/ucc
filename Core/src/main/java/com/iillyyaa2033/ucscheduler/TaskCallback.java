package com.iillyyaa2033.ucscheduler;

public interface TaskCallback {

    void notification(String title, String message);
    void download(String url);

}

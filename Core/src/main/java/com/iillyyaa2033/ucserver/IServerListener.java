package com.iillyyaa2033.ucserver;

import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.Message;
import com.iillyyaa2033.ucmodel.Server;

import java.util.Collection;

/**
 * Additional events sent by LocalServer
 */
public interface IServerListener extends IEventsListener {

    void onServerStopped();

    void onAdaptersLoaded(Collection<Adapter> adapters);

    void onAdapterCreate(Adapter adapter);
    void onAdapterUpdate(Adapter adapter);
    void onAdapterRemove(String eid);


    void onServersLoaded(boolean fromCache, String eid, Server... servers);

    void onChannelsLoaded(boolean fromCache, String eid, String sid, Channel[] channels);

    void onMessagesLoaded(boolean fromCache, String eid, String cid, Message[] messages);

    void onMessagesBeforeLoaded(boolean fromCache, String eid, String cid, long date, Message[] messages);
}

package com.iillyyaa2033.ucserver;

import com.iillyyaa2033.ucmodel.ConnectionSettings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;

public class AdapterHolder {

    private IEventsListener listener;
    private HashMap<String, Module> modulesMap;
    private HashMap<String, Adapter> adaptersMap;

    public AdapterHolder(IEventsListener listener) {
        this.listener = listener;
        modulesMap = new HashMap<>();
        adaptersMap = new HashMap<>();
    }

    public void loadAdapters(ConnectionSettings[] settings) {

        for (ConnectionSettings setting : settings) {
            try {
                setupAdapter(setting);
            } catch (Throwable e) {
                if(listener != null) listener.onError(e);
            }
        }
    }

    public void addModule(Module module) {
        modulesMap.put(module.getDescription().getClassName(), module);
    }

    public Collection<Module> getModules() {
        return modulesMap.values();
    }

    public Adapter setupAdapter(ConnectionSettings settings) {
        Adapter adapter = createAdapterByClass(settings.getTarget());

        if (adapter != null) {
            adapter.setEid(settings.getEid());
            adapter.setSettings(settings);
            adapter.setListener(listener);

            adapter.onConnectionSettingsChanged(settings);
            adaptersMap.put(settings.getEid(), adapter);
        }

        return adapter;
    }

    private Adapter createAdapterByClass(String clazz) {
        if (modulesMap.containsKey(clazz)) return modulesMap.get(clazz).getAdapter();
        else return null;
    }

    public boolean has(String extId) {
        return adaptersMap.containsKey(extId);
    }

    public Adapter get(String extId) {
        return adaptersMap.get(extId);
    }

    public Collection<Adapter> all() {
        return adaptersMap.values();
    }

    public void remove(String eid) {
        if (has(eid)) get(eid).stopRealtime();

        adaptersMap.remove(eid);
    }

    public void saveStates(File folder) {
        for (Adapter adapter : adaptersMap.values()) {
            try {
                Object object = adapter.onSaveState();

                if (object != null) {

                    File file = new File(folder, adapter.getEId());
                    FileOutputStream fos = new FileOutputStream(file);
                    ObjectOutputStream oos = new ObjectOutputStream(fos);

                    oos.writeObject(object);

                    oos.close();
                    fos.close();
                }
            } catch (Exception e) {
                if (listener != null) listener.onError(e);
            }
        }
    }

    public void restoreStates(File folder) {
        for (Adapter adapter : adaptersMap.values()) {
            try {
                File file = new File(folder, adapter.getEId());
                if (file.exists()) {
                    FileInputStream fis = new FileInputStream(file);
                    ObjectInputStream ois = new ObjectInputStream(fis);

                    Object object = ois.readObject();

                    ois.close();
                    fis.close();

                    adapter.onRestoreState(object);
                }
            } catch (Exception e) {
                if (listener != null) listener.onError(e);
            }
        }
    }
}

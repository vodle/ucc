package com.iillyyaa2033.ucserver;

import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.Message;
import com.iillyyaa2033.ucmodel.MessageMetaPatch;
import com.iillyyaa2033.ucmodel.Server;
import com.iillyyaa2033.ucmodel.User;

import java.util.Collection;

public class ServerListenerStub implements IServerListener {

    @Override
    public void onServerStopped() {

    }

    @Override
    public void onAdaptersLoaded(Collection<Adapter> adapters) {

    }

    @Override
    public void onAdapterCreate(Adapter adapter) {

    }

    @Override
    public void onAdapterUpdate(Adapter adapter) {

    }

    @Override
    public void onAdapterRemove(String eid) {

    }

    @Override
    public void onServersLoaded(boolean fromCache, String eid, Server... servers) {

    }

    @Override
    public void onChannelsLoaded(boolean fromCache, String eid, String sid, Channel[] channels) {

    }

    @Override
    public void onMessagesLoaded(boolean fromCache, String eid, String cid, Message[] messages) {

    }

    @Override
    public void onMessagesBeforeLoaded(boolean fromCache, String eid, String cid, long date, Message[] messages) {

    }

    @Override
    public void onServerCreate(Server... server) {

    }

    @Override
    public void onServerUpdate(Server... server) {

    }

    @Override
    public void onServerRemove(Server... server) {

    }

    @Override
    public void onChannelUpdate(Channel... channel) {

    }

    @Override
    public void onChannelDelete(String... channels) {

    }

    @Override
    public void onMessageUpdate(Message message) {

    }

    @Override
    public void onMessageDelete(String eid, String guildId, String channelId, String messageId) {

    }

    @Override
    public void onMessageMetaPatch(MessageMetaPatch[] patches) {

    }

    @Override
    public void onReadStateUpdate(String eid, String sid, String cid, String lastReadMessage) {

    }

    @Override
    public void onUserUpdate(User user) {

    }

    @Override
    public void onUserPresenceUpdate(String server, String channel, User user) {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onTypingState(String userId, boolean isBecameActive, String eid, String serverId, String channelId) {

    }

    @Override
    public void onRealtimeState(int state, String extid) {

    }

    @Override
    public void onUploadProgress(String filename, int progress) {

    }
}

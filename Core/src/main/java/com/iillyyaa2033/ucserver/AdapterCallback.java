package com.iillyyaa2033.ucserver;

import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.Message;
import com.iillyyaa2033.ucmodel.MessageMetaPatch;
import com.iillyyaa2033.ucmodel.Server;
import com.iillyyaa2033.ucmodel.User;

import java.util.Timer;
import java.util.TimerTask;

public class AdapterCallback {

    private String eid;
    private IEventsListener listener;

    void setListener(IEventsListener listener){
        this.listener = listener;
    }

    private String getEId(){
        return eid;
    }

    public void setEid(String eid){
        this.eid = eid;
    }

    public void serverCreate(Server... server){
        if(listener != null) listener.onServerCreate(server);
    }

    public void serverRemove(Server... servers){
        if(listener != null) listener.onServerRemove(servers);
    }

    public void channelDelete(String... channels){
        if(listener != null) listener.onChannelDelete(channels);
    }



    public void sendServerUpdate(final Server... server){
        if(listener != null) listener.onServerUpdate(server);
    }

    public void sendChannelUpdate(final Channel... channel){
        if(listener != null) listener.onChannelUpdate(channel);
    }

    public void sendMessageUpdate(final Message message){
        if(listener != null) listener.onMessageUpdate(message);
    }

    public void sendMessageDelete(String guildId, String channelId, String messageId){
        if(listener != null) listener.onMessageDelete(getEId(), guildId, channelId, messageId);
    }

    public void sendUserUpdated(final User user){
        if(listener != null) listener.onUserUpdate(user);
    }

    public void sendError(Throwable e){
        if(listener != null) listener.onError(e);
    }

    public void sendTypingWithDelay(final String userId, final String serverId, final String channelId, long stopAfterNanos){
        // TODO: wrong implementation
        // Этот метод не должен слать никаких onTypingState если его уже триггерили с теми же самыми
        // uid-eid-cid, а срок предыдущего триггера не истек
        if(listener != null) listener.onTypingState(userId, true, getEId(), serverId, channelId);

        new Timer().schedule(new TimerTask(){
            @Override
            public void run() {
                if(listener != null) listener.onTypingState(userId,false, getEId(), serverId, channelId);
            }
        }, stopAfterNanos);
    }

    public void sendRealtimeStarting(){
        if(listener != null) listener.onRealtimeState(Adapter.REALTIME_STARTING, getEId());
    }

    public void sendRealtimeStarted(){
        if(listener != null) listener.onRealtimeState(Adapter.REALTIME_STARTED, getEId());
    }

    public void sendUploadProgress(String filename, int progress){
        if(listener != null) listener.onUploadProgress(filename, progress);
    }

    public void sendMessageMetaPatch(MessageMetaPatch... patch){
        if(listener != null) listener.onMessageMetaPatch(patch);
    }

    public void sendReadStateUpdate(String sid, String cid, String lastReadMessage){
        if(listener != null) listener.onReadStateUpdate(getEId(), sid, cid, lastReadMessage);
    }
}

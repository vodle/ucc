package com.iillyyaa2033.ucserver;

import com.iillyyaa2033.UCStorage;
import com.iillyyaa2033.ucmodel.MessageMetaPatch;
import com.iillyyaa2033.ucmodel.Server;
import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.ConnectionSettings;
import com.iillyyaa2033.ucmodel.EnhancedText;
import com.iillyyaa2033.ucmodel.Message;
import com.iillyyaa2033.ucmodel.ServerInfo;
import com.iillyyaa2033.ucmodel.User;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Default server implementation for any type of user application.
 *
 * <p>
 *     TODO: a few words about @ExportRequest annotation
 * </p>
 */
public class LocalServer implements IEventsListener {

    /**
     * Server online (connected and works)
     */
    public static final int NETWORK_MODE_ONLINE = 0;

    /**
     * Server is offline (no internet connection)
     */
    public static final int NETWORK_MODE_OFFLINE = 1;

    private List<IServerListener> listeners = new ArrayList<>();

    private ExecutorService executor;

    private AdapterHolder holder;

    private boolean useStorage = false;
    private UCStorage storage;

    private boolean useCache = false;
    private String cacheFolder = null;

    /**
     * Current network state. You have to manually update this field.
     */
    public static volatile int networkMode = NETWORK_MODE_ONLINE;

    /**
     * Database-less constructor
     *
     * @param listener -
     * @param modules  -
     */
    public LocalServer(IServerListener listener, ArrayList<Module> modules) {
        listeners.add(listener);

        holder = new AdapterHolder(this);

        executor = Executors.newCachedThreadPool();

        for (Module m : modules) holder.addModule(m);
    }

    /**
     * Default constructor.
     *
     * <p>
     * Note: if dbPath points to non-existing file new database will be created.
     * </p>
     *
     * @param dbPath   path to local database. Can be absolute, or start with ./ (current directory) or start with ~/ (home directory)
     * @param listener callback for most of operations here
     * @param modules  list of Module objects
     * @throws SQLException on database problems
     */
    public LocalServer(String dbPath, String cachePath, IServerListener listener, ArrayList<Module> modules) throws SQLException {
        this(listener, modules);

        useStorage = true;
        storage = UCStorage.connect(dbPath);

        useCache = true;
        cacheFolder = cachePath;

        executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    holder.loadAdapters(storage.getAdapters());
                    if(useCache) holder.restoreStates(new File(cacheFolder));
                } catch (SQLException e) {
                    onError(e);
                }
                
                for(IServerListener listener : listeners)
                    listener.onAdaptersLoaded(holder.all());
            }
        });
    }

    public void addListener(IServerListener listener){
        listeners.add(listener);
    }
    
    public void removeListener(IServerListener listener){
        listeners.remove(listener);
    }
    
    public void pause(){
        if(useCache) holder.saveStates(new File(cacheFolder));
    }

    public void destroy() {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                for (Adapter adapter : holder.all()) {
                    if (adapter != null) adapter.stopRealtime();
                }

                for(IServerListener listener : listeners) listener.onServerStopped();
                listeners.clear();
                
                holder = null;
                storage = null;

                executor.shutdownNow();
            }
        });
    }

    public void delMyData() {
        try {
            if (useStorage) storage.delMyData();
        } catch (SQLException e) {
            onError(e);
        }
    }

    // Adapters

    public Adapter getAdapter(String eid) {
        return holder.get(eid);
    }

    public Collection<Adapter> getLoadedAdapters() {
        return holder.all();
    }

    public boolean adapterHasFeature(String eid, long featureCode) {
        return holder.get(eid).hasFeature(featureCode);
    }

    public void delAdapter(String eid) {
        try {
            if (useStorage) storage.delAdapter(eid);
            holder.remove(eid);
            for(IServerListener listener : listeners) listener.onAdapterRemove(eid);
        } catch (SQLException e) {
            onError(e);
        }
    }


    // Auth proc
    public List<Module.Descr> getAvailableAdapters() {
        List<Module.Descr> rz = new ArrayList<>();

        for (Module m : holder.getModules()) {
            rz.add(m.getDescription());
        }

        return rz;
    }

    public AuthProc getAuthProcFor(String authProcClass) {
        for (Module m : holder.getModules()) {
            if (m.getDescription().getClassName().equals(authProcClass)) {
                return m.getAuthenticator().getAuthProc();
            }
        }
        return null;
    }

    @ExportRequest
    public void finishAuthProc(final String className, final HashMap<String, String> results) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                for (Module module : holder.getModules()) {
                    if (module.getDescription().getClassName().equals(className)) {
                        ConnectionSettings settings = module.getAuthenticator().finishAuthProc(results);
                        try {
                            Adapter adapter = holder.setupAdapter(settings);
                            if (useStorage) storage.addAdapter(settings);
                            for(IServerListener listener : listeners) listener.onAdapterCreate(adapter);
                        } catch (Throwable throwable) {
                            for(IServerListener listener : listeners) listener.onError(throwable);
                        }
                    }
                }
            }
        });
    }

    // Invites

    public String getInviteInfo(String eid, String invite) {
        return holder.get(eid).getInviteInfo(invite);
    }

    public User getUser(String eid, String uid) {
        try {
            if (useStorage) return storage.getUser(uid);
            else return null;
        } catch (SQLException e) {
            onError(e);
            return null;
        }
    }

    public ServerInfo[] getExtendedServerInfo(String eid, String sid){
        return getAdapter(eid).getServerInfo(sid);
    }

    // ==== //
    // SEND //
    // ==== //

    // request

    /**
     * Stops realtime mode on fromEid. Starts realtime on toEid.
     *
     * @param fromEid -
     * @param toEid   -
     */
    @ExportRequest
    public void requestAdapterSwitch(final String fromEid, String toEid) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                Adapter from = holder.get(fromEid);
                if (from != null) from.stopRealtime();
            }
        });

        // TODO: make it async?
        Adapter to = holder.get(toEid);
        if (to != null && !to.isRealtime()) to.startRealtime();
    }

    /**
     * Get list of all servers on given adapter
     *
     * <p>
     * This function sends onServersLoaded twice: after lookup in internal db and after performing request to
     * remote server.
     * </p>
     *
     * @param eid id of your adapter
     */
    @ExportRequest
    public void requestServers(final String eid) {

        if (useStorage) {
            try {
                Server[] servers = storage.getServers(eid);
                for(IServerListener listener : listeners) listener.onServersLoaded(true, eid, servers);
            } catch (SQLException e) {
                onError(e);
            }
        }

        executor.submit(new Runnable() {
            @Override
            public void run() {
                Server[] servers = holder.get(eid).getServers();
                if (servers != null && servers.length > 0) {

                    for(IServerListener listener : listeners) listener.onServersLoaded(false, eid, servers);

                    if (useStorage) {
                        try {
                            storage.updateServers(eid, servers);
                        } catch (SQLException e) {
                            onError(e);
                        }
                    }
                }
            }
        });
    }

    /**
     * Get list of channels on given adapter/server
     *
     * <p>
     * This function sends onChannelsLoaded twice: after lookup in internal db and after performing request to
     * remote server.
     * </p>
     *
     * @param eid id of adapter
     * @param sid is of server
     */
    @ExportRequest
    public void requestChannels(final String eid, final String sid) {
        if (useStorage) {
            try {
                Channel[] channels = storage.getChannels(eid, sid);
                for(IServerListener listener : listeners) listener.onChannelsLoaded(true, eid, sid, channels);
            } catch (SQLException e) {
                onError(e);
            }
        }

        executor.submit(new Runnable() {
            @Override
            public void run() {

                Channel[] channels = holder.get(eid).getChannels(sid);
                if (channels != null && channels.length > 0) {

                    if (useStorage) {
                        for (Channel ch : channels) {
                            long lastVisit = 0;

                            try {
                                lastVisit = storage.getLastVisited(ch.eid, ch.sid, ch.cid);
                            } catch (SQLException e) {
                                onError(e);
                            }

                            if (!holder.get(eid).hasFeature(Adapter.Feature.EXCPLICT_UNREADS)
                                    && lastVisit < ch.last_time)
                                ch.set(Channel.FLAG_UNREAD);
                        }
                    }

                    for(IServerListener listener : listeners) listener.onChannelsLoaded(false, eid, sid, channels);

                    if (useStorage) {
                        try {
                            storage.updateChannels(eid, sid, channels);
                        } catch (SQLException e) {
                            onError(e);
                        }
                    }
                }
            }
        });
    }

    /**
     * Get last 50 messages on given adapter/server/channel
     *
     * <p>
     * This function sends onMessagesLoaded on success.
     * </p>
     *
     * @param eid id of adapter
     * @param sid id of server
     * @param cid id of channel
     */
    @ExportRequest
    public void requestMessages(final String eid, final String sid, final String cid) {

        executor.submit(new Runnable() {
            @Override
            public void run() {
                if (networkMode == NETWORK_MODE_OFFLINE) return;

                Message[] messages = holder.get(eid).getLastMessages(sid, cid);

                if (messages != null && messages.length > 0) {

                    for(IServerListener listener : listeners) listener.onMessagesLoaded(false, eid, cid, messages);

                    if (useStorage) {
                        for (int i = 0; i < messages.length; i++) {
                            try {
                                storage.updateMessage(messages[i]);
                            } catch (SQLException e) {
                                onError(e);
                            }
                        }
                    }
                }
            }
        });
    }

    /**
     * Get 50 messages before given one
     *
     * <p>
     * (Сейчас нужно передавать и mid и date. Адаптер оставляет на своей совести выбор критерия выборки)
     * </p>
     *
     * @param eid  id of adapter
     * @param sid  id of server
     * @param cid  id of channel
     * @param mid  (айдишник сбщ раньше которого нужно получить список)
     * @param date (дата в мс раньше которой нужно получить список)
     */
    @ExportRequest
    public void requestMessagesBefore(final String eid, final String sid, final String cid, final String mid, final long date) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                if (networkMode == NETWORK_MODE_OFFLINE) return;

                Message[] messages = holder.get(eid).getMessagesBefore(sid, cid, mid, date, 50);

                if (messages != null && messages.length > 0) {

                    for(IServerListener listener : listeners) listener.onMessagesBeforeLoaded(false, eid, cid, date, messages);

                    if (useStorage) {
                        for (int i = 0; i < messages.length; i++) {
                            try {
                                storage.updateMessage(messages[i]);
                            } catch (SQLException e) {
                                onError(e);
                            }
                        }
                    }
                }
            }
        });
    }

    /**
     * Mark given channel as read.
     *
     * @param eid id of adapter
     * @param sid id of server
     * @param cid id of channel
     */
    @ExportRequest
    public void requestMarkRead(final String eid, final String sid, final String cid) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                if (useStorage) {
                    try {
                        storage.updateLastVisited(eid, sid, cid, System.currentTimeMillis());
                    } catch (SQLException e) {
                        onError(e);
                    }
                }

                Channel channel = null;
                if (useStorage) {
                    try {
                        channel = storage.getChannel(eid, sid, cid);
                    } catch (SQLException e) {
                        onError(e);
                    }
                }

                if (channel != null) {
                    channel.removeFlag(Channel.FLAG_UNREAD);
                    for(IServerListener listener : listeners) listener.onChannelUpdate(channel);
                }

                Adapter adapter = holder.get(eid);
                if (adapter != null) {
                    adapter.markRead(sid, cid);
                }
            }
        });
    }

    @ExportRequest
    public void requestChangeNickname(final String eid, final String sid, final String nickname) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                holder.get(eid).changeNickname(sid, nickname);
            }
        });
    }


    // send

    @ExportRequest
    public void sendTypingRequest(final String eid, final String sid, final String cid, final boolean b) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                holder.get(eid).setTypingState(sid, cid, b);
            }
        });
    }

    @ExportRequest
    public void sendInviteAccept(final String eid, final String invite) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                holder.get(eid).acceptInvite(invite);
            }
        });
    }

    @ExportRequest
    public void sendLeaveServer(final String eid, final String sid) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                holder.get(eid).leaveServer(sid);
            }
        });
    }

    public void sendMessage(final String eid, final String sid, final String cid, final EnhancedText message, final String[] attaches) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                holder.get(eid).sendMessage(sid, cid, message, attaches);
            }
        });
    }

    public void sendEditMessage(final String eid, final String sid, final String cid, final String mid, final EnhancedText text) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                holder.get(eid).editMessage(sid, cid, mid, text);
            }
        });
    }

    @ExportRequest
    public void sendDeleteMessage(final String eid, final String sid, final String cid, final String mid) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                holder.get(eid).deleteMessage(sid, cid, mid);
            }
        });
    }

    @ExportRequest
    public void sendServerSwitch(final String eid, final String sid) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                holder.get(eid).sendSeverEventsSwitch(sid);
            }
        });
    }

    @ExportRequest
    public void sendInlineClick(final String eid, final String sid, final String cid, final String mid, final String data) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                holder.get(eid).inlineCallback(sid, cid, mid, data);
            }
        });
    }

    @ExportRequest
    public void test(final String m) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                for(IServerListener listener : listeners) listener.onUploadProgress(m, 0);
            }
        });
    }

    // =============== //
    // REALTIME EVENTS //
    // =============== //

    @Override
    public void onServerCreate(Server... server) {
        for(IServerListener listener : listeners) listener.onServerCreate(server);
    }

    @Override
    public void onServerUpdate(Server... server) {
        for(IServerListener listener : listeners) listener.onServerUpdate(server);

        if (useStorage) {
            try {
                for(Server si : server)storage.updateServer(si);
            } catch (SQLException e) {
                onError(e);
            }
        }
    }

    @Override
    public void onServerRemove(Server... server) {
        for(IServerListener listener : listeners) listener.onServerRemove(server);
    }

    @Override
    public void onChannelUpdate(Channel... channel) {
        if (channel != null) {
            for(IServerListener listener : listeners) listener.onChannelUpdate(channel);

            if (useStorage) {
                try {
                    storage.updateChannel(channel);
                } catch (SQLException e) {
                    onError(e);
                }
            }
        }
    }

    @Override
    public void onChannelDelete(String... channels) {
        for(IServerListener listener : listeners) listener.onChannelDelete(channels);
    }

    @Override
    public void onMessageUpdate(Message message) {
        for(IServerListener listener : listeners) listener.onMessageUpdate(message);
    }

    @Override
    public void onMessageDelete(String eid, String guildId, String channelId, String messageId) {
        // TODO: mark message as deleted

        for(IServerListener listener : listeners) listener.onMessageDelete(eid, guildId, channelId, messageId);
    }

    @Override
    public void onMessageMetaPatch(MessageMetaPatch[] patches) {
        for(IServerListener listener : listeners) listener.onMessageMetaPatch(patches);
    }

    @Override
    public void onReadStateUpdate(String eid, String sid, String cid, String lastReadMessage) {
        for(IServerListener listener : listeners) listener.onReadStateUpdate(eid, sid, cid, lastReadMessage);
    }

    @Override
    public void onUserUpdate(User user) {
        for(IServerListener listener : listeners) listener.onUserUpdate(user);

        if (useStorage) {
            try {
                storage.updateUser(user);
            } catch (SQLException e) {
                onError(e);
            }
        }
    }

    @Override
    public void onUserPresenceUpdate(String server, String channel, User user) {
        for(IServerListener listener : listeners) listener.onUserPresenceUpdate(server, channel, user);

        if (useStorage) {
            try {
                storage.updateUser(user);
            } catch (SQLException e) {
                onError(e);
            }
        }
    }

    @Override
    public void onError(Throwable e) {
        for(IServerListener listener : listeners) listener.onError(e);
    }

    @Override
    public void onTypingState(String userId, boolean isBecameActive, String eid, String serverId, String channelId) {
        for(IServerListener listener : listeners) listener.onTypingState(userId, isBecameActive, eid, serverId, channelId);
    }

    @Override
    public void onRealtimeState(int state, String extid) {
        for(IServerListener listener : listeners) listener.onRealtimeState(state, extid);
    }

    @Override
    public void onUploadProgress(String filename, int progress) {
        for(IServerListener listener : listeners) listener.onUploadProgress(filename, progress);
    }
}

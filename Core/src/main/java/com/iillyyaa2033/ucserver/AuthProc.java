package com.iillyyaa2033.ucserver;

public class AuthProc {

    private AuthStep[] steps;

    public AuthProc(AuthStep... steps){
        this.steps = steps;
    }

    public AuthStep getStep(int i){
        if(i >= 0 && i < steps.length){
            return steps[i];
        } else {
            return null;
        }
    }

    public static class AuthStep {
        public final String name;

        public AuthStep(String name){
            this.name = name;
        }
    }

    public static class MessageStep extends AuthStep {

        public static final String NAME = "MessageStep";

        public final String message;

        public MessageStep(String message){
            super(NAME);
            this.message = message;
        }

    }

    public static class WebHeadersStep extends AuthStep {

        public static final String NAME = "WebHeadersStep";

        public final String startPage;
        public final String headerName;
        public final String destination;

        public String color;

        public WebHeadersStep(String startUrl, String headerHame){
            super(NAME);

            this.startPage = startUrl;
            this.headerName = headerHame;
            this.destination = "header";
        }

        public WebHeadersStep withBackgroundColor(String color){
            this.color = color;
            return this;
        }
    }

    public static class WebUrlStep extends AuthStep {

        public static final String NAME = "WebUrlStep";

        public final String startUrl;
        public final String successUrl;
        public final String destination;
        public final String errorUrl;

        public WebUrlStep(String startUrl, String successUrl, String errorUrl){
            super(NAME);

            this.startUrl = startUrl;
            this.successUrl = successUrl;
            this.errorUrl = errorUrl;
            this.destination = "url";
        }

    }

    public static class WebCookiesStep extends AuthStep {

        public static final String NAME = "WebCookiesStep";

        public final String startUrl;
        public final String destUrl;
        public final String destination;

        public WebCookiesStep(String startUrl, String destUrl){
            super(NAME);

            this.startUrl = startUrl;
            this.destUrl = destUrl;

            this.destination = "cookies";
        }
    }

    public abstract static class AdapterSideStep extends AuthStep {

        public interface IUICallback {

            String getString(String hint);
            void block(String m);
            void setResult(String key, String value);
            void message(String m);

        }

        public static final String NAME = "AdapterSideStep";

        private int code;

        public AdapterSideStep() {
            super(NAME);
        }

        public abstract boolean step(IUICallback callback);
    }

    public static class FinalStep extends AuthStep {

        public static final String NAME = "FinalStep";

        public FinalStep(){
            super(NAME);
        }

    }

    public static class FailStep extends AuthStep {

        public static final String NAME = "FailStep";

        public FailStep(){
            super(NAME);
        }
    }
}

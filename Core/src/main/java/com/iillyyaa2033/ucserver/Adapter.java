package com.iillyyaa2033.ucserver;

import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.ConnectionSettings;
import com.iillyyaa2033.ucmodel.EnhancedText;
import com.iillyyaa2033.ucmodel.Message;
import com.iillyyaa2033.ucmodel.Server;
import com.iillyyaa2033.ucmodel.ServerInfo;
import com.iillyyaa2033.ucmodel.User;

public abstract class Adapter {

    public static final int REALTIME_STARTING = 0;
    public static final int REALTIME_STARTED = 1;

    public static final String SERVER_ID = "hardcoded";

    private ConnectionSettings settings;
    private AdapterCallback callback = new AdapterCallback();
    private long featureSet = 0;

    private String eid;

    public final void setEid(String eid) {
        this.eid = eid;
        callback.setEid(eid);
    }

    public final String getEId() {
        return this.eid;
    }

    @Override
    public final String toString() {
        return eid;
    }

    final void setListener(IEventsListener server) {
        callback.setListener(server);
    }

    public final AdapterCallback callback() {
        return callback;
    }

    public final void setSettings(ConnectionSettings settings) {
        this.settings = settings;
    }

    public final ConnectionSettings getSettings() {
        return settings;
    }

    protected final void setFeatureSet(long fs) {
        this.featureSet = fs;
    }

    public final boolean hasFeature(long feature) {
        return (featureSet & feature) != 0;
    }

    // ======================= //
    // ADAPTER-SPECIFIC THINGS //
    // ======================= //
    public Object onSaveState() {
        return null;
    }

    public void onRestoreState(Object state) {
    }

    public void onConnectionSettingsChanged(ConnectionSettings settings){

    }

    public boolean isRealtime() {
        return true;
    }

    public void startRealtime() {
    }

    public void stopRealtime() {
    }

    public abstract User getAccountUser();

    public abstract String getServiceName();

    // ==================== //
    // REQUESTS FROM CLIENT //
    // ==================== //
    public Server[] getServers() {
        // This is default implementation that should be overriden by adapter
        Server server = new Server(getEId(), SERVER_ID, getServiceName());
        server.flags = server.flags | Server.FLAG_HARDCODED;
        return new Server[]{server};
    }

    public ServerInfo[] getServerInfo(String sid){
        return new ServerInfo[] {
                new ServerInfo("eid", getEId()),
                new ServerInfo("sid", sid)
        };
    }

    public abstract Channel[] getChannels(String serverId);

    public void changeNickname(String serverId, String nick) {
        throw new UnimplementedException();
    }

    public void leaveServer(String serverId) {
        throw new UnimplementedException();
    }

    public abstract Message[] getLastMessages(String serverId, String channelId);

    public abstract Message[] getMessagesBefore(String serverId, String channelId, String mid, long since, int count);

    public abstract Message[] getMessagesSince(String serverId, String channelId, long since, int count);

    public abstract void sendMessage(String sid, String cid, EnhancedText mentionedText, String[] attachments);

    public void editMessage(String serverId, String channelId, String messageId, EnhancedText replacement) {
        throw new UnimplementedException();
    }

    public void deleteMessage(String serverId, String channelId, String messageId) {
        throw new UnimplementedException();
    }

    public void inlineCallback(String serverId, String channelId, String messageId, String data) {

    }

    /**
     * Called when client switched server
     * <p>
     * Client wants to stop listening "special events" on old channels and start listening
     * on given channel.
     * </p><p>
     * For now "special events" is typing and activities changes (this is strongly related
     * to discord).
     * </p>
     */
    public void sendSeverEventsSwitch(String serverId) {
    }

    public void markRead(String sid, String cid) {
    }

    /**
     * Notify server our user typing on specific channel.
     * <p>
     * Повторный вызов с isTyping=true на одном сервере отменяет действие предыдущего.
     * </p>
     *
     * @param serverId  -
     * @param channelId -
     * @param isTyping  -
     */
    public void setTypingState(String serverId, String channelId, boolean isTyping) {
    }

    public String getInviteInfo(String invite) {
        throw new UnimplementedException();
    }

    public void acceptInvite(String invite) {
        throw new UnimplementedException();
    }


    public static class Feature {

        /**
         * Add "add picture" icon on message form
         */
        public static final long FILE_UPLOAD = 1;

        /**
         * Unused for now
         */
        public static final long EMOJI_KEYBOARD = 1 << 2;

        /**
         * Add "change nickname" entry into server's menu
         */
        public static final long PER_SERVER_NICKNAME = 1 << 3;

        /**
         * Add "join by invite" entry into adapter's menu
         */
        public static final long JOIN_SERVER_INVITE = 1 << 4;

        /**
         * Prevents local server from detecting unread channels/servers by comparison
         * last message time and last visit time.
         */
        public static final long EXCPLICT_UNREADS = 1 << 5;

    }

    public static class UploadProgress {

        public static final int ENQUEUED = -4;
        public static final int STARTED = -1;
        public static final int WAIT_SERVER = -2;
        public static final int DONE = -3;

    }

    public class UnimplementedException extends RuntimeException {
        UnimplementedException() {
            super("Unimplemented yet");
        }
    }
}

package com.iillyyaa2033.ucserver;

import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.MessageMetaPatch;
import com.iillyyaa2033.ucmodel.Server;
import com.iillyyaa2033.ucmodel.Message;
import com.iillyyaa2033.ucmodel.User;

/**
 * Events sent by adapter
 */
public interface IEventsListener {

    void onServerCreate(Server... server);
    void onServerUpdate(Server... server);
    void onServerRemove(Server... server);

    void onChannelUpdate(Channel... channel);
    void onChannelDelete(String... channels);

    void onMessageUpdate(Message message);

    void onMessageDelete(String eid, String guildId, String channelId, String messageId);

    /**
     * Fired when some user names, avatars or colors have changed
     *
     * @param patches --
     */
    void onMessageMetaPatch(MessageMetaPatch[] patches);

    /**
     * Fired when channel's read state changed.
     *
     * You have to set flag Message.FLAG_UNREAD to 0 for all messages up to lastReadMessage
     *
     * @param eid -
     * @param sid -
     * @param cid -
     * @param lastReadMessage can be null
     */
    void onReadStateUpdate(String eid, String sid, String cid, String lastReadMessage);

    void onUserUpdate(User user);

    void onUserPresenceUpdate(String server, String channel, User user);

    void onError(Throwable e);

    void onTypingState(String userId, boolean isBecameActive, String eid, String serverId, String channelId);

    void onRealtimeState(int state, String extid);

    void onUploadProgress(String filename, int progress);

}

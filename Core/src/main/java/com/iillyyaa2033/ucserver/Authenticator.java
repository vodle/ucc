package com.iillyyaa2033.ucserver;

import com.iillyyaa2033.ucmodel.ConnectionSettings;

import java.util.Map;

public abstract class Authenticator {

    public abstract AuthProc getAuthProc();
    public abstract ConnectionSettings finishAuthProc(Map<String, String> results);
}

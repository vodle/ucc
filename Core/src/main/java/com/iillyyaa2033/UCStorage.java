package com.iillyyaa2033;

import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.ConnectionSettings;
import com.iillyyaa2033.ucmodel.Message;
import com.iillyyaa2033.ucmodel.Server;
import com.iillyyaa2033.ucmodel.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UCStorage {

    private final Connection connection;

    private UCStorage(String path) throws SQLException {

        // Manually load driver
        org.h2.Driver.load();

        connection =  DriverManager.getConnection("jdbc:h2:"+path+";DEFAULT_LOCK_TIMEOUT=1000", "sa", "");


        checkOrCreateTable("servers",
                "eid varchar(128), id varchar(128), name text, icon text",
                new String[]{"eid", "id"});

        checkOrCreateTable("channels",
                "eid varchar(128), sid varchar(128), cid varchar(128), " +
                        "flags long, title text, lastMessage text, lastMessageDate text, " +
                        "draw_mode integer, priority integer, parent_id text, icon text",
                new String[]{"eid", "sid", "cid"});

        checkOrCreateTable("channels_last_visited",
                "eid varchar(128), sid varchar(128), cid varchar(128), time long",
                new String[]{"eid", "sid", "cid"});

        checkOrCreateTable("messages",
                "eid varchar(128), sid varchar(128), cid varchar(128), mid varchar(128), " +
                        "uid varchar(128), date bigint, content text",
                null);

        checkOrCreateTable("attachments",
                "completeId text, type integer, title text, description text," +
                        "thumbUrl text, actionUrl text, width integer, height integer",
                null);

        checkOrCreateTable("users",
                "eid varchar(128), id varchar(128), username text, icon_res text",
                new String[]{"eid", "id"});

        checkOrCreateTable("adapters",
                "eid varchar(128), target text, proxy text, ua text, token text",
                null);

        checkOrCreateTable("aprc",
                "eid varchar(128), sid varchar(128), cid varchar(128), uid varchar(128), " +
                        "name text, color int, icon text",
                new String[]{"eid", "sid", "cid", "uid"});
    }

    /**
     * Creates new table if not exists
     *
     * @param name -
     * @param fields -
     * @param uniqueIndexes unused
     * @throws SQLException on error
     */
    public void checkOrCreateTable(String name, String fields, String[] uniqueIndexes) throws SQLException {
        try{
            connection.createStatement().execute("SELECT 1 FROM "+name+" LIMIT 1;");
        } catch (Exception e){
            System.out.println("Creating table "+name);
            connection.createStatement().execute("CREATE TABLE "+name+"("+fields+");");
        }
    }

    public void exec(String base, String... args) throws SQLException {
        connection.createStatement().execute(String.format(base, (Object[]) args));
    }

    public void execParametrized(String query, String... params) throws SQLException {
        PreparedStatement stmt = connection.prepareStatement(query);
        for(int i = 0; i < params.length; i++){
            stmt.setString(i+1, params[i]);
        }
        stmt.executeUpdate();
    }

    // ====== //
    // DB API //
    // ====== //
    public static UCStorage connect(String path) throws SQLException {
        // TODO: prepare statements only once -- when creating connection
        return new UCStorage(path);
    }

    public void disconnect() throws SQLException {
        connection.close();
    }

    public void delMyData() throws SQLException {
        exec("drop table servers;");
        exec("drop table channels;");
        exec("drop table messages;");
        exec("drop table attachments;");
        exec("drop table users;");
    }

    public void delAdapter(String eid) throws SQLException {
        exec("delete from servers where eid = '"+eid+"'");
        exec("delete from channels where eid = '"+eid+"'");
        exec("delete from messages where eid = '"+eid+"'");
//        exec("delete from attachments where eid = '"+eid+"'");
        exec("delete from users where eid = '"+eid+"'");
        exec("delete from adapters where eid = '"+eid+"'");
    }

    // ======= //
    // SERVERS //
    // ======= //
    public void updateServer(Server info) throws SQLException {
        execParametrized("merge into servers(eid, id, name, icon) key(eid, id) " +
                    "values (?, ?, ?, ?);", info.eid, info.sid, info.name, info.icon_res);
    }

    public void updateServers(String eid, Server[] servers) throws SQLException{
        for(Server info : servers){
            updateServer(info);
        }
    }

    public Server[] getServers(String eid) throws SQLException {
        ArrayList<Server> result = new ArrayList<>();

        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("select * from servers where eid = '"+eid+"'");

        while (rs.next()) {
            Server i = new Server(rs.getString(1),rs.getString(2), rs.getString(3));
            i.icon_res = rs.getString(4);
            result.add(i);
        }

        return result.toArray(new Server[0]);
    }


    // ======== //
    // CHANNELS //
    // ======== //
    public void updateChannel(Channel... channels) throws SQLException {

        for(Channel channel : channels)
            execParametrized("merge into " +
                    "channels(eid, sid, cid, flags, title, lastMessage, lastMessageDate, priority, parent_id, draw_mode, icon) " +
                    "key (eid, sid, cid)" +
                    "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);",
                    channel.eid, channel.sid, channel.cid, channel.flags+"", channel.title,
                    channel.last_msg, ""+channel.last_time,
                    ""+channel.priority, channel.parent_id, ""+channel.draw_mode, channel.icon);


    }

    public void updateChannels(String eid, String sid, Channel[] channels) throws SQLException{

        for(Channel channel : channels) {
            updateChannel(channel);

        }
    }

    public Channel[] getChannels(String eid, String sid) throws SQLException {
        ArrayList<Channel> result = new ArrayList<>();

        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("select * from channels where eid = '"+eid+"' and sid = '"+sid+"'");

        while (rs.next()) {
            Channel i = new Channel(rs.getString(1),rs.getString(2), rs.getString(3),
                    rs.getString(5),rs.getString(6), rs.getLong(7));
            i.flags = rs.getLong(4);
            i.draw_mode = rs.getInt(8);
            i.priority = rs.getLong(9);
            i.parent_id = rs.getString(10);
            i.icon = rs.getString(11);
            result.add(i);
        }

        return result.toArray(new Channel[0]);
    }

    public Channel getChannel(String eid, String sid, String cid) throws SQLException {
        Channel result = null;

        Statement stmt = connection.createStatement();

        ResultSet rs = stmt.executeQuery("select * from channels where eid = '"+eid+"' and sid = '"+sid+"' and cid = '"+cid+"'");

        if (rs.next()) {
            Channel i = new Channel(rs.getString(1),rs.getString(2), rs.getString(3),
                    rs.getString(5),rs.getString(6), rs.getLong(7));
            i.flags = rs.getLong(4);
            i.draw_mode = rs.getInt(8);
            i.priority = rs.getLong(9);
            i.parent_id = rs.getString(10);
            i.icon = rs.getString(11);
            result = i;
        }

        return result;
    }

    public void updateLastVisited(String eid, String sid, String cid, long time) throws SQLException {
        execParametrized("merge into channels_last_visited(eid, sid, cid, time) " +
                "key (eid, sid, cid) " +
                "values (?, ?, ?, ?);",eid, sid, cid, ""+time);
    }

    public long getLastVisited(String eid, String sid, String cid) throws SQLException {
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("select time from channels_last_visited" +
                " where eid = '"+eid+"' and sid = '"+sid+"' and cid = '"+cid+"'");

        while (rs.next()) {
            return rs.getLong(1);
        }

        return 0;
    }


    // ======== //
    // MESSAGES //
    // ======== //
    public void updateMessage(Message message) throws SQLException {
        // TODO: add/update message
    }


    // ===== //
    // USERS //
    // ===== //
    public void updateUser(User user) throws SQLException {
        execParametrized("merge into users(eid, id, username, icon_res) key(eid, id) values (?, ?, ?, ?);",
                user.eid, user.uid, user.username, user.icon_res);
    }

    public User[] getUsers() throws SQLException {
        ArrayList<User> users = new ArrayList<>();

        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("select * from users;");

        while(rs.next()){
            users.add(new User(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
        }

        return users.toArray(new User[users.size()]);
    }

    public User getUser(String id) throws SQLException {
        User user = null;

        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("select * from users where id = '"+id+"';");

        if(rs.next()){
            user = new User(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
        }

        return user;
    }


    // ======== //
    // ADAPTERS //
    // ======== //
    public void addAdapter(ConnectionSettings settings) throws SQLException {
        execParametrized("insert into adapters(eid, target, proxy, ua, token) values (?, ?, ?, ?, ?);",
                settings.getEid(), settings.getTarget(), settings.getProxy(), settings.formatUserAgentForDB(), settings.getToken());
    }

    public ConnectionSettings[] getAdapters() throws SQLException {
        ArrayList<ConnectionSettings> adapters = new ArrayList<>();

        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("select * from adapters;");

        while(rs.next()){
            ConnectionSettings setting = new ConnectionSettings(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4).split("\\|"),rs.getString(5));
            adapters.add(setting);
        }

        return adapters.toArray(new ConnectionSettings[adapters.size()]);
    }
}

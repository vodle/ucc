package com.iillyyaa2033;

import java.util.HashMap;

public class TypingCache {

    private static final int TYPERS_COUNT = 3;

    private HashMap<String, String[]> channels = new HashMap<>();

    public void addTyper(String eid, String sid, String cid, String typer){
        String key = eid+sid+cid;

        if(!channels.containsKey(key)){
            channels.put(key, new String[TYPERS_COUNT]);
        }

        String[] typers = channels.get(key);

        for(int i = 0; i < TYPERS_COUNT; i++){
            if(typers[i] == null){
                typers[i] = typer;
                break;
            } else if (typers[i].equals(typer)){
                break;
            }
        }
    }

    public void removeTyper(String eid, String sid, String cid, String typer){
        String key = eid+sid+cid;

        if(channels.containsKey(key)){

            String[] typers = channels.get(key);

            for(int i = 0; i < TYPERS_COUNT; i++){
                if(typer.equals(typers[i])){
                    typers[i] = null;
                }
            }
        }
    }

    public String[] getTypersAtChannel(String eid, String sid, String cid){
        String key = eid+sid+cid;

        if(channels.containsKey(key)){
            return channels.get(key);
        } else {
            return new String[TYPERS_COUNT];
        }
    }

}

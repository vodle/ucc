package ucc.discord;

import com.iillyyaa2033.discord.DiscordUtils;
import com.iillyyaa2033.discord.objects.ChannelOverride;
import com.iillyyaa2033.discord.objects.Guild;
import com.iillyyaa2033.discord.objects.ReadState;
import com.iillyyaa2033.discord.objects.Ready;
import com.iillyyaa2033.discord.objects.Channel;
import com.iillyyaa2033.discord.objects.Relationship;
import com.iillyyaa2033.discord.objects.User;
import com.iillyyaa2033.discord.objects.UserGuildSettings;

public class ReadyHandler {

    private Ready ready;

    public ReadyHandler(Ready ready) {
        this.ready = ready;
    }

    Ready getReady(){
        return ready;
    }

    User getMe() {
        if (ready == null) return null;

        return ready.user;
    }

    Guild[] getGuilds() {
        if (ready == null) return null;

        return ready.guilds;
    }

    Guild getGuild(String guildId) {
        if (ready == null) return null;

        for (Guild guild : ready.guilds) {
            if (guild.id.equals(guildId)) {
                return guild;
            }
        }

        return null;
    }

    int getGuildUnread(String guildId) {
        if (ready == null) return 0;

        int cnt = 0;

        if (DiscordAdapter.SERVER_ID.equals(guildId)) {
            for (Channel channel : ready.private_channels) {
                cnt += getChannelMentionCount(channel.id);
            }
        } else {
            Guild guild = getGuild(guildId);
            if (guild != null) {
                for (Channel channel : guild.channels) {
                    cnt += getChannelMentionCount(channel.id);
                }
            }
        }

        return cnt;
    }

    int getGuildPosition(String guildId) {
        if (ready == null) return 0;

        int idx = 0;
        for (String s : ready.user_settings.guild_positions) {
            if (s.equals(guildId)) break;
            idx++;
        }

        return idx;
    }

    Channel[] getGuildChannels(String guildId) {
        if (ready == null) return null;

        Guild guild = getGuild(guildId);
        if (guild == null) return null;
        return guild.channels;
    }

    Channel[] getDMChannels() {
        if (ready == null) return null;

        return ready.private_channels;
    }

    Channel getChannel(String channelId) {
        if (ready == null) return null;

        for (Channel ch : ready.private_channels) {
            if (ch.id.equals(channelId)) return ch;
        }

        for (Guild guild : ready.guilds) {
            for (Channel ch : guild.channels) {
                if (ch.id.equals(channelId)) {
                    // TODO: this is bad code, guild of channel should be set when Ready object received
                    ch.guild_id = guild.id;
                    return ch;
                }
            }
        }

        return null;
    }

    ChannelOverride getChannelOverride(String guildId, String channelId){
        if (ready == null) return null;

        for (UserGuildSettings settings : ready.user_guild_settings) {
            if(settings.guild_id.equals(guildId)){
                for(ChannelOverride override : settings.channel_overrides){
                    if(override.channel_id.equals(channelId))
                        return override;
                }
            }
        }

        return null;
    }

    boolean isChannelUnread(Channel channel) {
        if (ready == null) return false;

        if (channel.type == 2 || channel.type == 4 || channel.type == 6) return false;

        for (ReadState state : ready.read_state) {
            if (state.id.equals(channel.id)) {
                long comp = DiscordUtils.getMillis(state.last_message_id) - DiscordUtils.getMillis(channel.last_message_id);
                return comp < 0;
            }
        }

        return true;
    }

    String getLastMessageId(String channelId) {
        if (ready == null) return null;

        Channel channel = getChannel(channelId);

        if (channel == null) return null;
        else return channel.last_message_id;
    }

    void setLastMessageId(String channelId, String message) {
        if (ready == null) return;

        Channel channel = getChannel(channelId);

        if (channel == null) return;
        else channel.last_message_id = message;
    }

    int getChannelMentionCount(String channelId) {
        if (ready == null) return 0;

        for (ReadState state : ready.read_state) {
            if (state.id.equals(channelId)) {
                return state.mention_count;
            }
        }

        return 0;
    }

    void setChannelMentionCount(String channelId, int mentions){
        if(ready == null) return;

        for (ReadState state : ready.read_state) {
            if (state.id.equals(channelId)) {
                state.mention_count = mentions;
            }
        }
    }

    void setChannelLastMessage(String channelId, String messageId) {
        if (ready == null) return;

        for (ReadState state : ready.read_state) {
            if (state.id.equals(channelId)) {
                state.last_message_id = messageId;
            }
        }

    }

    int getRelationship(String userId){
        if(ready == null) return 0;

        for(Relationship relation : ready.relationships){
            if(relation.user.id.equals(userId)){
                return relation.type;
            }
        }

        return 0;
    }
}

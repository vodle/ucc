package ucc.discord;

import com.iillyyaa2033.ucmodel.ConnectionSettings;
import com.iillyyaa2033.ucserver.Adapter;
import com.iillyyaa2033.ucserver.AuthProc;
import com.iillyyaa2033.ucserver.Authenticator;
import com.iillyyaa2033.ucserver.Module;

import java.util.Map;

public class DiscordModule extends Module {

    @Override
    public Descr getDescription() {
        return new Descr("ucc.discord.DiscordAdapter","Discord","Status: stable");
    }

    @Override
    public Authenticator getAuthenticator() {
        return new Authenticator() {
            @Override
            public AuthProc getAuthProc() {
                return new AuthProc(
                        new AuthProc.MessageStep("Сейчас будет открыта страничка логина дискорда." +
                                "\n\nНеобходимо залогиниться, чтобы прога смогла вытащить юзерский токен." +
                                "\n\nВажно. Хоть я и пользуюсь этой прогой уже полгода (сейчас июль 2019) " +
                                "без проблем со стороны дискорда, есть ненулевой шанс получить бан 'просто " +
                                "потому что'. Это я так, на всякий случай."),
                        new AuthProc.WebHeadersStep(
                                "https://discordapp.com/login",
                                "authorization"
                        ),
                        new AuthProc.FinalStep()
                );
            }

            @Override
            public ConnectionSettings finishAuthProc(Map<String, String> results) {
                return new ConnectionSettings(
                        "ucc.discord.DiscordAdapter",
                        null,
                        new String[]{"Mozilla", "Linux", "custom", "custom"},
                        results.get("header")
                );
            }
        };
    }

    @Override
    public Adapter getAdapter() {
        return new DiscordAdapter();
    }
}

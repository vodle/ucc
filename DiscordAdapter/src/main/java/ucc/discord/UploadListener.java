package ucc.discord;

import com.iillyyaa2033.ucserver.Adapter;
import com.iillyyaa2033.utils.HttpUtils;

public class UploadListener implements HttpUtils.IUploadListener {

    private Adapter adapter;
    private String filename;

    private long lastProgress = -1;

    UploadListener(Adapter adapter, String filename){
        this.adapter = adapter;
        this.filename = filename;
    }

    @Override
    public void started() {
        adapter.callback().sendUploadProgress(filename, Adapter.UploadProgress.STARTED);
    }

    @Override
    public void progress(long l, long l1) {
        long percent = l / l1;

//        adapter.sendUploadProgress(filename, (int) percent);
    }

    @Override
    public void waitingServer() {
        adapter.callback().sendUploadProgress(filename, Adapter.UploadProgress.WAIT_SERVER);
    }

    @Override
    public void done() {
        adapter.callback().sendUploadProgress(filename, Adapter.UploadProgress.DONE);
    }
}
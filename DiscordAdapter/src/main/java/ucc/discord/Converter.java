package ucc.discord;

import com.iillyyaa2033.discord.DiscordSettings;
import com.iillyyaa2033.discord.DiscordUtils;
import com.iillyyaa2033.discord.objects.Attachment;
import com.iillyyaa2033.discord.objects.Embed;
import com.iillyyaa2033.discord.objects.Guild;
import com.iillyyaa2033.discord.objects.GuildMember;
import com.iillyyaa2033.discord.objects.Reaction;
import com.iillyyaa2033.discord.objects.Role;
import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.ConnectionSettings;
import com.iillyyaa2033.ucmodel.EnhancedText;
import com.iillyyaa2033.ucmodel.Message;
import com.iillyyaa2033.ucmodel.MessagePartAttachment;
import com.iillyyaa2033.ucmodel.MessagePartInline;
import com.iillyyaa2033.ucmodel.MessagePartText;
import com.iillyyaa2033.ucmodel.Server;
import com.iillyyaa2033.ucmodel.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Converter {

    private static final String customEmojiPattern = "<a?:.+?:[0-9]+>";
    private static final String customEmojiPath = "https://cdn.discordapp.com/emojis/";

    private DiscordAdapter adapter;

    Converter(DiscordAdapter adapter) {
        this.adapter = adapter;
    }

    DiscordSettings settings(ConnectionSettings settings) {
        return new DiscordSettings(settings.getUserAgent(), settings.getToken(), settings.getProxy());
    }

    Message message(com.iillyyaa2033.discord.objects.Message msg, GuildMember cachedMember, HashMap<String, Role> cacheRoles) {
        String validGuild = msg.guild_id == null ? DiscordAdapter.SERVER_ID : msg.guild_id;

        Message m = new Message(adapter.getEId(), validGuild, "" + msg.channel_id, "" + msg.id,
                msg.author.id, DiscordUtils.getMillis(msg.id));

        m.updateByAppearance(msg.author.username, DiscordUtils.formatUserAvatarUrl(msg.author), 0);

        if (cachedMember != null) {
            String nick = cachedMember.nick;
            if (nick != null && nick.trim().equals("null")) nick = null;
            int color = getColorFromRoles(cachedMember.roles, cacheRoles);
            m.updateByAppearance(nick, DiscordUtils.formatUserAvatarUrl(msg.author), color);
        }


        // Text content
        MessagePartText text = getTextPart(msg);
        if (text != null) m.append(text);


        // Attachmants
        for (Attachment attach : msg.attachments) m.append(attachment(attach));
        for (Embed embed : msg.embeds) m.append(attachment(embed));


        // Edited flag
        if (msg.edited_timestamp != null) m.flags = m.flags | Message.FLAG_EDITED;


        // Reactions
        if (msg.reactions != null && msg.reactions.length > 0) {
            MessagePartInline inline = new MessagePartInline();

            MessagePartInline.InlineButton[] buttons = new MessagePartInline.InlineButton[msg.reactions.length];
            for (int i = 0; i < msg.reactions.length; i++) {
                Reaction reaction = msg.reactions[i];
                MessagePartInline.InlineButton btn = new MessagePartInline.InlineButton();

                String emoji, callbackEmoji;
                if (reaction.emoji.id == null || "null".equals(reaction.emoji.id)) {
                    emoji = reaction.emoji.name;
                    callbackEmoji = reaction.emoji.name;
                } else {
                    emoji = "![:?:](" + DiscordUtils.formatEmojiUrl(reaction.emoji.id) + ")";
                    callbackEmoji = reaction.emoji.name + ":" + reaction.emoji.id;
                }

                btn.text = emoji + "  " + reaction.count;
                btn.isActive = reaction.me;
                btn.callbackData = callbackEmoji;
                buttons[i] = btn;
            }

            inline.buttons = buttons;

            m.append(inline);
        }


        return m;
    }

    private MessagePartText getTextPart(com.iillyyaa2033.discord.objects.Message msg) {

        if (msg.content != null && !msg.content.isEmpty()) {
            String content = msg.content;

            EnhancedText text = new EnhancedText();

            // Usernames
            if (msg.mentions != null) {
                for (com.iillyyaa2033.discord.objects.User u : msg.mentions) {
                    String uid = u.id;
                    String username = u.username;
                    String name = username + "#" + u.discriminator;
                    content = content.replace("<@!" + uid + ">", "@" + name);
                    content = content.replace("<@" + uid + ">", "@" + name);

                    text.mentions.put(uid, "@" + name);
                    text.fancyNames.put(uid, "@" + username);
                }
            }


            // Emojis
            Pattern pattern = Pattern.compile(customEmojiPattern);
            Matcher matcher = pattern.matcher(content);

            while (matcher.find()) {
                // Matcher fill search for something like '<:thonking_2:535901520399433728>' -- standart emoji
                // '<a:thonking_2:535901520399433728>' -- standart emoji animated emoji

                String[] emoji = content.substring(matcher.start() + 2, matcher.end() - 1).split(":");

                String replacement = "![:" + emoji[0] + ":](" + customEmojiPath + emoji[1] + ".png)";

                text.otherReplacements.put(matcher.group(), replacement);
            }


            // Channels
            // TODO: finish this
            if (msg.mention_roles != null) {

            }

            // Channels
            // TODO: finish this
            if (msg.mention_channels != null) {

            }

            text.text = content;
            return new MessagePartText(text);
        }


        return null;
    }

    private MessagePartAttachment attachment(Attachment attachment) {
        MessagePartAttachment attach = new MessagePartAttachment();

        attach.type = DiscordUtils.guessType(attachment.filename);
        attach.actionUrl = attachment.url;
        attach.thumbUrl = attachment.proxy_url;
        attach.h = attachment.height;
        attach.w = attachment.width;
        attach.title = attachment.id;
        attach.description = attachment.filename;

        if(attach.thumbUrl == null || attach.thumbUrl.trim().isEmpty())
            attach.thumbUrl = attachment.url;

        return attach;
    }

    private MessagePartAttachment attachment(Embed embed) {
        MessagePartAttachment attach = new MessagePartAttachment();

        attach.title = embed.title;
        attach.description = embed.description;
        attach.type = 1; // 'URL' type
        attach.actionUrl = embed.url;
        attach.thumbUrl = embed.url;

        if(embed.image != null){
            attach.w = embed.image.width;
            attach.h = embed.image.height;
            attach.thumbUrl = embed.image.proxy_url;
        }

        return attach;
    }

    User user(com.iillyyaa2033.discord.objects.User user) {
        return new User(adapter.getEId(), user.id, user.username, DiscordUtils.formatUserAvatarUrl(user));
    }

    Channel channel(String guild, com.iillyyaa2033.discord.objects.Channel dch) {
        long millis = dch.last_message_id == null ? 0 : DiscordUtils.getMillis(dch.last_message_id);

        Channel ch = new Channel(adapter.getEId(), guild, dch.id, dch.name, "", millis);

        if (isDm(dch)) {
            ch.draw_mode = Channel.ChannelDrawMode.DEFAULT;
            ch.priority = -ch.last_time / 1000;
            if (dch.type == 1) {
                ch.icon = DiscordUtils.formatUserAvatarUrl(dch.recipients[0]);
                ch.title = dch.recipients[0].username;
            } else {
                ch.icon = DiscordUtils.formatChannelIconUrl(dch.id, dch.icon);
            }

        } else {
            ch.draw_mode = dch.type == 4 ? Channel.ChannelDrawMode.CATEGORY : Channel.ChannelDrawMode.MINIMIZED;
            ch.priority = dch.position;

            switch (dch.type){
                case 2:
                    ch.type = Channel.TYPE_VOICE;
                    break;
                case 4:
                    ch.type = Channel.TYPE_GROUP;
                    break;
                default:
                    ch.type = Channel.TYPE_TEXT;
            }
        }

        // TODO: flag unread?

        ch.parent_id = dch.parent_id;

        return ch;
    }

    private boolean isDm(com.iillyyaa2033.discord.objects.Channel dch) {
        return dch.type == 1 || dch.type == 3;
    }

    Server server(Guild g) {
        Server server = new Server(adapter.getEId(), g.id, g.name);
        server.icon_res = DiscordUtils.formatGuildIconUrl(g.id, g.icon);

        return server;
    }

    static int getColorFromRoles(String[] roleIDs, HashMap<String, Role> roles) {

        int hightestPosition = 0;
        int color = 0;

        for (String id : roleIDs) {
            Role role = roles.get(id);

            if (role != null) {
                if (role.position > hightestPosition) {
                    hightestPosition = role.position;
                    if (role.color != 0) color = role.color;
                }
            }
        }

        return color;
    }
}

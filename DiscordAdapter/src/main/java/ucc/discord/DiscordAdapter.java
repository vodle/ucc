package ucc.discord;

import com.iillyyaa2033.Utils;
import com.iillyyaa2033.discord.DiscordApi;
import com.iillyyaa2033.discord.DiscordApiRealtime;
import com.iillyyaa2033.discord.DiscordUtils;
import com.iillyyaa2033.discord.IRealtimeListener;
import com.iillyyaa2033.discord.objects.Ack;
import com.iillyyaa2033.discord.objects.ChannelOverride;
import com.iillyyaa2033.discord.objects.Emoji;
import com.iillyyaa2033.discord.objects.Guild;
import com.iillyyaa2033.discord.objects.GuildMember;
import com.iillyyaa2033.discord.objects.PresenceUpdate;
import com.iillyyaa2033.discord.objects.Ready;
import com.iillyyaa2033.discord.objects.Role;
import com.iillyyaa2033.discord.objects.VoiceState;
import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.ConnectionSettings;
import com.iillyyaa2033.ucmodel.EnhancedText;
import com.iillyyaa2033.ucmodel.Message;
import com.iillyyaa2033.ucmodel.Server;
import com.iillyyaa2033.ucmodel.ServerInfo;
import com.iillyyaa2033.ucmodel.User;
import com.iillyyaa2033.ucserver.Adapter;
import com.iillyyaa2033.ucmodel.MessageMetaPatch;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class DiscordAdapter extends Adapter implements IRealtimeListener {

    static final String SERVER_ID = "hardcoded-discord-pm";

    private DiscordApi discordApi;
    private DiscordApiRealtime realtimeApi;

    private Converter convert = new Converter(this);

    private String typingChannel = null;
    private Timer timer;

    // Caches
    private String lastSwitchedServer = null;

    /**
     * Map: (guild id + member id) to member object
     */
    private HashMap<String, GuildMember> members = new HashMap<>();

    /**
     * Map: role id - role object
     */
    private HashMap<String, Role> roles = new HashMap<>();
    private Channel[] lastChannels = {};

    private ReadyHandler ready = new ReadyHandler(null);

    DiscordAdapter(){
        long featureSet =
                Feature.FILE_UPLOAD
                        | Feature.EMOJI_KEYBOARD
                        | Feature.PER_SERVER_NICKNAME
                        | Feature.JOIN_SERVER_INVITE
                        | Feature.EXCPLICT_UNREADS;
        setFeatureSet(featureSet);
    }

    // ======= //
    // ADAPTER //
    // ======= //
    @Override
    public boolean isRealtime() {
        return timer != null;
    }

    @Override
    public void onConnectionSettingsChanged(ConnectionSettings settings) {
        super.setSettings(settings);

        if (discordApi != null) ; // TODO: destroy old api
        discordApi = new DiscordApi(convert.settings(getSettings()));
    }

    @Override
    public void startRealtime() {
        timer = new Timer();

        callback().sendRealtimeStarting();

        if (realtimeApi != null) {
            realtimeApi.stop();
        } else {
            realtimeApi = new DiscordApiRealtime(this, convert.settings(getSettings()));
            realtimeApi.setStartPresence(DiscordApiRealtime.StatusConst.STATUS_ONLINE, false, null);
        }

        realtimeApi.start();
    }

    @Override
    public void stopRealtime() {
        realtimeApi.stop();

        typingChannel = null;
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public Object onSaveState() {
        return ready.getReady();
    }

    @Override
    public void onRestoreState(Object state) {
        onRealtimeReady((Ready) state);
    }

    private synchronized com.iillyyaa2033.discord.objects.User getMe() throws IOException, JSONException {
        if (ready == null || ready.getMe() == null) {
            return discordApi.getMe();
        } else {
            return ready.getMe();
        }
    }

    @Override
    public User getAccountUser() {
        try {
            return convert.user(getMe());
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String getServiceName() {
        return "Discord";
    }

    @Override
    public Server[] getServers() {
        Server[] result = {};

        try {
            Guild[] guilds = (ready == null || ready.getGuilds() == null ) ? discordApi.getMyServers() : ready.getGuilds();
            result = new Server[guilds.length + 1];

            Server info = new Server(getEId(), SERVER_ID, "Discord PM");
            info.icon_res = Utils.getResUrl("icon_discord_256.png");
            info.flags = info.flags | Server.FLAG_HARDCODED;
            info.priority = -10;
            info.unread_count = ready.getGuildUnread(SERVER_ID);
            result[0] = info;


            for (int i = 0; i < guilds.length; i++) {
                Guild guild = guilds[i];
                result[i + 1] = convert.server(guild);
                result[i + 1].priority = ready.getGuildPosition(guild.id);
                result[i + 1].unread_count = ready.getGuildUnread(guild.id);

                if(guild.roles != null) {
                    for (Role role : guild.roles) {
                        roles.put(role.id, role);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            callback().sendError(e);
        }

        return result;
    }

    @Override
    public ServerInfo[] getServerInfo(String sid) {
        Guild guild = ready.getGuild(sid);

        return new ServerInfo[]{
                new ServerInfo("name", guild.name),
                new ServerInfo("region", guild.region),
                new ServerInfo("member_count", guild.member_count+""),
                new ServerInfo("channel_count", guild.channels.length+""),
                new ServerInfo("description", guild.description),
                new ServerInfo("premium_tier", guild.premium_tier+""),
                new ServerInfo("preferred_locale", guild.preferred_locale)
        };
    }

    @Override
    public Channel[] getChannels(String serverId) {

        try {
            com.iillyyaa2033.discord.objects.Channel[] channels;

            if (SERVER_ID.equals(serverId)) {
                channels = ready.getDMChannels();
                if(channels == null) channels = discordApi.getDMChannels();
            } else {
                channels = ready.getGuildChannels(serverId);
                if(channels == null) channels = discordApi.getChannels(serverId);
            }

            Channel[] rz = new Channel[channels.length];

            for (int i = 0; i < channels.length; i++) {
                rz[i] = convert.channel(serverId, channels[i]);
                rz[i].eid = getEId();
                rz[i].unread_count = ready.getChannelMentionCount(channels[i].id);
                if(ready.isChannelUnread(channels[i])) rz[i].set(Channel.FLAG_UNREAD);
                ChannelOverride override = ready.getChannelOverride(serverId, channels[i].id);
                if(override != null && override.muted) rz[i].set(Channel.FLAG_HIDDEN);
            }

            lastChannels = rz;

            return rz;
        } catch (Exception e) {
            callback().sendError(e);
        }

        return new Channel[0];
    }

    @Override
    public void changeNickname(String serverId, String nick) {
        try {
            discordApi.changeNickname(serverId, "@me", nick);
        } catch (IOException | JSONException e) {
            callback().sendError(e);
        }
    }

    @Override
    public void leaveServer(String serverId) {

        if (serverId.startsWith("hardcoded")) {
            callback().sendError(new Exception("Cannot leave PM"));
        } else {
            try {
                discordApi.leaveGuild(serverId);
            } catch (IOException e) {
                callback().sendError(e);
            }
        }

    }

    @Override
    public Message[] getLastMessages(String serverId, String channelId) {

        try {
            com.iillyyaa2033.discord.objects.Message[] rawMsgs = discordApi.getMessages(serverId, channelId);
            Message[] msgs = new Message[rawMsgs.length];

            for (int i = 0; i < rawMsgs.length; i++) {
                User usr = convert.user(rawMsgs[i].author);
                usr.eid = getEId();
                callback().sendUserUpdated(usr);

                msgs[i] = convert.message(rawMsgs[i], gm(serverId, usr.uid), roles);
                msgs[i].eid = getEId();

                if (getMe().id.equals(msgs[i].uid)) {
                    msgs[i].flags = msgs[i].flags |
                            Message.FLAG_EDITABLE |
                            Message.FLAG_DELETABLE;
                }

                // Discord may not send channel id or guild id, so we'll override it
                msgs[i].sid = serverId;
                msgs[i].cid = channelId;

                if(ready.getRelationship(msgs[i].uid) == 2) msgs[i].set(Message.FLAG_HIDDEN);
            }

            realtimeFindUnknownUsers(serverId, rawMsgs);

            return msgs;
        } catch (IOException | JSONException e) {
            callback().sendError(e);
        }

        return null;
    }

    private GuildMember gm(String sid, String uid) {
        return members.get(sid + uid);
    }

    @Override
    public Message[] getMessagesBefore(String serverId, String channelId, String mid, long since, int count) {

        try {

            com.iillyyaa2033.discord.objects.Message[] rawMsgs = discordApi.getMessagesBefore(channelId, DiscordUtils.getSnowlfake(since));
            Message[] msgs = new Message[rawMsgs.length];

            for (int i = 0; i < rawMsgs.length; i++) {
                User usr = convert.user(rawMsgs[i].author);
                usr.eid = getEId();
                callback().sendUserUpdated(usr);

                msgs[i] = convert.message(rawMsgs[i], gm(serverId, usr.uid), roles);
                msgs[i].eid = getEId();
                if (getMe().id.equals(msgs[i].uid)) {
                    msgs[i].flags = msgs[i].flags |
                            Message.FLAG_EDITABLE |
                            Message.FLAG_DELETABLE;
                }

                // Discord may not send channel id or guild id, so we'll override it
                msgs[i].sid = serverId;
                msgs[i].cid = channelId;

                if(ready.getRelationship(msgs[i].uid) == 2) msgs[i].set(Message.FLAG_HIDDEN);
            }

            realtimeFindUnknownUsers(serverId, rawMsgs);

            return msgs;

        } catch (Exception e) {
            callback().sendError(e);
        }

        return null;
    }

    @Override
    public Message[] getMessagesSince(String serverId, String channelId, long since, int count) {

        try {

            com.iillyyaa2033.discord.objects.Message[] rawMsgs = discordApi.getMessagesSince(channelId, DiscordUtils.getSnowlfake(since));
            Message[] msgs = new Message[rawMsgs.length];

            for (int i = 0; i < rawMsgs.length; i++) {
                User usr = convert.user(rawMsgs[i].author);
                usr.eid = getEId();
                callback().sendUserUpdated(usr);

                msgs[i] = convert.message(rawMsgs[i], gm(serverId, usr.uid), roles);
                msgs[i].eid = getEId();
                if (getMe().id.equals(msgs[i].uid)) {
                    msgs[i].flags = msgs[i].flags |
                            Message.FLAG_EDITABLE |
                            Message.FLAG_DELETABLE;
                }

                // Discord may not send channel id or guild id, so we'll override it
                msgs[i].sid = serverId;
                msgs[i].cid = channelId;

                if(ready.getRelationship(msgs[i].uid) == 2) msgs[i].set(Message.FLAG_HIDDEN);
            }

            realtimeFindUnknownUsers(serverId, rawMsgs);

            return msgs;

        } catch (Exception e) {
            callback().sendError(e);
        }

        return null;
    }

    private void realtimeFindUnknownUsers(String guild, com.iillyyaa2033.discord.objects.Message[] messages) {
        if(guild == null || SERVER_ID.equals(guild)) return;

        if (isRealtime()) {
            try {
                ArrayList<String> ids = new ArrayList<>();

                for (com.iillyyaa2033.discord.objects.Message message : messages) {
                    if (!ids.contains(message.author.id) && !members.containsKey(guild + message.author.id)) {
                        ids.add(message.author.id);
                    }
                }

                if (ids.size() > 0) {
                    realtimeApi.sendRequestGuildMembers(new String[]{guild}, null, 0, ids.toArray(new String[0]));
                }
            } catch (Exception e) {
                callback().sendError(e);
            }
        }
    }

    @Override
    public void sendMessage(String sid, String cid, EnhancedText mentionedText, String[] attachments) {

        try {
            discordApi.sendChatMessage(cid, humanToIds(mentionedText));

            if (attachments != null) {
                for (String file : attachments) {
                    discordApi.sendAttachment(cid, file, new UploadListener(this, file));
                }
            }
        } catch (IOException | JSONException e) {
            callback().sendError(e);
        }

    }

    private String humanToIds(EnhancedText mt) {
        String text = "" + mt.text;

        for (String uid : mt.mentions.keySet()) {
            text = text.replace(mt.mentions.get(uid), String.format("<@%s>", uid));
        }

        return text;
    }

    @Override
    public void editMessage(String serverId, String channelId, String messageId, EnhancedText replacement) {
        try {
            discordApi.editMessage(channelId, messageId, humanToIds(replacement));
        } catch (IOException e) {
            callback().sendError(e);
        }
    }

    @Override
    public void deleteMessage(String serverId, String channelId, String messageId) {
        try {
            discordApi.deleteChatMessage(channelId, messageId);
        } catch (IOException e) {
            callback().sendError(e);
        }
    }

    @Override
    public void sendSeverEventsSwitch(String serverId) {
        try {
            lastSwitchedServer = serverId;

            if (!serverId.equals("hardcoded-discord-pm") && isRealtime())
                realtimeApi.sendOp14(serverId);
        } catch (JSONException e) {
            callback().sendError(e);
        }
    }

    @Override
    public void markRead(String sid, String cid) {
        try {
            String mid = ready.getLastMessageId(cid);

            if(mid != null && !"null".equals(mid))
                discordApi.ackMessageToken(cid, mid);
        } catch (IOException e) {
            callback().sendError(e);
        }
    }

    @Override
    public void setTypingState(String serverId, final String channelId, boolean isTyping) {
        if (isTyping) {
            if (typingChannel != null && typingChannel.equals(channelId)) {
                // Timer works fine. We shouldn't do anything
            } else {

                if (timer != null) {
                    timer.cancel();
                }

                timer = new Timer();
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            discordApi.postTyping(channelId);
                        } catch (Exception e) {
                            callback().sendError(e);
                        }
                    }
                }, 0, 8 * 1000);

            }
        } else {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        }
    }

    @Override
    public String getInviteInfo(String invite) {
        try {
            return discordApi.getInviteInfo(invite);
        } catch (IOException | JSONException e) {
            callback().sendError(e);
            return null;
        }
    }

    @Override
    public void acceptInvite(String invite) {
        try {
            Server server = convert.server(discordApi.acceptInvite(invite));
            server.eid = getEId();
            callback().sendServerUpdate(server);
        } catch (IOException | JSONException e) {
            callback().sendError(e);
        }
    }


    @Override
    public void inlineCallback(String serverId, String channelId, String messageId, String data) {
        try {
            discordApi.putReaction(channelId, messageId, data);
        } catch (IOException e) {
            callback().sendError(e);
        }
    }

    // ================= //
    // REALTIME LISTENER //
    // ================= //
    @Override
    public void onRealtimeStarted() {
        callback().sendRealtimeStarted();
    }

    @Override
    public void onRealtimeReady(Ready data) {

        this.ready = new ReadyHandler(data);

        for (Guild guild : data.guilds) {
            if (guild.roles != null) {
                for (Role role : guild.roles) {
                    roles.put(role.id, role);
                }
            }
        }

        callback().sendChannelUpdate(lastChannels);

        Server[] servers = new Server[data.guilds.length];
        for(int i = 0; i < servers.length; i++){
            Guild guild = data.guilds[i];
            servers[i] = convert.server(guild);
            servers[i].priority = ready.getGuildPosition(guild.id);
            servers[i].unread_count = ready.getGuildUnread(guild.id);
        }
        callback().sendServerUpdate(servers);


        if(lastSwitchedServer != null && !lastSwitchedServer.equals(SERVER_ID)){
            try {
                realtimeApi.sendOp14(lastSwitchedServer);
            } catch (JSONException e) {
                callback().sendError(e);
            }
        }
    }

    @Override
    public void onRealtimeStopped() {

    }

    @Override
    public void onGuildCreate(Guild guild) {
        for (Role role : guild.roles) {
            roles.put(role.id, role);
        }

        Server s = convert.server(guild);
        s.priority = ready.getGuildPosition(guild.id);
        callback().serverCreate(s);
    }

    @Override
    public void onGuildUpdate(Guild guild) {
        for (Role role : guild.roles) {
            roles.put(role.id, role);
        }

        Server s = convert.server(guild);
        s.priority = ready.getGuildPosition(guild.id);
        callback().sendServerUpdate(s);
    }

    @Override
    public void onGuildDelete(Guild guild) {
        Server s = new Server(getEId(), guild.id, "Removed guild");
        s.eid = getEId();
        s.flags = s.flags | Server.FLAG_GONE;
        callback().serverRemove(s);
    }

    @Override
    public void onGuildBanAdd(String s, com.iillyyaa2033.discord.objects.User user) {
        // TODO: what should i do here?
    }

    @Override
    public void onGuildBanRemove(String s, com.iillyyaa2033.discord.objects.User user) {
        // TODO: what should i do here?
    }

    @Override
    public void onGuildEmojisUpdate(String server, Emoji[] emojis) {
        // TODO: update emojis on Ready
        // Не уверен, является ли массив эмоджей полным списком эмоджей на сервере
        // или же это массив только лишь измененных айтемов
    }

    @Override
    public void onGuildMemberAdd(GuildMember member) {
        // TODO: how do I obtain guild id?
        // members.put(/*member.guild_id + */member.user.id, member);
    }

    @Override
    public void onGuildMemberUpdate(GuildMember member) {
        // TODO: how do I obtain guild id?
    }

    @Override
    public void onGuildMemberRemove(String server, com.iillyyaa2033.discord.objects.User user) {
        // TODO: how do I obtain guild id?
    }

    @Override
    public void onGuildRoleCreate(String s, Role role) {
        roles.put(role.id, role);
    }

    @Override
    public void onGuildRoleUpdate(String guildId, Role role) {
        roles.put(role.id, role);
    }

    @Override
    public void onGuildRoleDelete(String s, String roleid) {
        roles.remove(roleid);
    }

    @Override
    public void onGuildMemberChunk(String guild, GuildMember[] members) {

        MessageMetaPatch[] patches = new MessageMetaPatch[members.length];

        for (int i = 0; i < members.length; i++) {
            GuildMember member = members[i];
            this.members.put(guild + member.user.id, member);

            String nick = member.nick;
            if (nick != null && nick.trim().equals("null")) nick = null;
            int color = Converter.getColorFromRoles(member.roles, roles);

            patches[i] = new MessageMetaPatch(getEId(), guild, null, member.user.id,
                    nick, DiscordUtils.formatUserAvatarUrl(member.user), color);
        }

        callback().sendMessageMetaPatch(patches);
    }

    @Override
    public void onChannelCreate(com.iillyyaa2033.discord.objects.Channel channel) {
        String guild = channel.guild_id == null ? SERVER_ID : channel.guild_id;
        Channel ch = convert.channel(guild, channel);
        ch.eid = getEId();
        callback().sendChannelUpdate(ch);
    }

    @Override
    public void onChannelUpdate(com.iillyyaa2033.discord.objects.Channel channel) {
        String guild = channel.guild_id == null ? SERVER_ID : channel.guild_id;
        Channel ch = convert.channel(guild, channel);
        ch.eid = getEId();
        callback().sendChannelUpdate(ch);
    }

    @Override
    public void onChannelDelete(com.iillyyaa2033.discord.objects.Channel channel) {
        callback().channelDelete(channel.id);
    }

    @Override
    public void onChannelPinsUpdate(String guild, String channel, String lastPinTimestamp) {
        // TODO: return here when pins implemented
    }

    @Override
    public void onMessageCreate(com.iillyyaa2033.discord.objects.Message message) {

        ready.setLastMessageId(message.channel_id, message.id);

        Message msg = convert.message(message, gm(message.guild_id, message.author.id), roles);
        msg.eid = getEId();

        try {
            if (getMe().id.equals(msg.uid)) {
                msg.flags = msg.flags |
                        Message.FLAG_EDITABLE |
                        Message.FLAG_DELETABLE;
                ready.setChannelLastMessage(msg.cid, msg.mid);
                ready.setChannelMentionCount(msg.cid, 0);
            } else {
                ready.setChannelLastMessage(msg.cid, msg.mid);
                int cnt = ready.getChannelMentionCount(msg.cid) + 1;
                if(isMyMention(message)) ready.setChannelMentionCount(msg.cid,cnt);
            }
        } catch (Exception e) {
            callback().sendError(e);
        }

        if(ready.getRelationship(msg.uid) == 2) msg.set(Message.FLAG_HIDDEN);

        callback().sendMessageUpdate(msg);
        sendChannelAndGuildUpdate(message.channel_id);

        realtimeFindUnknownUsers(msg.sid, new com.iillyyaa2033.discord.objects.Message[]{message});

        if ((System.currentTimeMillis() - msg.date) > 5 * 1000) {
            callback().sendError(new Exception("Discord: new message, bad timings, " +
                    (System.currentTimeMillis() - msg.date) + "ms delay"));
        }
    }

    @Override
    public void onMessageUpdate(com.iillyyaa2033.discord.objects.Message message) {
        // Message.author can be null -- in that case we're just dropping this message
        if(message.author == null){
            return;
        }

        Message msg = convert.message(message, gm(message.guild_id, message.author.id), roles);

        msg.eid = getEId();
        msg.flags = msg.flags | Message.FLAG_EDITED;

        try {
            if (getMe().id.equals(msg.uid)) {
                msg.flags = msg.flags |
                        Message.FLAG_EDITABLE |
                        Message.FLAG_DELETABLE;
            }
        } catch (Exception e) {
            callback().sendError(e);
        }

        callback().sendMessageUpdate(msg);
    }

    @Override
    public void onMessageDelete(String id, String channel, String guild) {
        if (guild == null) guild = SERVER_ID;
        if (guild.isEmpty()) guild = SERVER_ID;

        callback().sendMessageDelete(guild, channel, id);
    }

    @Override
    public void onMessageDeleteBulk(String[] strings, String s, String s1) {
        // TODO: how do I handle this?
    }

    @Override
    public void onReactionCreate(String guild, String channel, String message, String user, Emoji dEmoji) {
        // TODO: return here when inlines will be better
    }

    @Override
    public void onReactionRemove(String guild, String channel, String message, String user, Emoji dEmoji) {
        // TODO: return here when inlines will be better
    }

    @Override
    public void onReactionRemoveAll(String s, String s1, String s2) {
        // TODO: return here when inlines will be better
    }

    @Override
    public void onMessageAck(Ack ack) {

        ready.setChannelLastMessage(ack.channel_id, ack.message_id);
        if(ack.manual) ready.setChannelMentionCount(ack.channel_id, ack.mention_count);
        else ready.setChannelMentionCount(ack.channel_id, 0);

        sendChannelAndGuildUpdate(ack.channel_id);
    }

    @Override
    public void onUserUpdate(com.iillyyaa2033.discord.objects.User user) {

    }

    @Override
    public void onPresenceUpdate(PresenceUpdate presenceUpdate) {

    }

    @Override
    public void onUserTyping(String uid, String guildId, String channelId) {
        String correctSID = guildId == null ? SERVER_ID : guildId;

        callback().sendTypingWithDelay(uid, correctSID, channelId, 7 * 1000);
    }

    @Override
    public void onVoiceServerUpdate(String endpoint, String guild_id, String token) {

    }

    @Override
    public void onVoiceStateUpdate(VoiceState voiceState) {

    }

    /**
     * Шлет серверу апдейт канала и соответствующего сервера
     *
     * @param channel_id -- id of channel
     */
    private void sendChannelAndGuildUpdate(String channel_id){
        com.iillyyaa2033.discord.objects.Channel channel = ready.getChannel(channel_id);
        if(channel != null){

            String validGuild = channel.guild_id == null ? DiscordAdapter.SERVER_ID : channel.guild_id;

            Channel rz = convert.channel(validGuild, channel);
            rz.eid = getEId();
            rz.unread_count = ready.getChannelMentionCount(channel.id);
            if(ready.isChannelUnread(channel)) rz.set(Channel.FLAG_UNREAD);

            ChannelOverride override = ready.getChannelOverride(validGuild, channel_id);
            if(override != null && override.muted) rz.set(Channel.FLAG_HIDDEN);

            callback().sendChannelUpdate(rz);

            Server server;
            if(channel.guild_id == null){
                server = new Server(getEId(), SERVER_ID, "Discord PM");
                server.icon_res = Utils.getResUrl("icon_discord_256.png");
                server.flags = server.flags | Server.FLAG_HARDCODED;
                server.priority = -10;
            } else {
                Guild guild = ready.getGuild(validGuild);
                server = convert.server(guild);
                server.priority = ready.getGuildPosition(validGuild);
            }

            server.unread_count = ready.getGuildUnread(validGuild);
            callback().sendServerUpdate(server);
        }
    }

    private boolean isMyMention(com.iillyyaa2033.discord.objects.Message message){
        if(message.guild_id == null) return true;

        if(message.mention_everyone) return true;

        com.iillyyaa2033.discord.objects.User me = ready.getMe();
        if(message.mentions != null && me != null) {
            String uid = me.id;
            for(com.iillyyaa2033.discord.objects.User user : message.mentions){
                if(uid.equals(user.id)){
                    return true;
                }
            }
        }

        // TODO: roles

        return false;
    }
}

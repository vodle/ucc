# Feature matrix
Cell value meaning:
```
yes = fully implemented;
+-  = partially implemented;
    = unimplemented;
n\a = service not providing this feature;
```

Service abbreviations:
- DC = Discord
- VK = VKontakte
- TG = Telegram

## Reading
| Name                  | DC  | VK  | TG  |
| ----                  | --  | --  | --  |
| Channels list         | yes | yes | yes |
| Channel history       | yes | yes | yes |
| Mentions              | yes | yes |     |
| Attachments           | yes | +-  | +-  |
| Replies               | n\a | +-  |     |
| Forwarded messages    | n\a | +-  |     |
| Other message types   | +-  | +-  | +-  |
| Inline buttons (1)    | yes | n\a |     |
| Per-server usernames  | yes | n\a | n\a |
| Real-time mode (2)    | yes | yes | +-  |

Notes:
1. *Inline buttons* is a buttons below message. You can see it in Discord (reaction buttons) or Telegram;
2. *Real-time mode* means that you can receive message as soon as it appeared on server. If real-time mode unimplemented, you have to manually update channel to get new messages - e.g. select another channel and switch back;

## Sending
| Name                  | DC  | VK  | TG  |
| ----                  | --  | --  | --  |
| Messages              | yes | yes | yes |
| Pictures              | yes | yes | yes |
| Files (1)             |     |     |     |
| Mentions              | yes | yes |     |
| Attachments           | yes | +-  | +-  |
| Replies               | n\a |     |     |
| Forwarded messages    | n\a |     |     |
| Other message types   |     |     |     |
| Inline buttons        | +-  | n\a |     |
| Per-server username   | yes | n\a | n\a |

Notes:
1. *File upload* means you can send any type of file, not only images;

## Complex features
| Name                  | DC  | VK  | TG  |
| ----                  | --  | --  | --  |
| Voice calls           |     |     |     |
| Contacts              |     |     |     |
| Users search          |     |     |     |
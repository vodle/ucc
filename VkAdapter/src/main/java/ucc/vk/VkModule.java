package ucc.vk;

import com.iillyyaa2033.ucmodel.ConnectionSettings;
import com.iillyyaa2033.ucserver.Adapter;
import com.iillyyaa2033.ucserver.AuthProc;
import com.iillyyaa2033.ucserver.Authenticator;
import com.iillyyaa2033.ucserver.Module;

import java.util.Map;

public class VkModule extends Module {

    @Override
    public Descr getDescription() {
        return new Descr("ucc.vk.VkAdapter","VK","Status: stable");
    }

    @Override
    public Authenticator getAuthenticator() {
        return new Authenticator() {
            @Override
            public AuthProc getAuthProc() {
                return new AuthProc(
                        new AuthProc.MessageStep("Сейчас будет открыта страничка логина ВК." +
                                "\n\nТак как ВК ограничил доступ к API переписок, мы прикинемся Kate Mobile, чтобы получить токен." +
                                "\n\nНеобходимо разрешить доступ Kate Mobile к аккаунту."),
                        new AuthProc.WebUrlStep(
                                "https://oauth.vk.com/authorize?client_id=2685278&scope=1073737727&redirect_uri=https://api.vk.com/blank.html&display=page&response_type=token&revoke=1",
                                "https://api.vk.com/blank.html",
                                null),
                        new AuthProc.FinalStep()
                );
            }

            @Override
            public ConnectionSettings finishAuthProc(Map<String, String> results) {
                String key = "#access_token=";
                String url = results.get("url");
                String token = url.substring(url.indexOf(key)+key.length());
                token = token.substring(0, token.indexOf("&"));

                return new ConnectionSettings(
                        "ucc.vk.VkAdapter",
                        null,
                        new String[]{"Mozilla", "Linux", "custom", "custom"},
                        token
                );
            }
        };
    }

    @Override
    public Adapter getAdapter() {
        return new VkAdapter();
    }
}

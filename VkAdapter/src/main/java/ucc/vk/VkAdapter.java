package ucc.vk;

import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.ConnectionSettings;
import com.iillyyaa2033.ucmodel.EnhancedText;
import com.iillyyaa2033.ucmodel.Message;
import com.iillyyaa2033.ucmodel.User;
import com.iillyyaa2033.ucserver.Adapter;

import org.json.JSONException;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

public class VkAdapter extends Adapter {

    private VkApi api;

    private String typingChannel = null;
    private Timer timer;

    VkAdapter(){
        setFeatureSet(Feature.FILE_UPLOAD | Feature.EXCPLICT_UNREADS);
    }

    @Override
    public void onConnectionSettingsChanged(ConnectionSettings settings) {
        super.setSettings(settings);

        api = new VkApi(this, getSettings());
    }

    @Override
    public String getServiceName() {
        return "VK";
    }

    @Override
    public boolean isRealtime() {
        return api.isRealtimeStarted();
    }

    @Override
    public void startRealtime() {
        api.startRealtime();
    }

    @Override
    public void stopRealtime() {
        if(api != null)
            api.stopRealtime();
    }

    @Override
    public User getAccountUser() {
        try {
            if(api == null) return null;
            else return api.getMe();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Channel[] getChannels(String serverId) {
        try {
            return api.getChannels();
        } catch (IOException | JSONException e) {
            callback().sendError(e);
            return null;
        }
    }

    @Override
    public Message[] getLastMessages(String serverId, String channelId) {
        try {
            return api.getLastMessages(channelId);
        } catch (IOException | JSONException e) {
            callback().sendError(e);
            return null;
        }
    }

    @Override
    public Message[] getMessagesBefore(String serverId, String channelId, String mid, long since, int count) {
        try {
            return api.getMessagesBefore(channelId, mid);
        } catch (IOException | JSONException e) {
            callback().sendError(e);
            return null;
        }
    }

    @Override
    public Message[] getMessagesSince(String serverId, String channelId, long since, int count) {
        return new Message[0];
    }


    @Override
    public void sendMessage(String sid, String cid, EnhancedText text, String[] attachments) {
        try {
            api.sendMessage(cid, humanToIds(text));
        } catch (IOException | JSONException e) {
            callback().sendError(e);
        }

        if(attachments != null){
            for(String file : attachments){
                sendAttachment(cid, file);
            }
        }
    }

    private String humanToIds(EnhancedText mt){
        String result = ""+mt.text;

        for(String key : mt.mentions.keySet()){
            String properUid = key;

            if(Pattern.matches("^[0-9]+$",properUid))
                properUid = "id"+properUid;

            result = result.replace(mt.mentions.get(key),
                    String.format("[%s|%s]", properUid, mt.mentions.get(key)));
        }

        return result;
    }

    private void sendAttachment(String channelId, String path) {
        try {
            api.sendAttachment(channelId, path);
        } catch (IOException | JSONException e) {
            callback().sendError(e);
        }
    }

    @Override
    public void editMessage(String serverId, String channelId, String messageId, EnhancedText replacement) {
        super.editMessage(serverId, channelId, messageId, /*humanToIds(replacement)*/null);
    }

    @Override
    public void deleteMessage(String serverId, String channelId, String messageId) {
        try {
            api.deleteMessage(messageId);
        } catch (IOException e) {
            callback().sendError(e);
        }
    }

    @Override
    public void markRead(String sid, String cid) {
        try {
            api.markRead(cid);
        } catch (IOException e) {
            callback().sendError(e);
        }
    }

    @Override
    public void setTypingState(String serverId, final String channelId, boolean isTyping) {
        if(isTyping){
            if(typingChannel != null && typingChannel.equals(channelId)){
                // Timer works fine. We shouldn't do anything
            } else {

                if(timer != null) {
                    timer.cancel();
                }

                timer = new Timer();
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            api.postTyping(channelId);
                        } catch (Exception e) {
                            callback().sendError(e);
                        }
                    }
                }, 0, 9 * 1000);

            }
        } else {
            if(timer != null) {
                timer.cancel();
                timer = null;
            }
        }
    }
}

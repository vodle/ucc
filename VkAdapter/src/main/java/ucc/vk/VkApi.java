package ucc.vk;

import com.iillyyaa2033.Utils;
import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.ConnectionSettings;
import com.iillyyaa2033.ucmodel.EnhancedText;
import com.iillyyaa2033.ucmodel.Message;
import com.iillyyaa2033.ucmodel.MessagePart;
import com.iillyyaa2033.ucmodel.MessagePartAttachment;
import com.iillyyaa2033.ucmodel.MessagePartText;
import com.iillyyaa2033.ucmodel.User;
import com.iillyyaa2033.ucserver.Adapter;
import com.iillyyaa2033.utils.HttpUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class VkApi {

    private static final boolean DEBUG_REALTIME_EVENTS = false;

    private ConnectionSettings settings;
    private VkAdapter parent;

    private static final String basicUrl = "https://api.vk.com/method/%s?access_token=%s&v=%s%s";
    private static final String vkApiVersion = "5.95";

    private Thread realtimeThread = null;

    private boolean errorWhileObtainingUser = false;
    private User me = null;
    private Channel[] lastChannels = null;

    /**
     * Map: userID - user
     */
    private HashMap<String, User> cachedUsers = new HashMap<>();

    VkApi(VkAdapter parent, ConnectionSettings settings){
        this.parent = parent;
        this.settings = settings;
    }

    Channel[] getChannels() throws IOException, JSONException {

        JSONObject json = connectGetJSONObject("messages.getConversations","filter=all&count=200&extended=true").getJSONObject("response");

        User[] users = {};
        if(json.has("profiles")) {
            users = parseProfiles(json.getJSONArray("profiles"));
        }

        if(json.has("items")){

            JSONArray items = json.getJSONArray("items");
            Channel[] channels = new Channel[items.length()];

            for(int i = 0; i < items.length(); i++){
                JSONObject conv = items.getJSONObject(i).getJSONObject("conversation");

                JSONObject peer = conv.getJSONObject("peer");

                String title;
                String icon = null;
                if(conv.has("chat_settings")) {
                    JSONObject chat_settings = conv.getJSONObject("chat_settings");
                    title = chat_settings.getString("title");

                    if(chat_settings.has("photo"))
                        icon = chat_settings.getJSONObject("photo").optString("photo_200");

                } else {

                    String uid = peer.optString("id");
                    User user = null;

                    for(User u : users){
                        if(u != null && u.uid.equals(uid)){
                            user = u;
                        }
                    }

                    if(user != null){
                        title = user.username;
                        icon = user.icon_res;
                    } else {
                        title = peer.optString("type")+"#"+peer.optString("id");
                        icon = null;
                    }

                }

                Message msg = msgFromJson(items.getJSONObject(i).getJSONObject("last_message"));
                String username = "unknown";

                for(User u : users){
                    if(u != null && u.uid.equals(msg.uid)){
                        username = u.username;
                        break;
                    }
                }

                channels[i] = new Channel(parent.getEId(), Adapter.SERVER_ID, ""+peer.getLong("id"),
                        title, username+": "+ msg.getText(), msg.date);
                channels[i].draw_mode = Channel.ChannelDrawMode.DEFAULT;
                channels[i].priority = -msg.date/1000;
                channels[i].icon = icon;
                channels[i].unread_count = conv.optInt("unread_count");
            }

            lastChannels = channels;

            return channels;
        }

        return null;
    }

    Message[] getLastMessages(String peer) throws IOException, JSONException {
        return _internalMessages(peer, null);
    }

    Message[] getMessagesBefore(String peer, String mid) throws IOException, JSONException {
        return _internalMessages(peer, mid);
    }

    private Message[] _internalMessages(String peer, String mid) throws IOException, JSONException {
        String params = "count=50&extended=1&peer_id="+peer;
        if(mid != null) params += "&start_message_id="+mid;
        JSONObject json = connectGetJSONObject("messages.getHistory", params).getJSONObject("response");

        User[] profiles = null;

        if(json.has("profiles")){
            JSONArray pr = json.getJSONArray("profiles");

            profiles = (parseProfiles(pr));
        }

        JSONArray convers = json.getJSONArray("conversations");
        String in_read = "";
        String out_read = "";

        for(int i = 0; i < convers.length(); i++){
            JSONObject conv = convers.getJSONObject(i);
            if(peer.equals(conv.getJSONObject("peer").optString("id"))){
                in_read = conv.optString("in_read");
                out_read = conv.optString("out_read");
            }
        }

        if(json.has("items")){

            JSONArray items = json.getJSONArray("items");
            Message[] messages = new Message[items.length()];

            for(int i = 0; i < messages.length; i++){
                messages[i] = msgFromJson(items.getJSONObject(i), peer);
                updateMessageByProfilesArray(messages[i], profiles);
                if(out_read.compareTo(messages[i].mid) < 0) {
                    messages[i].flags = messages[i].flags | Message.FLAG_UNREAD;
                }
            }

            return messages;

        }

        return null;
    }

    /**
     * This is might be ugly but its really simple
     */
    private void updateMessageByProfilesArray(Message message, User[] profiles){
        if(profiles == null) return;

        User found = null;

        for(User u : profiles){
            if(u != null) {
                if(message.uid.equals(u.uid)){
                    found = u;
                    break;
                }
            }
        }

        if(found != null){
            message.updateByAppearance(found.username, found.icon_res, 0);
        }
    }

    synchronized User getMe() throws IOException, JSONException {
        if(errorWhileObtainingUser) return null;

        if(me == null){

            // Set error flag to true (we'll reset it after successful end of operation)
            // If error occurs, this flag will stay unchanged
            errorWhileObtainingUser = true;

            JSONObject json =
                    connectGetJSONObject("users.get", "name_case=Nom&fields=photo_100,photo_200")
                            .getJSONArray("response")
                            .getJSONObject(0);

            String icon = null;
            if(json.has("photo_200")) {
                icon = json.optString("photo_200");
            } else if(json.has("photo_100")){
                icon = json.optString("photo_100");
            }

            me = new User(
                    parent.getEId(),
                    json.optString("id"),
                    json.optString("first_name") + " " + json.optString("last_name"),
                    icon);

            // If no error thrown, set error flag back to false
            errorWhileObtainingUser = false;
        }

        return me;
    }

    /**
     * Has side-effects
     *
     * <p>
     *     Updates cachedUsers (updates cachedUsers map)
     * </p>
     *
     * @param pr -
     * @return -
     * @throws JSONException -
     */
    private User[] parseProfiles(JSONArray pr) throws JSONException {

        User[] users = new User[pr.length()];

        for(int i = 0; i < pr.length(); i++) {
            JSONObject u = pr.getJSONObject(i);

            String icon = null;

            if (u.has("photo_200")) {
                icon = u.optString("photo_200");
            } else if (u.has("photo_100")) {
                icon = u.optString("photo_100");
            }

            User user = new User(parent.getEId(), u.optString("id"),
                    u.optString("first_name") + " " + u.optString("last_name"), icon);
            users[i] = user;

            cachedUsers.put(user.uid, user);
        }

        return users;
    }

    private Message msgFromJson(JSONObject msg){
        return msgFromJson(msg, msg.optString("peer_id"));
    }

    private Message msgFromJson(JSONObject msg, String peer){

        Message message =  new Message(parent.getEId(), Adapter.SERVER_ID, peer, msg.optString("id"),
                msg.optString("from_id"),msg.optLong("date")*1000);

        // Replies
        JSONObject reply = msg.optJSONObject("reply_message");
        if(reply != null) {
            MessagePartAttachment t = new MessagePartAttachment();
            t.type = MessagePartAttachment.Type.URL;

            String uid = reply.optString("from_id", "unknown");
            if(cachedUsers.containsKey(uid)) t.title = cachedUsers.get(uid).username;
            else t.title = "Reply to "+uid;

            t.description = "> "+reply.optString("text");
            message.append(t);
        }

        // Message contents
        String rawText = msg.optString("text", "");
        if(!rawText.isEmpty()) {
            message.append( new MessagePartText(formatText(rawText)));
        }


        // Forwards
        JSONArray forwards = msg.optJSONArray("fwd_messages");
        if(forwards != null){
            MessagePart[] fwd = new MessagePart[forwards.length()];

            for(int i = 0; i < forwards.length(); i++){
                JSONObject item = forwards.optJSONObject(i);

                MessagePartAttachment t = new MessagePartAttachment();
                t.type = MessagePartAttachment.Type.URL;

                String uid = item.optString("from_id", "unknown");
                if(cachedUsers.containsKey(uid)) t.title = cachedUsers.get(uid).username;
                else t.title = "Repost from "+uid;

                t.description = "> "+item.optString("text");
                fwd[i] = t;
            }

            message.append(fwd);
        }


        // Attachments
        message.append(parseAttachments(msg.optJSONArray("attachments")));

        try {
            User me = getMe();
            if(me != null){
                if(message.uid.equals(me.uid)){
                    message.flags = message.flags | Message.FLAG_EDITABLE | Message.FLAG_DELETABLE;
                }
            }
        } catch (IOException | JSONException e) {
            parent.callback().sendError(e);
        }

        int deleted = msg.optInt("deleted", 0);
        if(deleted == 1) {
            message.set(Message.FLAG_DELETED);
        }

        return message;
    }

    private MessagePartAttachment[] parseAttachments(JSONArray attaches){
        if(attaches == null) return new MessagePartAttachment[0];

        MessagePartAttachment[] res = new MessagePartAttachment[attaches.length()];

        for(int i = 0; i < attaches.length(); i++){
            JSONObject item = attaches.optJSONObject(i);
            MessagePartAttachment attach = new MessagePartAttachment();

            switch(item.optString("type","")){
                case "photo":
                    attach.type = MessagePartAttachment.Type.IMAGE;

                    JSONObject photo = item.optJSONObject("photo");
                    if(photo == null) break;

                    attach.description = photo.optString("text");

                    JSONArray sizes = photo.optJSONArray("sizes");
                    if(sizes == null) break;

                    char type = 'a';
                    for(int j = sizes.length() - 1; j >=0; j--){
                        JSONObject s = sizes.optJSONObject(j);
                        char n_type = s.optString("type", "a").toCharArray()[0];

                        if(n_type > type){
                            attach.actionUrl = s.optString("url");
                            attach.thumbUrl = s.optString("url");
                            attach.w = s.optInt("width");
                            attach.h = s.optInt("height");
                            type = n_type;
                        }
                    }
                    break;

                case "video":
                    JSONObject video = item.optJSONObject("video");
                    if(video == null) break;
                    attach.type = MessagePartAttachment.Type.VIDEO;
                    attach.title = video.optString("title");
                    attach.description = video.optString("description");
                    attach.thumbUrl = video.optString("photo_320");
                    attach.actionUrl = video.optString("player");
                    break;

                case "audio":
                    JSONObject audio = item.optJSONObject("audio");
                    if(audio == null) break;
                    attach.type = MessagePartAttachment.Type.MUSIC;
                    attach.title = audio.optString("title", "Аудиозапись");
                    attach.description = audio.optString("artist");
                    attach.actionUrl = audio.optString("url");
                    break;

                case "doc":
                    JSONObject doc = item.optJSONObject("doc");
                    if(doc == null) break;
                    attach.type = MessagePartAttachment.Type.URL;
                    attach.title = doc.optString("title", "Документ");
                    attach.actionUrl = doc.optString("url");
                    break;

                case "link":
                    JSONObject link = item.optJSONObject("link");
                    if(link == null) break;
                    attach.type = MessagePartAttachment.Type.URL;
                    attach.actionUrl = link.optString("url");
                    attach.title = link.optString("title", attach.actionUrl);
                    attach.description = link.optString("description");
                    break;

                case "sticker":
                    attach.type = MessagePartAttachment.Type.STICKER;

                    JSONObject sticker = item.optJSONObject("sticker");
                    if(sticker == null) break;

                    JSONArray images = sticker.optJSONArray("images");
                    if(images == null){
                        images = item.optJSONArray("images_with_background");
                        if(images == null)
                            break;
                    }

                    JSONObject image = images.optJSONObject(images.length() -1);
                    attach.actionUrl = image.optString("url");
                    attach.thumbUrl = image.optString("url");
                    attach.w = image.optInt("width");
                    attach.h = image.optInt("height");
                    break;

                case "audio_message":
                    JSONObject audio_message = item.optJSONObject("audio_message");
                    if(audio_message == null) break;
                    attach.type = MessagePartAttachment.Type.MUSIC;
                    attach.title = "Аудиосообщение";
                    attach.actionUrl = audio_message.optString("link_mp3");
                    break;


                case "wall":
                    JSONObject wall = item.optJSONObject("wall");
                    if(wall == null) break;
                    attach.type = MessagePartAttachment.Type.RECURSIVE;
                    attach.title = "Repost from wall "+wall.optString("to_id");
                    attach.description = wall.optString("text");
                    attach.recursive = parseAttachments(wall.optJSONArray("attachments"));
                    break;

                case "wall_reply":
                case "gift":
                case "market":
                case "market_album":
                default:
                    System.out.println("TODO: add type "+item.optString("type"));
                    attach.type = MessagePartAttachment.Type.URL;
                    attach.description = "Unimplemented attachment "+item.optString("type");
            }

            res[i] = attach;
        }

        return res;
    }

    void sendMessage(String peer, String message) throws IOException, JSONException {
        if(message.isEmpty()) return;

        message = URLEncoder.encode(message,"UTF-8");
        JSONObject response = connectGetJSONObject(
                "messages.send","peer_id="+peer+"&message="+message+"&random_id="+System.currentTimeMillis());

        if(response.has("error")){
            throw new IOException(response.getJSONObject("error").getString("error_msg"));
        }
    }

    void sendAttachment(String peerId, String file) throws IOException, JSONException{

        // TODO: guess upload type by file extension

        parent.callback().sendUploadProgress(file, Adapter.UploadProgress.STARTED);

        JSONObject uploadTarget = connectGetJSONObject("photos.getMessagesUploadServer","peer_id="+peerId).getJSONObject("response");
        String uploadUrl = uploadTarget.getString("upload_url");

        HttpURLConnection conn = (HttpURLConnection) URI.create(uploadUrl).toURL().openConnection();
        HttpUtils.multipartFileupload(conn, "photo", file, null);

        parent.callback().sendUploadProgress(file, Adapter.UploadProgress.WAIT_SERVER);
        int status = conn.getResponseCode();
        if (status != HttpURLConnection.HTTP_OK) {
            parent.callback().sendUploadProgress(file, Adapter.UploadProgress.DONE);
            throw new IOException("Server returned non-OK status: " + status);
        }

        JSONObject uploaded = new JSONObject(HttpUtils.fasterRead(conn));
//        System.out.println("VK uploaded "+uploaded);


        String photo = URLEncoder.encode(uploaded.getString("photo"), "UTF-8");
        JSONObject saved = connectGetJSONObject("photos.saveMessagesPhoto",
                String.format("photo=%s&server=%s&hash=%s", photo, uploaded.optInt("server"), uploaded.getString("hash")))
                .getJSONArray("response").getJSONObject(0);
//        System.out.println("VK saved "+saved);

        String attach = "photo"+saved.getInt("owner_id")+"_"+saved.getInt("id");
        JSONObject response = connectGetJSONObject(
                "messages.send","peer_id="+peerId+"&attachment="+attach+"&random_id="+System.currentTimeMillis());

        parent.callback().sendUploadProgress(file, Adapter.UploadProgress.DONE);

        if(response.has("error")){
            throw new IOException(response.getJSONObject("error").getString("error_msg"));
        }
    }

    void deleteMessage(String messageId) throws IOException {
        connGetCode("messages.delete", "delete_for_all=1&message_ids="+messageId);
    }

    void markRead(String peerId) throws IOException {
        connGetCode("messages.markAsRead","peer_id="+peerId);
    }

    void postTyping(String peerId) throws IOException {
        connGetCode("messages.setActivity","type=typing&peer_id="+peerId);
    }

    // ============= //
    // REALTIME MODE //
    // ============= //
    synchronized void startRealtime(){
        if(realtimeThread != null){
            if(!realtimeThread.isInterrupted()){
                return;
            }
        }

        parent.callback().sendRealtimeStarting();

        realtimeThread = new Thread(){

            @Override
            public void run() {
                try {
                    JSONObject startupData = connectGetJSONObject("messages.getLongPollServer","lp_version=3").getJSONObject("response");

                    if(DEBUG_REALTIME_EVENTS) System.out.println("VK/realtime: startup "+startupData);

                    String key = startupData.getString("key");
                    String server = startupData.getString("server");
                    long ts = startupData.getLong("ts");

                    parent.callback().sendRealtimeStarted();

                    while(!isInterrupted()){
                        // TODO: investigate 'Connection refused' exception
                        JSONObject answer = connectGetJSONObject(URI.create(formatUrl(server,key,ts)));

                        ts = answer.getLong("ts");

                        JSONArray updates = answer.getJSONArray("updates");

                        for(int i =0; i < updates.length(); i++){
                            switchEvCode(updates.getJSONArray(i));
                        }
                    }

                } catch (IOException | JSONException e) {
                    if(!(e instanceof ConnectException)) parent.callback().sendError(e);
                    VkApi.this.realtimeThread = null;
                }
            }

            private void switchEvCode(JSONArray array) throws JSONException {
                System.out.println(array);

                switch(array.getInt(0)){
                    case 1: // Message flags has been changed
                    case 2: // Message flags has been set
                    case 3: // Message flags has been reseted
                    case 4: // Message appeared
                    case 5: // Message edited
                        String messageId = array.getString(1);
                        Message m = fetchMessage(messageId);
                        if(m != null && !m.is(Message.FLAG_DELETED))
                            m.set(Message.FLAG_UNREAD);

                        parent.callback().sendMessageUpdate(m);
                        Channel ch = updateChannelWithMessage(m);
                        if(ch != null) parent.callback().sendChannelUpdate(ch);
                        break;
                    case 6: // Incoming messages read
                    case 7: // Outcoming messages read
                        String peerId = array.getString(1);
                        String localId = array.getString(2);
                        parent.callback().sendReadStateUpdate(Adapter.SERVER_ID, peerId, localId);
                        break;
                    case 8: // User became online
                    case 9: // User became offline
                        break;
                    case 61: // User started typing
                        parent.callback().sendTypingWithDelay(array.getString(1), Adapter.SERVER_ID, array.getString(1), 5 * 1000);
                        break;
                    case 62: // User started typing
                        parent.callback().sendTypingWithDelay(
                                array.getString(1), // Id of user
                                Adapter.SERVER_ID, // Id of server
                                ""+(2000000000 + array.getInt(2)), // Id of channel
                                // Здесь нужно дать комментарий. В вк есть несколько различных айдишников переписок: локальный, чатов, сообществ, и еще что-то
                                // В доках говорят, что, для чатов, полный айдишник канала получается вычитанием из локального двух лярдов, т.е. =2000000000-id
                                // Однако в конкретно этом случае необходимо сложить лярды и айди, чтобы получился стандартный айдишник канала.
                                5 * 1000);
                        break;
                    case 80: // Counter changes
                        break;
                    default:
                        System.out.println("VK/realtime: unknown event "+array);
                }
            }

            private String formatUrl(String server, String key, long ts){
                return String.format("https://%s?act=a_check&key=%s&ts=%s&wait=25&mode=2&version=2",server, key, ""+ts);
            }

        };
        realtimeThread.start();

    }

    /**
     * Blocking method
     *
     * @param id -
     * @return -
     */
    private Message fetchMessage(String id){
        try {
            JSONObject response = connectGetJSONObject("messages.getById","extended=true&message_ids="+id).getJSONObject("response");
            User[] profiles = parseProfiles(response.getJSONArray("profiles"));
            Message msg = msgFromJson(response.getJSONArray("items").getJSONObject(0));
            updateMessageByProfilesArray(msg, profiles);
            return msg;
        } catch (Exception e) {
            parent.callback().sendError(e);
            return null;
        }
    }

    /**
     * Has side effects
     *
     * <p>
     *     Modifies element(s) of lastChannels array.
     * </p>
     *
     * @param m -
     * @return -
     */
    private Channel updateChannelWithMessage(Message m){
        if(lastChannels != null && m != null){
            for(Channel channel : lastChannels){
                if(channel != null){
                    if(m.cid.equals(channel.cid)){
                        channel.flags = channel.flags | Channel.FLAG_UNREAD;
                        channel.unread_count++;
                        channel.last_time = m.date;
                        channel.priority = -m.date/1000;

                        String uid;
                        if(cachedUsers.containsKey(m.uid)) uid = cachedUsers.get(m.uid).username;
                        else uid = m.uid;
                        channel.last_msg = uid+": "+m.getText().toString();

                        return channel;
                    }
                }
            }
        }

        return null;
    }

    private EnhancedText formatText(String text){
        EnhancedText mentionedText = new EnhancedText(text);

        Pattern pattern = Pattern.compile("\\[.+?\\|{1}.+?\\]");
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            String sub = mentionedText.text.substring(matcher.start()+1, matcher.end()-1);
            String[] split = sub.split("\\|");

            if(split.length > 1) {
                mentionedText.text = mentionedText.text.replace("[" + sub + "]", split[1]);
                mentionedText.mentions.put(split[0], split[1]);
            } else {
                break;
            }

            matcher = pattern.matcher(mentionedText.text);
        }


        return mentionedText;
    }

    synchronized void stopRealtime(){
        if(realtimeThread != null){
            realtimeThread.interrupt();
            realtimeThread = null;
            if(DEBUG_REALTIME_EVENTS) System.out.println("Vk realtime stopped");
        }
    }

    boolean isRealtimeStarted(){
        return realtimeThread != null && !realtimeThread.isInterrupted();
    }

    // ===================== //
    // VK CONNECTION COMMONS //
    // ===================== //
    private int connGetCode(String method, String params) throws IOException {

        URI methodUri = getMethodUrl(method, params);

        HttpURLConnection conn = (HttpURLConnection) methodUri.toURL().openConnection();

        conn.setRequestMethod("GET");
        conn.setDoInput(false);
        conn.connect();
        int response = conn.getResponseCode();
        conn.disconnect();

        return response;
    }

    private JSONObject connectGetJSONObject(String method, String params) throws IOException, JSONException {
        URI methodUri = getMethodUrl(method, params);

        return connectGetJSONObject(methodUri);
    }

    private JSONObject connectGetJSONObject(URI methodUri) throws IOException, JSONException {

        HttpURLConnection conn = (HttpURLConnection) methodUri.toURL().openConnection();
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        conn.connect();

        JSONObject json = new JSONObject(HttpUtils.fasterRead(conn));

        conn.disconnect();

        return json;
    }

    private URI getMethodUrl(String method, String parameters){
        if(!parameters.startsWith("&")) parameters = "&"+parameters;
        return URI.create(String.format(basicUrl, method, settings.getToken(), vkApiVersion, parameters));
    }
}

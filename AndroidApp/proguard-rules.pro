-dontobfuscate

-keep class org.h2.** { *; }

-keep class com.iillyyaa2033.ucmodel.** { *; }
-keep class com.iillyyaa2033.ucserver.** { *; }
-keep class com.iillyyaa2033.ucstorage.** { *; }

-keep class org.drinkless.td.libcore.telegram.** { *; }

-keep class ucc.** { *; }
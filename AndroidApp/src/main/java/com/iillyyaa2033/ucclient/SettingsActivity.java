package com.iillyyaa2033.ucclient;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.iillyyaa2033.utils.HttpUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class SettingsActivity extends PreferenceActivity {

    private static final int DWL_BUILD = 10;

    private String savedDownloadUrl = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);

        findPreference("pref_reset_db").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showResetDbDialog();
                return true;
            }
        });

        findPreference("pref_download_build").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showDownloadBuildDialog();
                return true;
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == DWL_BUILD) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                download(savedDownloadUrl);
            } else {
                Toast.makeText(this, "Cannot download without WRITE permission", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void showResetDbDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.reset_db_confirm)
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new AlertDialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MainActivity.server.delMyData();
                        System.exit(0);
                    }
                })
                .show();
    }

    private void showDownloadBuildDialog() {
        final AlertDialog al = new AlertDialog.Builder(this).setMessage("Fetching info...").setCancelable(false).show();

        final Handler handler = new Handler();

        new Thread() {
            @Override
            public void run() {
                try {
                    HttpURLConnection conn = (HttpURLConnection) new URL(GitlabIntegration.getLatestInfoUrl()).openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();

                    JSONObject rz = new JSONArray(HttpUtils.fasterRead(conn)).getJSONObject(0);
                    JSONObject apkData = rz.getJSONObject("apkData");

                    final String text = "This build info:\n" +
                            "- code: " + BuildConfig.VERSION_CODE + "\n" +
                            "- name: " + BuildConfig.VERSION_NAME + "\n" +
                            "\nLatest build info:\n" +
                            "- code: " + apkData.getInt("versionCode") + "\n" +
                            "- name: " + apkData.getString("versionName");

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            al.dismiss();
                            al.hide();

                            new AlertDialog.Builder(SettingsActivity.this)
                                    .setMessage(text)
                                    .setNegativeButton("Cancel", null)
                                    .setPositiveButton("Download", new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            checkPermissionsAndDownload(GitlabIntegration.getLatestApkUrl());
                                        }
                                    }).show();
                        }
                    });

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SettingsActivity.this, "Error", Toast.LENGTH_SHORT).show();
                            al.hide();
                        }
                    });
                }
            }
        }.start();
    }

    private void checkPermissionsAndDownload(String url) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        DWL_BUILD);
                savedDownloadUrl = url;
                return;
            }
        }

        download(url);
    }

    private void download(String url) {
        System.out.println("Downloading " + url);
        try {
            Uri uri = Uri.parse(url);
            DownloadManager.Request r = new DownloadManager.Request(uri);
            r.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, uri.getLastPathSegment());
            r.allowScanningByMediaScanner();
            r.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            if (dm != null) dm.enqueue(r);
            else throw new Exception("Cannot find DownloadManager");
        } catch (Exception e) {
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

}

package com.iillyyaa2033.ucclient;

import androidx.annotation.NonNull;

import org.commonmark.node.SoftLineBreak;

import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.MarkwonVisitor;

public class MdNewlinePlugin extends AbstractMarkwonPlugin {

    @Override
    public void configureVisitor(@NonNull MarkwonVisitor.Builder builder) {
        builder.on(SoftLineBreak.class, new MarkwonVisitor.NodeVisitor<SoftLineBreak>() {

            @Override
            public void visit(@NonNull MarkwonVisitor visitor, @NonNull SoftLineBreak softLineBreak) {
                visitor.forceNewLine();
            }
        });
    }


}

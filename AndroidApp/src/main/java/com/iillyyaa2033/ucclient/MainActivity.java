package com.iillyyaa2033.ucclient;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.iillyyaa2033.TypingCache;
import com.iillyyaa2033.ucclient.adapters.AttachmentsAdapter;
import com.iillyyaa2033.ucclient.adapters.MessagesViewAdapter;
import com.iillyyaa2033.ucclient.adapters.UserlistViewAdapter;
import com.iillyyaa2033.ucclient.navigation.NavigationFragment;
import com.iillyyaa2033.ucclient.navigation.NavigationPagerAdapter;
import com.iillyyaa2033.ucclient.scheduler.SchedulerTools;
import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.EnhancedText;
import com.iillyyaa2033.ucmodel.Message;
import com.iillyyaa2033.ucmodel.Server;
import com.iillyyaa2033.ucmodel.ServerInfo;
import com.iillyyaa2033.ucmodel.User;
import com.iillyyaa2033.ucserver.Adapter;
import com.iillyyaa2033.ucserver.IServerListener;
import com.iillyyaa2033.ucserver.LocalServer;
import com.iillyyaa2033.ucmodel.MessageMetaPatch;
import com.iillyyaa2033.ucserver.Module;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;

import ucc.discord.DiscordModule;
import ucc.vk.VkModule;

public class MainActivity extends AppCompatActivity implements IServerListener {

    private static final int PICK_IMAGE = 1;

    // TODO: make this block user preferences
    public static final boolean FEATURE_IMAGE_LOADING = true;
    public static final boolean FEATURE_SEND_SERVER_SWITCH = true;
    public static final boolean FEATURE_SEND_READ = true;
    public static final boolean FEATURE_SEND_MY_TYPING = true;
    public static final String MENTION_PREFIX = "@";

    public static LocalServer server;

    private BroadcastReceiver networkStateReceiver;

    private NavigationPagerAdapter pagerAdapter;
    private MessagesViewAdapter messagesAdapter;
    private UserlistViewAdapter usersAdapter;
    private AttachmentsAdapter attachAdapter;

    private UploadViewCtlr uploadViewCtlr;
    private TypingCache typers = new TypingCache();

    private Handler handler = new Handler();

    private Intent savedData = null;

    private EnhancedText currentMessageData = new EnhancedText();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Thread.setDefaultUncaughtExceptionHandler(new CustomUEH());

        registerNetworkReceiver();

        setupNavigationPager();
        setupLeftDrawer();
        setupMessagesList();
        setupUserlist();
        setupAttachList();
        setupMessageForm();

        uploadViewCtlr = new UploadViewCtlr((TextView) findViewById(R.id.upload_info));

        try {
            setupServer();
        } catch (SQLException e) {
            onError(e);
        }

        SchedulerTools.scheduleNextAlarm(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (server != null) {
            if (server.getAvailableAdapters().size() < 1) {
                Intent i = new Intent(MainActivity.this, AccountsMgmtActivity.class);
                i.putExtra("force_add", true);
                startActivity(i);
            }
        }

        // TODO: do not set proxy here
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        messagesAdapter.setCustomProxy(prefs.getString("pref_proxy_url", null));

        if (ShareReceiverActivity.text != null) {
            EditText field = findViewById(R.id.message_field);
            field.getText().append(ShareReceiverActivity.text);
            ShareReceiverActivity.text = null;
        }

        if (ShareReceiverActivity.images.size() > 0) {
            for (String s : ShareReceiverActivity.images)
                attachAdapter.add(s);

            ShareReceiverActivity.images.clear();
        }

    }

    @Override
    protected void onPause() {
        if (server != null) server.pause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        server.destroy();
        server = null;

        unregisterNetworkReceiver();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_IMAGE) {

                if (data.getData() == null) {
                    Toast.makeText(this, R.string.err_bad_selection, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (Build.VERSION.SDK_INT >= 23) {
                    if (this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        savedData = data;
                        requestPermissions(
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                PICK_IMAGE);
                        return;
                    }
                }

                postPickSend(data);

            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length == 1) {
            if (requestCode == PICK_IMAGE) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    postPickSend(savedData);
                } else {
                    Toast.makeText(this, R.string.err_no_permission, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    void postPickSend(Intent data) {
        String path = getImagePath(data.getData());
        if (path != null)
            attachAdapter.add(path);
    }


    // ================ //
    // COMPONENTS SETUP //
    // ================ //
    private void setupServer() throws SQLException {
        if (server == null) {
            String path = getFilesDir().getPath() + "/databases/main;FILE_LOCK=FS;PAGE_SIZE=1024;CACHE_SIZE=8192;";
            String cache = getCacheDir().getAbsolutePath();

            server = new LocalServer(path, cache, this, getKnownModules(this));
        } else {
            findViewById(R.id.loading_overlay).setVisibility(View.GONE);
        }
    }

    public static ArrayList<Module> getKnownModules(Context context) {
        ArrayList<Module> modules = new ArrayList<>();

        modules.add(new DiscordModule());
        modules.add(new VkModule());

        if (context != null) modules.addAll(ExtensionLoader.loadAll(context));
        return modules;
    }

    private void setupLeftDrawer() {
        View v = MainActivity.this.findViewById(R.id.chat_panel);
        if (v instanceof SpecialRelativeLayout) {
            ((SpecialRelativeLayout) v).setDrawerStateListener(new SpecialRelativeLayout.OnDrawerState() {

                @Override
                public void onDrawerOpened() {

                }

                @Override
                public void onDrawerClosed() {
                    int idx = ((ViewPager) findViewById(R.id.pager)).getCurrentItem();
                    NavigationFragment f = pagerAdapter.getItem(idx);

                    if (f.adapter != pagerAdapter.currentAdapter) {
                        selectAdapter(f.adapter);
                        if (f.currentServer != null) selectServer(f.currentServer);
                    }
                }
            });
        }
    }

    private void setupNavigationPager() {
        ViewPager pager = findViewById(R.id.pager);
        pagerAdapter = new NavigationPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
    }

    private void setupMessagesList() {
        messagesAdapter = new MessagesViewAdapter(this, new MessagesViewAdapter.EventsListener() {

            @Override
            public void onTextClick(View view, Message m) {
                // Disabled for now
                // Rus: тут показывал менюху перевода.
                // Проблема в том, что часто менюха вылезает невовремя. Например, когда скроллим лист чата.

//                showMessageClickMenu(view, m);
            }

            @Override
            public void onLongClick(View view, Message m) {
                showMessageLongclickMenu(view, m);
            }

            @Override
            public void onUsernameClick(Message m) {
                String mention = MENTION_PREFIX + m.username;
                currentMessageData.mentions.put(m.uid, mention);

                EditText field = findViewById(R.id.message_field);
                field.append(mention);
            }

            @Override
            public void onAvatarClick(Message m) {
                // TODO: open user profile
            }

            @Override
            public void onInlineClick(Message m, String inlineData) {
                server.sendInlineClick(m.eid, m.sid, m.cid, m.mid, inlineData);
            }

            @Override
            public void onScrolledToTop(final Message message) {
                server.requestMessagesBefore(message.eid, message.sid, message.cid, message.mid, message.date);
            }
        });

        final ListView messagesList = findViewById(R.id.messages_list);
        messagesList.setAdapter(messagesAdapter);
        messagesAdapter.onBindView(messagesList);
    }

    private void setupUserlist() {
        usersAdapter = new UserlistViewAdapter(this);

//        final ListView usersList = findViewById(R.id.userlist);
//        usersList.setAdapter(usersAdapter);
    }

    private void setupAttachList() {
        RecyclerView rv = findViewById(R.id.attach_list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv.setLayoutManager(layoutManager);

        attachAdapter = new AttachmentsAdapter(this);
        rv.setAdapter(attachAdapter);
    }

    private void setupMessageForm() {
        EditText field = findViewById(R.id.message_field);
        field.addTextChangedListener(new TextWatcher() {

            final String bgColor = "#50909090";

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                for (BackgroundColorSpan span : editable.getSpans(0, editable.length(), BackgroundColorSpan.class)) {
                    editable.removeSpan(span);
                }

                for (String key : currentMessageData.mentions.keySet()) {
                    String user = currentMessageData.mentions.get(key);

                    int start = 0;
                    while ((start = editable.toString().indexOf(user, start)) != -1) {
                        editable.setSpan(
                                new BackgroundColorSpan(Color.parseColor(bgColor)),
                                start,
                                start + user.length(),
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        start += user.length();
                    }
                }
            }
        });

        (findViewById(R.id.btn_send)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TextView t = findViewById(R.id.message_field);

                currentMessageData.text = t.getText().toString();
                String[] attaches = attachAdapter.all();

                if (pagerAdapter.currentChannel != null) {
                    uploadViewCtlr.increaseCountBy(attaches.length);
                    server.sendMessage(
                            pagerAdapter.currentChannel.eid, pagerAdapter.currentChannel.sid, pagerAdapter.currentChannel.cid,
                            currentMessageData, attaches);
                    attachAdapter.clear();
                    t.setText("");
                    currentMessageData = new EnhancedText();
                } else {
                    Toast.makeText(MainActivity.this, "Cannot get current channel (2)", Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.btn_upload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), PICK_IMAGE);
            }
        });

        if (FEATURE_SEND_MY_TYPING) {
            ((EditText) findViewById(R.id.message_field)).addTextChangedListener(new TextWatcher() {

                final long CHECK_PERIOD = 1000;
                boolean startEventSent = false;
                Timer timer = new Timer();

                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (!startEventSent) {

                        if (pagerAdapter.currentChannel != null) {
                            MainActivity.server.sendTypingRequest(pagerAdapter.currentChannel.eid, pagerAdapter.currentChannel.sid, pagerAdapter.currentChannel.cid, true);
                            startEventSent = true;
                        }
                    }
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (timer != null)
                        timer.cancel();
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {

                            if (pagerAdapter.currentChannel != null) {
                                MainActivity.server.sendTypingRequest(pagerAdapter.currentChannel.eid, pagerAdapter.currentChannel.sid, pagerAdapter.currentChannel.cid, false);
                            }

                            startEventSent = false;
                        }
                    }, CHECK_PERIOD);
                }
            });
        }
    }


    private void registerNetworkReceiver() {
        networkStateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                final boolean isOnline;

                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                if (cm != null) {
                    NetworkInfo ni = cm.getActiveNetworkInfo();
                    isOnline = (ni != null) && (ni.isConnected());
                } else {
                    isOnline = false;
                }

                LocalServer.networkMode = isOnline ? LocalServer.NETWORK_MODE_ONLINE : LocalServer.NETWORK_MODE_OFFLINE;

                if (server != null && pagerAdapter.currentAdapter != null)
                    server.requestAdapterSwitch(pagerAdapter.currentAdapter.getEId(), pagerAdapter.currentAdapter.getEId());

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        findViewById(R.id.indicator_offline).setVisibility(isOnline ? View.GONE : View.VISIBLE);
                    }
                });
            }
        };

        registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private void unregisterNetworkReceiver() {
        unregisterReceiver(networkStateReceiver);
    }


    // ======= //
    // DIALOGS //
    // ======= //
    public void showInviteDialog(final Adapter adapter) {
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);

        final TextView text = new TextView(this);
        text.setText(R.string.enter_discord_invite);
        ll.addView(text);

        final EditText edit = new EditText(this);
        ll.addView(edit);

        new AlertDialog.Builder(this)
                .setView(ll)
                .setPositiveButton(android.R.string.ok, new AlertDialog.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        new Thread() {

                            @Override
                            public void run() {
                                final String invite = edit.getText().toString();
                                final String s = server.getInviteInfo(adapter.getEId(), invite);

                                if (s == null) return;

                                handler.post(new Runnable() {

                                    @Override
                                    public void run() {
                                        new AlertDialog.Builder(MainActivity.this)
                                                .setMessage("" + s)
                                                .setNegativeButton(android.R.string.cancel, null)
                                                .setPositiveButton(android.R.string.ok, new AlertDialog.OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        server.sendInviteAccept(adapter.getEId(), invite);
                                                    }
                                                })
                                                .show();
                                    }
                                });
                            }
                        }.start();
                    }
                }).show();
    }

    public void showServerMenu(View v, final Server server) {

        PopupMenu popupMenu = new PopupMenu(this, v);

        Menu menu = popupMenu.getMenu();
        menu.add(2, 2, 0, R.string.srv_info);
        menu.add(0, 0, 1, R.string.srv_change_nick);
        menu.add(1, 1, 2, R.string.srv_leave);

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case 0:
                        if (MainActivity.server.adapterHasFeature(server.eid, Adapter.Feature.PER_SERVER_NICKNAME))
                            showChangeNickDialog(server);
                        else
                            Toast.makeText(MainActivity.this, R.string.unimplemented, Toast.LENGTH_LONG).show();
                        break;
                    case 1:
                        showLeaveDialog(server);
                        break;
                    case 2:
                        showServerInfoDialog(server);
                        break;
                }
                return false;
            }
        });
        popupMenu.show();


    }

    private void showChangeNickDialog(final Server target) {
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);

        final TextView text = new TextView(this);
        text.setText(R.string.enter_nick);
        ll.addView(text);

        final EditText edit = new EditText(this);
        ll.addView(edit);

        new AlertDialog.Builder(this)
                .setView(ll)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new AlertDialog.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MainActivity.server.requestChangeNickname(target.eid, target.sid, edit.getText().toString());
                    }
                }).show();
    }

    private void showLeaveDialog(final Server target) {
        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.srv_leave_confirm, target.name))
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new AlertDialog.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MainActivity.server.sendLeaveServer(target.eid, target.sid);
                    }
                }).show();
    }

    private void showServerInfoDialog(final Server sv) {

        ServerInfo[] info = server.getExtendedServerInfo(sv.eid, sv.sid);
        String s = "<b>" + sv.name + "</b>";

        Arrays.sort(info);

        for (ServerInfo i : info) s += "<br/>" + i.getKey() + ": " + i.getValue();

        new AlertDialog.Builder(this)
                .setMessage(Html.fromHtml(s))
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }

    private void showMessageLongclickMenu(final View v, final Message m) {

        PopupMenu popupMenu = new PopupMenu(this, v);
        Menu menu = popupMenu.getMenu();
        menu.add(0, 0, 0, R.string.msg_copytext);
        menu.add(1, 1, 1, R.string.msg_userprofile);
        menu.add(2, 2, 2, R.string.msg_sharelink);

        if (m.is(Message.FLAG_EDITABLE)) menu.add(3, 3, 3, R.string.msg_edit);
        if (m.is(Message.FLAG_DELETABLE)) menu.add(4, 4, 4, R.string.msg_delete);

        menu.add(5, 5, 5, "Translate");

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case 0: // Copy
                        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("ucc", m.getRawText());
                        if (clipboard != null)
                            clipboard.setPrimaryClip(clip);
                        else
                            onError(new Exception("Cannot get ClipboardManager!"));
                        break;
                    case 1: // Profile
                    case 2: // Share
                        Toast.makeText(MainActivity.this, R.string.unimplemented, Toast.LENGTH_SHORT).show();
                        break;
                    case 3: // Edit
                        showEditMessageDialog(m);
                        break;
                    case 4: // Delete
                        showDeleteMessageDialog(m);
                        break;
                    case 5: // Translate
                        showFullTranslateDialog(m);
                        break;
                }

                return true;
            }
        });
        popupMenu.show();
    }

    private void showEditMessageDialog(final Message m) {
        final EditText field = new EditText(this);
        field.setText(m.getRawText());

        new AlertDialog.Builder(this)
                .setView(field)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new AlertDialog.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        EnhancedText text = new EnhancedText(field.getText().toString());
                        text.mentions = m.getText().mentions;
                        server.sendEditMessage(m.eid, m.sid, m.cid, m.mid, text);
                    }
                })
                .show();
    }

    private void showDeleteMessageDialog(final Message m) {
        if (m.is(Message.FLAG_DELETABLE)) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.msg_delete_confirm)
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            server.sendDeleteMessage(m.eid, m.sid, m.cid, m.mid);
                        }
                    })
                    .show();
        } else {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.msg_undeletable)
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
        }
    }

    private void showFullTranslateDialog(final Message m) {
        @SuppressLint("InflateParams")
        View root = LayoutInflater.from(this).inflate(R.layout.dialog_translate_full, null, false);

        final TextView text = root.findViewById(R.id.translate_text);

        Spinner spinner = root.findViewById(R.id.translate_spinner);
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, Translator.names);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                text.setText(R.string.translate_working);

                new Thread() {
                    @Override
                    public void run() {
                        try {
                            final String result = Translator.translate(m.getRawText(), adapter.getItem(i));
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    text.setText(result);
                                }
                            });
                        } catch (IOException e) {
                            onError(e);
                        }
                    }
                }.start();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        new AlertDialog.Builder(this)
                .setView(root)
                .show();
    }


    // ========== //
    // SELECTIONS //
    // ========== //
    public void selectAdapter(Adapter adapter) {
        PreferenceManager
                .getDefaultSharedPreferences(this)
                .edit()
                .putString("LAST_ADAPTER", adapter.getEId())
                .apply();

        String eid = pagerAdapter.currentAdapter == null ? null : pagerAdapter.currentAdapter.getEId();
        server.requestAdapterSwitch(eid, adapter.getEId());

        pagerAdapter.currentAdapter = adapter;
    }

    public void selectServer(Server server) {
        if (server == null) return;

        boolean sameAdapter = pagerAdapter.currentAdapter != null
                && server.eid.equals(pagerAdapter.currentAdapter.getEId());
        boolean sameServer = server.equals(pagerAdapter.currentServer);

        PreferenceManager
                .getDefaultSharedPreferences(this)
                .edit()
                .putString("LAST_SERVER_ON_" + server.eid, server.sid)
                .apply();

        pagerAdapter.currentServer = server;
        pagerAdapter.setCurrentServer(server.eid, server);

        if (!sameAdapter) {
            Adapter a = pagerAdapter.getAdapter(server.eid);
            if (a != null) selectAdapter(a);
        }

        if (!sameServer) {
            MainActivity.server.requestChannels(server.eid, server.sid);

            if (FEATURE_SEND_SERVER_SWITCH) {
                MainActivity.server.sendServerSwitch(server.eid, server.sid);
            }
        }
    }

    public void selectChannel(Channel channel) {
        if (channel == null) return;

        boolean same = false;

        if (pagerAdapter.currentChannel != null) {
            same = pagerAdapter.currentChannel.equals(channel);
            if (FEATURE_SEND_READ)
                server.requestMarkRead(pagerAdapter.currentChannel.eid, pagerAdapter.currentChannel.sid, pagerAdapter.currentChannel.cid);
        }

        PreferenceManager
                .getDefaultSharedPreferences(this)
                .edit()
                .putString("LAST_CHANNEL_ON_" + channel.eid + channel.sid, channel.cid)
                .apply();

        pagerAdapter.currentChannel = channel;
        pagerAdapter.setCurrentChannel(channel);

        if (!same) {
            EditText field = findViewById(R.id.message_field);

            String s = channel.title + "";
            if (s.length() > 20) s = s.substring(0, 20) + "...";
            field.setHint("Message " + s);

            server.requestMessages(pagerAdapter.currentChannel.eid, pagerAdapter.currentChannel.sid, pagerAdapter.currentChannel.cid);
            updTypersView();
        }
    }


    // ================== //
    // VIEW MODIFIACTIONS //
    // ================== //
    private void hideNochat() {
        View panel = findViewById(R.id.nochat_overlay);

        if (panel.getVisibility() == View.VISIBLE) {
            panel.setVisibility(View.GONE);
            findViewById(R.id.chat_panel).setVisibility(View.VISIBLE);
        }
    }

    public void closeLeftDrawer() {
        View v = MainActivity.this.findViewById(R.id.chat_panel);
        if (v instanceof SpecialRelativeLayout)
            ((SpecialRelativeLayout) v).closeDrawer();
    }

    private void updTypersView() { // TODO: move to another place
        // TODO: un-obfuscate this fields
        StringBuilder ts = new StringBuilder();
        boolean a = false;

        if (pagerAdapter.currentChannel != null) {
            for (String name : typers.getTypersAtChannel(pagerAdapter.currentChannel.eid, pagerAdapter.currentChannel.sid, pagerAdapter.currentChannel.cid)) {
                if (name != null) {
                    ts.append(name).append(", ");
                    a = true;
                }
            }
        }

        final boolean finalA = a;
        final String finalTs = getString(R.string.typing_prefix) + " "
                + ts.substring(0, ts.length() > 1 ? ts.length() - 2 : 0)
                + getString(R.string.typing_suffix);
        handler.post(new Runnable() {
            @Override
            public void run() {
                TextView ttext = findViewById(R.id.typers_text);
                if (finalA) {
                    ttext.setVisibility(View.VISIBLE);
                    ttext.setText(finalTs);
                } else {
                    ttext.setVisibility(View.GONE);
                }
            }
        });
    }


    // ====== //
    // EVENTS //
    // ====== //
    @Override
    public void onServerCreate(final Server... server) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                pagerAdapter.updateServers(server);
            }
        });
    }

    @Override
    public void onServerUpdate(final Server... server) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                pagerAdapter.updateServers(server);
            }
        });
    }

    @Override
    public void onServerRemove(final Server... server) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                pagerAdapter.deleteServers(server);
            }
        });
    }

    @Override
    public void onChannelUpdate(final Channel... channels) {

        if (pagerAdapter.currentChannel != null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    pagerAdapter.update(channels);
                }
            });
        }
    }

    @Override
    public void onChannelDelete(String... channels) {

    }

    @Override
    public void onMessageUpdate(final Message message) {
        if (message == null) return;
        if (pagerAdapter.currentChannel == null) return;

        if (!message.eid.equals(pagerAdapter.currentChannel.eid)) return;
        if (!message.sid.equals(pagerAdapter.currentChannel.sid)) return;
        if (!message.cid.equals(pagerAdapter.currentChannel.cid)) return;

        handler.post(new Runnable() {

            @Override
            public void run() {
                messagesAdapter.addOrUpdate(message);
                hideNochat();
                ((ListView) findViewById(R.id.messages_list)).invalidateViews();
            }
        });
    }

    @Override
    public void onMessageDelete(final String eid, final String guildId, final String channelId, final String messageId) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                messagesAdapter.markDeleted(eid, guildId, channelId, messageId);
            }
        });
    }

    @Override
    public void onMessageMetaPatch(final MessageMetaPatch[] patches) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                messagesAdapter.updateAppearance(patches);
            }
        });
    }

    @Override
    public void onReadStateUpdate(final String eid, final String sid, final String cid, final String lastReadMessage) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (pagerAdapter.currentChannel != null) {
                    if (pagerAdapter.currentChannel.eid.equals(eid)
                            && pagerAdapter.currentChannel.sid.equals(sid)
                            && pagerAdapter.currentChannel.cid.equals(cid)) {
                        messagesAdapter.updateReadState(lastReadMessage);
                    }
                }
            }
        });
    }

    @Override
    public void onTypingState(String userId, boolean isActive, String eid, String serverId, String channelId) {
        String typer = userId;
        User user = server.getUser(eid, userId);
        if (user != null) {
            typer = user.username;
        }

        if (isActive) {
            typers.addTyper(eid, serverId, channelId, typer);
        } else {
            typers.removeTyper(eid, serverId, channelId, typer);
        }

        updTypersView();
    }

    @Override
    public void onRealtimeState(int state, String extid) {
        if (state == Adapter.REALTIME_STARTING) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    TextView ttext = findViewById(R.id.typers_text);
                    ttext.setVisibility(View.VISIBLE);
                    ttext.setText(R.string.establishing_realtime);
                }
            });
        } else {
            updTypersView();
        }
    }

    @Override
    public void onUploadProgress(final String filename, final int progress) {

        handler.post(new Runnable() {
            @Override
            public void run() {
                uploadViewCtlr.update(progress);
            }
        });
    }

    @Override
    public void onUserUpdate(final User user) {

    }

    @Override
    public void onUserPresenceUpdate(final String server, final String channel, final User user) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                usersAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onError(final Throwable e) {
        e.printStackTrace();
        handler.post(new Runnable() {

            @Override
            public void run() {

                if (BuildConfig.DEBUG_ERR_TOAST) {
                    Toast.makeText(MainActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }

                if (BuildConfig.DEBUG_ERR_DIALOG) {
                    String text = formatError(e);

                    new AlertDialog.Builder(MainActivity.this)
                            .setMessage(text)
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                }
            }

            String formatError(Throwable e) {
                StringBuilder rz = new StringBuilder(e.getLocalizedMessage() + "\n");

                for (StackTraceElement el : e.getStackTrace()) {
                    rz.append("\t").append(el.toString()).append("\n");
                }

                Throwable cause = e.getCause();
                if (cause != null) {
                    rz.append("\n\n");
                    rz.append(formatError(cause));
                }

                return rz.toString();
            }
        });
    }

    private String getImagePath(Uri uri) {
        try {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor == null) throw new RuntimeException("Cursor is null (1)");

            cursor.moveToFirst();
            String document_id = cursor.getString(0);
            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
            cursor.close();

            cursor = getContentResolver().query(
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
            if (cursor == null) throw new RuntimeException("Cursor is null (2)");

            cursor.moveToFirst();
            String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();
            return path;
        } catch (CursorIndexOutOfBoundsException e) {
            Toast.makeText(this, R.string.err_bad_selection, Toast.LENGTH_SHORT).show();
            return null;
        } catch (Exception e) {
            onError(e);
            return null;
        }
    }


    @Override
    public void onServerStopped() {
        // TODO: implement me
    }

    // ================ //
    // SERVER's ANSWERS //
    // ================ //

    @Override
    public void onAdaptersLoaded(final Collection<Adapter> adapters) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                pagerAdapter.setItems(adapters);
            }
        });

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String eid = prefs.getString("LAST_ADAPTER", null);

        Adapter selectedAdapter = null;
        int lastAdapterIdx = -1;

        int i = 0;
        for (Adapter adapter : adapters) {
            server.requestServers(adapter.getEId());

            String name = adapter.getEId();
            if (name.equals(eid)) {
                selectedAdapter = adapter;
                lastAdapterIdx = i;
            }

            i++;
        }

        final Adapter finalSelectedAdapter = selectedAdapter;
        final int finalLastAdapterIdx = lastAdapterIdx;

        handler.post(new Runnable() {
            @Override
            public void run() {
                if (pagerAdapter.currentAdapter == null && finalSelectedAdapter != null) {
                    selectAdapter(finalSelectedAdapter);
                }

                if (finalLastAdapterIdx != -1) {
                    pagerAdapter.setCurrentAdapter(finalLastAdapterIdx);
                    ((ViewPager) findViewById(R.id.pager)).setCurrentItem(finalLastAdapterIdx);
                    hideNochat();
                }

                if (adapters.size() < 1) {
                    Intent i = new Intent(MainActivity.this, AccountsMgmtActivity.class);
                    i.putExtra("force_add", true);
                    startActivity(i);
                }

                findViewById(R.id.loading_overlay).setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onAdapterCreate(Adapter adapter) {
        onAdaptersLoaded(server.getLoadedAdapters());
    }

    @Override
    public void onAdapterUpdate(Adapter adapter) {
        onAdaptersLoaded(server.getLoadedAdapters());
    }

    @Override
    public void onAdapterRemove(String eid) {
        onAdaptersLoaded(server.getLoadedAdapters());
    }

    @Override
    public void onServersLoaded(final boolean fromCache, final String eid, final Server[] servers) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        final String saved_eid = prefs.getString("LAST_ADAPTER", "");
        final String saved_sid = prefs.getString("LAST_SERVER_ON_" + eid, null);

        Server selectedServer = null;

        for (Server server : servers) {
            if (server.sid.equals(saved_sid)) {
                selectedServer = server;
            }
        }

        if (selectedServer == null && servers.length > 0) {
            selectedServer = servers[0];
        }

        final Server finalSelectedServer = selectedServer;

        handler.post(new Runnable() {
            @Override
            public void run() {
                pagerAdapter.setData(eid, servers);

                if (finalSelectedServer != null) {

                    if (saved_eid.equals(eid)) selectServer(finalSelectedServer);
                    else pagerAdapter.setCurrentServer(eid, finalSelectedServer);

                    if (!fromCache) server.requestChannels(eid, finalSelectedServer.sid);
                }
            }
        });
    }

    @Override
    public void onChannelsLoaded(final boolean fromCache, final String eid, final String sid, final Channel[] channels) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String cid = prefs.getString("LAST_CHANNEL_ON_" + eid + sid, null);
        Channel selectedChannel = null;

        for (Channel channel : channels) {
            if (channel.cid.equals(cid)) {
                selectedChannel = channel;
            }
        }

        if (selectedChannel == null && channels.length > 0) {
            selectedChannel = channels[0];
        }

        final Channel finalChannel = selectedChannel;

        boolean serverIsValid = pagerAdapter.currentServer != null
                && pagerAdapter.currentServer.sid.equals(sid)
                && pagerAdapter.currentServer.eid.equals(eid);
        boolean selectedChannelExists = pagerAdapter.currentChannel != null;
        boolean selectedChannelNotSame = false;
        if (selectedChannelExists)
            selectedChannelNotSame = !pagerAdapter.currentChannel.sid.equals(sid);

        final boolean shouldSelectChannel = serverIsValid && (!selectedChannelExists || selectedChannelNotSame);
        final Channel finalSelectedChannel = selectedChannel;

        handler.post(new Runnable() {
            @Override
            public void run() {
                pagerAdapter.setChannels(eid, sid, channels);
                if (shouldSelectChannel) selectChannel(finalChannel);
                if (finalSelectedChannel != null)
                    pagerAdapter.setCurrentChannel(finalSelectedChannel);
            }
        });

    }

    @Override
    public void onMessagesLoaded(boolean fromCache, String eid, String cid, final Message[] messages) {
        if (pagerAdapter.currentChannel != null && pagerAdapter.currentChannel.eid.equals(eid) && pagerAdapter.currentChannel.cid.equals(cid)) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    messagesAdapter.setData(messages);
                    messagesAdapter.setTopLoadingDone();
                    hideNochat();
                }
            });
        }
    }

    @Override
    public void onMessagesBeforeLoaded(boolean fromCache, String eid, String cid, long date, final Message[] messages) {
        if (pagerAdapter.currentChannel != null && pagerAdapter.currentChannel.eid.equals(eid) && pagerAdapter.currentChannel.cid.equals(cid)) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    ListView messagesList = findViewById(R.id.messages_list);

                    int visiblePosition = messagesList.getFirstVisiblePosition();
                    Message item = messagesAdapter.getItem(visiblePosition);
                    int scrollY = 0;
                    View child = messagesList.getChildAt(messagesList.getHeaderViewsCount() + visiblePosition);
                    if (child != null) scrollY = child.getScrollY();

                    messagesAdapter.addData(messages);
                    messagesAdapter.setTopLoadingDone();
                    hideNochat();

                    messagesList.setSelection(messagesAdapter.getPostition(item));
                    messagesList.scrollListBy(scrollY);
                }
            });
        }
    }
}

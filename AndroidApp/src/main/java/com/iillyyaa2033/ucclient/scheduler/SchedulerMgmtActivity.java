package com.iillyyaa2033.ucclient.scheduler;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.iillyyaa2033.ucclient.MainActivity;
import com.iillyyaa2033.ucclient.R;
import com.iillyyaa2033.ucscheduler.Executor;
import com.iillyyaa2033.ucscheduler.Task;

public class SchedulerMgmtActivity extends Activity {

    ArrayAdapter<Task> adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActionBar() != null) getActionBar().setDisplayHomeAsUpEnabled(true);

        ListView list = new ListView(this);
        adapter = new ArrayAdapter<Task>(this, android.R.layout.simple_list_item_1) {

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

                if (convertView == null) {
                    convertView = LayoutInflater.from(SchedulerMgmtActivity.this).inflate(R.layout.item_taskinfo, parent, false);
                }

                Task item = getItem(position);
                ((TextView) convertView.findViewById(R.id.title)).setText(item.getDescription());

                String descr;

                if (item.getSupportedParams().length > 0) {
                    descr = "Parameters: ";
                    for (String param : item.getSupportedParams()) {
                        descr += "\n  " + param + ": " + item.getParam(param);
                    }
                } else {
                    descr = "Task doesnt have any parameters";
                }
                ((TextView) convertView.findViewById(R.id.descr)).setText(descr);


                return convertView;
            }
        };
        list.setAdapter(adapter);
        setContentView(list);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                final Task task = adapter.getItem(i);

                PopupMenu pm = new PopupMenu(SchedulerMgmtActivity.this, view);
                Menu menu = pm.getMenu();
                menu.add(0, 0, 0, "Edit task");
                menu.add(1, 1, 1, "Remove task");

                pm.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case 0:
                                showEditTaskDialog(task);
                                break;
                            case 1:
                                showRemoveTaskDialog(task);
                                break;
                        }

                        return true;
                    }
                });
                pm.show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateTaskList();
    }

    void addTask(Task task) {
        SchedulerTools.getExecutor(this).addTask(task);
        SchedulerTools.saveExecutor(this);
        SchedulerTools.scheduleNextAlarm(this);
        updateTaskList();
    }

    void removeTask(Task task) {
        SchedulerTools.getExecutor(SchedulerMgmtActivity.this).removeTask(task);
        SchedulerTools.saveExecutor(SchedulerMgmtActivity.this);
        SchedulerTools.scheduleNextAlarm(this);
        updateTaskList();
    }

    void updateTaskList() {
        Executor ex = SchedulerTools.getExecutor(this);
        if (ex != null) {
            adapter.clear();
            adapter.addAll(ex.getTasks());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        SchedulerTools.saveExecutor(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add(0, 0, 0, "Add new task")
                .setIcon(android.R.drawable.ic_menu_add)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case 0:
                showAddTaskDialog();
                break;
            case android.R.id.home:
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    void showAddTaskDialog() {

        final Task[] availTasks = Executor.getAvailableTasks();

        String[] names = new String[availTasks.length];
        for (int i = 0; i < availTasks.length; i++) {
            names[i] = availTasks[i].toString();
        }

        new AlertDialog.Builder(this)
                .setTitle("Select task to add")
                .setItems(names, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        showEditTaskDialog(availTasks[i]);
                    }
                })
                .show();
    }

    void showRemoveTaskDialog(final Task task) {
        if (task.isSystem()) {
            new AlertDialog.Builder(SchedulerMgmtActivity.this)
                    .setMessage("Do you sure want to remove system task?")
                    .setNegativeButton("No", null)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            removeTask(task);
                        }
                    })
                    .show();
        } else {
            removeTask(task);
        }
    }

    void showEditTaskDialog(final Task task) {

        final LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);

        if (task.getSupportedParams().length > 0) {
            for (String param : task.getSupportedParams()) {
                TextView textView = new TextView(this);
                textView.setText(param);
                ll.addView(textView);

                EditText text = new EditText(this);
                text.setHint(param);
                if (task.getParam(param) != null) text.setText(task.getParam(param));

                ll.addView(text);
            }
        } else {
            TextView textView = new TextView(this);
            textView.setText("No supported params specified");
        }

        new AlertDialog.Builder(this)
                .setTitle("Edit task params")
                .setView(ll)
                .setNegativeButton("Cancel", null)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int itm) {

                        for (int i = 0; i < ll.getChildCount(); i++) {
                            View view = ll.getChildAt(i);
                            if (view instanceof EditText) {
                                String key = ((EditText) view).getHint().toString();
                                String val = ((EditText) view).getText().toString();
                                task.setParam(key, val);
                            }
                        }

                        removeTask(task);
                        addTask(task);
                    }
                })
                .show();
    }


}

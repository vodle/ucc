package com.iillyyaa2033.ucclient.scheduler;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.iillyyaa2033.UCStorage;
import com.iillyyaa2033.ucclient.JustAnotherUtils;
import com.iillyyaa2033.ucclient.MainActivity;
import com.iillyyaa2033.ucclient.R;
import com.iillyyaa2033.ucscheduler.TaskCallback;
import com.iillyyaa2033.ucserver.Adapter;
import com.iillyyaa2033.ucserver.AdapterCallback;
import com.iillyyaa2033.ucserver.AdapterHolder;
import com.iillyyaa2033.ucserver.Module;

import java.io.File;
import java.sql.SQLException;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {

        Adapter[] adapters = {};

        if(MainActivity.server != null) {
            adapters = MainActivity.server.getLoadedAdapters().toArray(new Adapter[0]);
        } else {
            AdapterHolder holder = new AdapterHolder(null);
            for(Module module : MainActivity.getKnownModules(context)) holder.addModule(module);

            String path = context.getFilesDir().getPath() + "/databases/main;FILE_LOCK=FS;PAGE_SIZE=1024;CACHE_SIZE=8192;";
            String cache = context.getCacheDir().getAbsolutePath();

            try {
                UCStorage storage = UCStorage.connect(path);
                holder.loadAdapters(storage.getAdapters());
                holder.restoreStates(new File(cache));
                storage.disconnect();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            adapters = holder.all().toArray(new Adapter[0]);
        }

        System.out.println("AlarmReceiver triggered: server("+MainActivity.server+") cnt("+adapters.length+")");

        final TaskCallback callback = getCallback(context);

        final Adapter[] finalAdapters = adapters;
        new Thread(){

            @Override
            public void run() {
                SchedulerTools.getExecutor(context).execute(finalAdapters, callback);
                SchedulerTools.scheduleNextAlarm(context);
                SchedulerTools.saveExecutor(context);
            }
        }.start();
    }

    private TaskCallback getCallback(final Context context){
        return new TaskCallback(){

            @Override
            public void notification(String title, String message) {

                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                if(manager == null) return;

                Intent notificationIntent = new Intent(context, MainActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

                Notification.Builder builder;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    String channel = "scheduler_notif";
                    NotificationChannel chan = new NotificationChannel(channel, "UCC scheduler", NotificationManager.IMPORTANCE_NONE);
                    manager.createNotificationChannel(chan);
                    builder = new Notification.Builder(context, channel);
                } else {
                    builder = new Notification.Builder(context);
                }

                Notification notification = builder
                        .setContentTitle("UCC: "+title)
                        .setContentText(message)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentIntent(pendingIntent)
                        .build();

                manager.notify((int) System.currentTimeMillis(), notification);
            }

            @Override
            public void download(String url) {
                try {
                    JustAnotherUtils.launchAndroidDownloader(context, Uri.parse(url));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }
}

package com.iillyyaa2033.ucclient.scheduler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.iillyyaa2033.ucscheduler.Executor;

import java.io.File;
import java.io.IOException;

public class SchedulerTools {

    private static Executor executor = null;

    public static void scheduleNextAlarm(Context context){
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pi = getIntent(context);

        if(manager != null) {
            manager.set(AlarmManager.RTC_WAKEUP, getExecutor(context).getNextWakeupTime(), pi);
            System.out.println("Registered new alarm");
        }
    }

    static synchronized Executor getExecutor(Context context){
        if(executor == null){
            executor = new Executor();

            try {
                executor.onRestoreState(new File(context.getCacheDir().getAbsoluteFile(), "scheduler"));
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return executor;
    }

    static void saveExecutor(Context context){
        if(executor != null){
            try {
                executor.onSaveState(new File(context.getCacheDir().getAbsoluteFile(), "scheduler"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static PendingIntent getIntent(Context context){
        Intent i = new Intent(context, AlarmReceiver.class);
        return PendingIntent.getBroadcast(context, 0, i, 0);
    }
}

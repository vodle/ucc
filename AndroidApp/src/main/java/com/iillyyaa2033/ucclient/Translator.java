package com.iillyyaa2033.ucclient;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

public class Translator {

    private static final String URL = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=%s";
    private static final String KEY = "trnsl.1.1.20190808T183411Z.335d1a91f7d676fe.65018b94644511ad158f1b51fc487a602b104daf";

    public static String[] names = {
        "ru",
        "en",
        "pl"
    };

    /**
     * Translate text using Yandex.Translate service
     *
     * <p>Note: original code belongs to <a href="https://habr.com/en/users/zdanevich-vitaly/">zdanevich-vitaly</a>
     * was posted on https://habr.com/ru/post/261259/</p>
     *
     * @param input text to translate
     * @param lang target language
     * @return translated text
     * @throws IOException when something went wrong
     */
    public static String translate(String input, String lang) throws IOException {
        String urlStr = String.format(URL, KEY);
        URL urlObj = new URL(urlStr);
        HttpsURLConnection connection = (HttpsURLConnection)urlObj.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
        dataOutputStream.writeBytes("text=" + URLEncoder.encode(input, "UTF-8") + "&lang=" + lang);

        InputStream response = connection.getInputStream();
        String json = new java.util.Scanner(response).nextLine();
        int start = json.indexOf("[");
        int end = json.indexOf("]");
        return json.substring(start + 2, end - 1);
    }
}

package com.iillyyaa2033.ucclient;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.iillyyaa2033.ucclient.adapters.AdaptersViewAdapter;
import com.iillyyaa2033.ucserver.Adapter;
import com.iillyyaa2033.ucserver.IServerListener;
import com.iillyyaa2033.ucserver.Module;
import com.iillyyaa2033.ucserver.ServerListenerStub;

import java.util.HashMap;
import java.util.List;

public class AccountsMgmtActivity extends Activity {

    private AdaptersViewAdapter adapter;
    private ServerListenerStub listener;

    private static final int AUTH_PROC = 2;
    private String authProcClass = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActionBar() != null) getActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_accounts);

        if (getIntent() != null) {
            if (getIntent().getBooleanExtra("force_add", false)) {
                showAddAdapterDialog(false);
            }
        }

        ListView list = findViewById(R.id.list);
        adapter = new AdaptersViewAdapter(this);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                showDeleteAdapterDialog(adapter.getItem(i));
            }
        });

        final Handler handler = new Handler();
        listener = new ServerListenerStub(){

            @Override
            public void onAdapterCreate(Adapter adapter) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        updAdaptersList();
                    }
                });
            }

            @Override
            public void onAdapterUpdate(Adapter adapter) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        updAdaptersList();
                    }
                });
            }

            @Override
            public void onAdapterRemove(String adapter) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        updAdaptersList();
                    }
                });
            }
        };
        MainActivity.server.addListener(listener);
    }

    @Override
    protected void onResume() {
        super.onResume();

        updAdaptersList();
    }

    @Override
    protected void onDestroy() {
        MainActivity.server.removeListener(listener);

        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AUTH_PROC) {

                if (authProcClass != null) {
                    HashMap<String, String> results = new HashMap<>();

                    if (data.getExtras() != null)
                        for (String key : data.getExtras().keySet())
                            results.put(key, data.getStringExtra(key));

                    MainActivity.server.finishAuthProc(authProcClass, results);
                }

                authProcClass = null;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add(0, 0, 0, "")
                .setIcon(android.R.drawable.ic_menu_add)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case 0:
                showAddAdapterDialog(true);
                break;
            case android.R.id.home:
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updAdaptersList() {
        if (adapter != null) adapter.setItems(MainActivity.server.getLoadedAdapters());
    }

    private void showAddAdapterDialog(boolean cancelable) {
        final List<Module.Descr> descrs = MainActivity.server.getAvailableAdapters();
        String[] items = new String[descrs.size()];

        for (int i = 0; i < descrs.size(); i++) {
            items[i] = descrs.get(i).getHumanName() + "\n" + descrs.get(i).getDescription();
        }

        new AlertDialog.Builder(this)
                .setTitle(R.string.select_service_to_login)
                .setItems(items, new AlertDialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        authProcClass = descrs.get(i).getClassName();
                        AuthProcActivity.target = MainActivity.server.getAuthProcFor(authProcClass);
                        startActivityForResult(new Intent(AccountsMgmtActivity.this, AuthProcActivity.class), AUTH_PROC);
                    }
                })
                .setCancelable(cancelable)
                .show();
    }

    private void showDeleteAdapterDialog(final Adapter adapter) {
        if (adapter != null) {
            new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.del_account_confirm, adapter.getServiceName()))
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new AlertDialog.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            MainActivity.server.delAdapter(adapter.getEId());
                            updAdaptersList();
                        }
                    })
                    .show();
        }
    }
}

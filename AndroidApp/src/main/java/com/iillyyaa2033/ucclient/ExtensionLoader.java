package com.iillyyaa2033.ucclient;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.FeatureInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.iillyyaa2033.ucserver.Module;

import java.util.ArrayList;
import java.util.List;

import dalvik.system.PathClassLoader;

/**
 * <p>
 *     Simple implementation of extension system for android.
 * </p><p>
 *     Initially created by Inorichi for Tachiyomi project.
 * </p><p>
 *     This code just rewritten on java with some simplifications.
 * </p>
 *
 * @see <a href="https://github.com/inorichi/tachiyomi/blob/master/app/src/main/java/eu/kanade/tachiyomi/extension/util/ExtensionLoader.kt">Tachiyomi github page</a>
 */
public class ExtensionLoader {

    /**
     * This feature should be declared in module's AndroidManifest
     * <pre>
     *     <uses-feature android:name="com.iillyyaa2033.ucc.ext"/>
     * </pre>
     */
    public static final String FEATURE = "com.iillyyaa2033.ucc.ext";

    /**
     * This metadata should be declared in module's AndroidManifest, within application tag
     * <pre>
     *      <meta-data
     *          android:name="com.iillyyaa2033.ucc.ext.class"
     *          android:value=".TgModule"/>
     * </pre>
     */
    public static final String METADATA = "com.iillyyaa2033.ucc.ext.class";

    /**
     * Load all installed modules
     *
     * <p>
     *     On Android, module is application that matches two conditions:
     *     <ol>
     *         <li>declared uses-feature tag</li>
     *         <li>declared meta-data tag</li>
     *     </ol>
     * </p><p>
     *     Also Android app should have same sharedUserId and sign if it loads native libraries.
     * </p>
     *
     * @param context application context
     * @return list of successfully loaded modules
     */
    public static List<Module> loadAll(Context context){
        int flags = PackageManager.GET_CONFIGURATIONS;
        PackageManager pm = context.getPackageManager();
        List<PackageInfo> packages = pm.getInstalledPackages(flags);

        ArrayList<Module> exts = new ArrayList<>();
        for(PackageInfo pg : packages){
            if(pg.reqFeatures == null) continue;

            for(FeatureInfo info : pg.reqFeatures){
                if(FEATURE.equals(info.name)){

                    Module ext = instantiateModule(context, pg);
                    if(ext != null){
                        try {
                            ext.loadNatives(pg.applicationInfo.nativeLibraryDir);
                            exts.add(ext);
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        return exts;
    }

    private static Module instantiateModule(Context context, PackageInfo packageInfo){
        PackageManager pm = context.getPackageManager();

        ApplicationInfo info;
        try {
            info = pm.getApplicationInfo(packageInfo.packageName, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        // TODO: version check
        // TODO: signature check

        String packagename = info.packageName;
        String classname = info.metaData.getString(METADATA);
        if(classname == null) return null;
        if(classname.startsWith(".")) classname = packagename+classname;



        PathClassLoader classLoader = new PathClassLoader(info.sourceDir, null, context.getClassLoader());

        try {
            Class clazz = Class.forName(classname, false, classLoader);

            // TODO: check if implementation has same structure as our interface

            return (Module) clazz.newInstance();
        } catch (Throwable e) {
            e.printStackTrace();

            return null;
        }
    }
}


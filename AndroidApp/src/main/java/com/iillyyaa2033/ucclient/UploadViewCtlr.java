package com.iillyyaa2033.ucclient;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.iillyyaa2033.ucserver.Adapter;

class UploadViewCtlr {

    private TextView view;

    private int count = 0;

    UploadViewCtlr(TextView view){
        this.view = view;
    }

    void increaseCountBy(int amount){
        this.count += amount;
    }

    void update(int progress){

        System.out.println("Upload "+count+" "+progress);

        Context context = view.getContext();

        switch (progress){
            case Adapter.UploadProgress.STARTED:
                view.setText(context.getString(R.string.upload_started, count));
                break;
            case Adapter.UploadProgress.WAIT_SERVER:
                view.setText(context.getString(R.string.upload_wait_server, count));
                break;
            case Adapter.UploadProgress.DONE:
                count--;
                break;
        }

        if(count <= 0){
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
        }
    }
}

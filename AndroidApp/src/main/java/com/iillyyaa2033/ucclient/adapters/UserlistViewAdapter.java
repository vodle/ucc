package com.iillyyaa2033.ucclient.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.iillyyaa2033.ucclient.MainActivity;
import com.iillyyaa2033.ucclient.R;
import com.iillyyaa2033.ucmodel.User;

public class UserlistViewAdapter extends BaseAdapter {

    private Context context;

    public UserlistViewAdapter(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public User getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    @SuppressLint("InflateParams")
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_user, null, false);
        }

        User user = getItem(i);
        if(user != null) {
            ((TextView) convertView.findViewById(R.id.username)).setText(user.username);

            ImageView avatar = convertView.findViewById(R.id.avatar);
            if(user.icon_res != null && MainActivity.FEATURE_IMAGE_LOADING) {
                Glide.with(context)
                        .load(user.icon_res)
                        .into(avatar);
            } else {
                avatar.setImageResource(android.R.drawable.screen_background_dark_transparent);
            }
        }

        return convertView;
    }
}

package com.iillyyaa2033.ucclient.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.iillyyaa2033.ucclient.R;

import java.io.File;
import java.util.ArrayList;

public class AttachmentsAdapter extends RecyclerView.Adapter<AttachmentsAdapter.MyVH> {

    private Context context;
    private RecyclerView rv;

    private ArrayList<String> attachments = new ArrayList<>();


    public AttachmentsAdapter(Context context){
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        this.rv = recyclerView;
    }

    @NonNull
    @Override
    public MyVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(context).inflate(R.layout.item_attachment, null, false);
        return new MyVH(root);
    }

    @Override
    public void onBindViewHolder(@NonNull MyVH holder, final int position) {
        File item = new File(attachments.get(position));

        Glide.with(context)
                .load(item)
                .centerCrop()
                .into(holder.preview);

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attachments.remove(position);
                notifyDataSetChanged();
                updateVisibility();
            }
        });
    }

    @Override
    public int getItemCount() {
        return attachments.size();
    }

    public void add(String attach){
        attachments.add(attach);
        notifyDataSetChanged();
        updateVisibility();
    }

    public void clear(){
        attachments.clear();
        notifyDataSetChanged();
        updateVisibility();
    }

    public String[] all(){
        return attachments.toArray(new String[attachments.size()]);
    }

    private void updateVisibility(){
        rv.setVisibility(getItemCount() == 0 ? View.GONE : View.VISIBLE);
    }

    class MyVH extends RecyclerView.ViewHolder {

        ImageView preview;
        TextView button;

        public MyVH(View itemView) {
            super(itemView);

            preview = itemView.findViewById(R.id.attach_image);
            button = itemView.findViewById(R.id.btn);
        }
    }
}

package com.iillyyaa2033.ucclient.adapters;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.iillyyaa2033.ucclient.R;
import com.iillyyaa2033.ucmodel.User;
import com.iillyyaa2033.ucserver.Adapter;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class AdaptersViewAdapter extends BaseAdapter {

    private Handler handler = new Handler();
    private Context context;

    private Adapter[] adapters = {};

    public AdaptersViewAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return adapters.length;
    }

    @Override
    public Adapter getItem(int i) {
        return adapters[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_adapter, null, false);

        Adapter adapter = getItem(i);
        if(adapter != null) {
            asyncUsername((ImageView) v.findViewById (R.id.avatar), (TextView) v.findViewById(R.id.account), adapter);
            ((TextView) v.findViewById(R.id.adapter)).setText(adapter.getServiceName());
        }

        return v;
    }

    private void asyncUsername(final ImageView image, final TextView view, final Adapter adapter){
        view.setText(R.string.loading);

        new Thread(){

            @Override
            public void run() {
                final User user = adapter != null ? adapter.getAccountUser() : null;
                final String username = user != null ? user.toString() : "Unknown";

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        view.setText(username);

                        if(user != null) Glide.with(context).load(user.icon_res).into(image);
                    }
                });
            }
        }.start();
    }

    public void setItems(Collection<Adapter> adapters){
        this.adapters = adapters.toArray(new Adapter[0]);
        notifyDataSetChanged();
    }

    public void delete(String eId) {
        List<Adapter> adptrs = Arrays.asList(adapters);
        for(int i = 0; i < adptrs.size(); i++){
            Adapter a = adptrs.get(i);
            if(eId.equals(a.getEId())){
                adptrs.remove(i);
                return;
            }
        }
    }
}

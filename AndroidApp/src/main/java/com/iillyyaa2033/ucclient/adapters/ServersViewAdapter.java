package com.iillyyaa2033.ucclient.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.iillyyaa2033.ucclient.MainActivity;
import com.iillyyaa2033.ucclient.R;
import com.iillyyaa2033.ucmodel.Server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class ServersViewAdapter extends RecyclerView.Adapter<ServersViewAdapter.MyVH> {

    private Context context;
    private OnItemClick listener;

    private RecyclerView rv;

    private String currentServer = null;
    private ArrayList<Server> data;

    private TypedValue bgSelected;

    public ServersViewAdapter(Context context) {
        this.context = context;
        data = new ArrayList<>();

        bgSelected = new TypedValue();
        context.getTheme().resolveAttribute(android.R.attr.colorAccent, bgSelected, true);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.rv = recyclerView;
    }

    @NonNull
    @Override
    public MyVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(context).inflate(R.layout.item_server, parent, false);
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rv != null && listener != null) {
                    int itemPosition = rv.getChildLayoutPosition(view);
                    listener.onClick(data.get(itemPosition));
                }
            }
        });

        root.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(rv != null && listener != null) {
                    int itemPosition = rv.getChildLayoutPosition(view);
                    listener.onLongClick(view, data.get(itemPosition));

                    return true;
                }

                return false;
            }
        });

        return new MyVH(root);
    }

    @Override
    public void onBindViewHolder(@NonNull MyVH holder, int position) {
        Server item = data.get(position);

        boolean isSelected = item.sid.equals(currentServer);
        if(isSelected) holder.convertView.setBackgroundColor(bgSelected.data);
        else holder.convertView.setBackgroundColor(Color.TRANSPARENT);

        if(item instanceof Server) {
            Server i = (Server) item;
            if (MainActivity.FEATURE_IMAGE_LOADING && i.icon_res != null && !i.icon_res.isEmpty()) {
                Glide.with(context)
                        .load(i.icon_res)
                        .error(android.R.drawable.screen_background_dark_transparent)
                        .centerCrop()
                        .into(holder.icon);
            } else {
                holder.icon.setImageResource(android.R.drawable.screen_background_dark_transparent);
            }
        }

        if(item.unread_count < 1){
            holder.indicatorUnreads.setVisibility(View.GONE);
        } else {
            holder.indicatorUnreads.setVisibility(View.VISIBLE);
            holder.indicatorUnreads.setText(""+item.unread_count);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public void setListener(OnItemClick listener){
        this.listener = listener;
    }


    private void onDataChanged(){
        if(rv == null) return;

        if(getItemCount() > 1){
            rv.setVisibility(View.VISIBLE);
        } else {
            rv.setVisibility(View.GONE);
        }
    }

    public void setData(Server[] data){
        this.data.clear();
        this.data.addAll(Arrays.asList(data));
        Collections.sort(this.data);
        notifyDataSetChanged();
        onDataChanged();
    }


    public void setData(Collection<Server> data){
        this.data.clear();
        this.data.addAll(data);
        Collections.sort(this.data);
        notifyDataSetChanged();
        onDataChanged();
    }

    public synchronized void updateServers(ArrayList<Server> servers){
        ArrayList<Server> rz = new ArrayList<>();

        for(Server oldServer : data){
            boolean newServerWasAdded = false;

            for(Server newServer : servers) {
                if(oldServer.sid.equals(newServer.sid)) {
                    rz.add(newServer);
                    servers.remove(newServer);
                    newServerWasAdded = true;
                    break;
                }
            }

            if(!newServerWasAdded) rz.add(oldServer);

        }

        this.data = rz;

        Collections.sort(this.data);
        notifyDataSetChanged();
        onDataChanged();
    }

    public synchronized void removeServers(ArrayList<Server> servers){
        ArrayList<Server> rz = new ArrayList<>();

        for(Server old : data){
            boolean removed = false;

            for (Server ns : servers){
                if(old.sid.equals(ns.sid)){
                    removed = true;
                    servers.remove(ns);
                    break;
                }
            }

            if(!removed){
                rz.add(old);
            }
        }

        this.data = rz;

        notifyDataSetChanged();
        onDataChanged();
    }

    public void setCurrentServer(String server){
        this.currentServer = server;
        notifyDataSetChanged();
    }


    class MyVH extends RecyclerView.ViewHolder {

        View convertView;
        ImageView icon;
        TextView indicatorUnreads;

        MyVH(View convertView) {
            super(convertView);

            this.convertView = convertView;
            this.icon = convertView.findViewById(R.id.icon);
            this.indicatorUnreads = convertView.findViewById(R.id.indicator_unreads);
        }
    }

    public interface OnItemClick {

        void onClick(Server item);

        void onLongClick(View view, Server item);
    }
}

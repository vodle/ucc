package com.iillyyaa2033.ucclient.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.iillyyaa2033.ucclient.MainActivity;
import com.iillyyaa2033.ucclient.R;
import com.iillyyaa2033.ucmodel.Channel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class ChannelsViewAdapter extends BaseAdapter {

    private Context context;
    private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());

    private boolean showHidden = false;

    private String currentCid = null;

    private final List<Channel> data;

    private TypedValue bgSelected, bgUnread;

    public ChannelsViewAdapter(Context context) {
        this.context = context;

        showHidden = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_show_hidden_channels", false);

        bgSelected = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.selected_background, bgSelected, true);

        bgUnread = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.unread_background, bgUnread, true);

        data = Collections.synchronizedList(new ArrayList<Channel>());
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Channel getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Channel channel = getItem(position);

        int unreadsCount = channel.unread_count;

        if(!showHidden && channel.is(Channel.FLAG_HIDDEN) && unreadsCount <= 0){
            return new View(context);
        }


        boolean isChannelUnread = channel.is(Channel.FLAG_UNREAD) || unreadsCount != 0;

        switch (channel.draw_mode) {
            case Channel.ChannelDrawMode.DEFAULT:
                convertView = LayoutInflater.from(context).inflate(R.layout.item_channel_default, null, false);
                ((TextView) convertView.findViewById(R.id.date)).setText(sdf.format(new Date(getItem(position).last_time)));
                ((TextView) convertView.findViewById(R.id.descr)).setText(getItem(position).last_msg);
                break;
            case Channel.ChannelDrawMode.MINIMIZED:
                convertView = LayoutInflater.from(context).inflate(R.layout.item_channel_minimized, null, false);
                ((TextView) convertView.findViewById(R.id.date)).setText("");
                break;
            case Channel.ChannelDrawMode.CATEGORY:
                convertView = LayoutInflater.from(context).inflate(R.layout.item_channel_category, null, false);
                break;
        }

        TextView title = convertView.findViewById(R.id.header);
        title.setText(getItem(position).title);

        boolean isSelected = channel.cid.equals(currentCid);
        if (isSelected) {
            convertView.setBackgroundColor(bgSelected.data);
            title.setTypeface(title.getTypeface(), Typeface.NORMAL);
        } else if (isChannelUnread) {
            convertView.setBackgroundColor(bgUnread.data);
            title.setTypeface(title.getTypeface(), Typeface.BOLD);
        } else {
            convertView.setBackgroundColor(Color.TRANSPARENT);
            title.setTypeface(title.getTypeface(), Typeface.NORMAL);
        }

        if (channel.draw_mode == Channel.ChannelDrawMode.DEFAULT) {
            if (channel.icon != null && MainActivity.FEATURE_IMAGE_LOADING) {
                Glide.with(context).load(channel.icon).into((ImageView) convertView.findViewById(R.id.imageView));
            } else {
                ((ImageView) convertView.findViewById(R.id.imageView)).setImageResource(R.mipmap.ic_launcher);
            }
        }


        if (unreadsCount == 0) {
            (convertView.findViewById(R.id.indicator_unreads)).setVisibility(View.GONE);
        } else {
            TextView unreads = convertView.findViewById(R.id.indicator_unreads);
            unreads.setVisibility(View.VISIBLE);
            unreads.setText("" + (unreadsCount > 0 ? unreadsCount : "⬤"));
        }

        return convertView;
    }

    public void setChannels(Channel[] channels) {

        synchronized (data) {
            data.clear();
            data.addAll(Arrays.asList(channels));
            sort(data);
        }

        notifyDataSetChanged();
    }

    public void setCurrent(String cid) {
        this.currentCid = cid;
        notifyDataSetChanged();
    }

    public void update(Channel... channels) {
        if (channels == null) return;

        synchronized (data) {

            for (Channel newChannel : channels) {
                if (newChannel == null) continue;

                Iterator<Channel> iterator = data.iterator();

                while (iterator.hasNext()) {
                    Channel oldChannel = iterator.next();

                    if (oldChannel.equals(newChannel)) {
                        iterator.remove();
                    }
                }

                data.add(newChannel);

            }

            sort(data);
        }

        notifyDataSetChanged();
    }


    private void sort(final List<Channel> list) {
        Collections.sort(list, new Comparator<Channel>() {
            @Override
            public int compare(Channel left, Channel right) {

                long priorLeft = left.priority;
                long priorRight = right.priority;

                Channel parentLeft = get(left.eid, left.sid, left.parent_id);
                if (parentLeft != null) priorLeft += parentLeft.priority * 100 + 1;
                else priorLeft *= 100;

                Channel parentRight = get(right.eid, right.sid, right.parent_id);
                if (parentRight != null) priorRight += parentRight.priority * 100 + 1;
                else priorRight *= 100;


                return Long.compare(priorLeft, priorRight);
            }

            private Channel get(String eid, String sid, String cid) {
                for (Channel ch : list) {
                    if (ch.eid.equals(eid) && ch.sid.equals(sid) && ch.cid.equals(cid)) {
                        return ch;
                    }
                }

                return null;
            }
        });
    }
}

package com.iillyyaa2033.ucclient.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.text.format.DateUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.browser.customtabs.CustomTabsIntent;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.flexbox.FlexboxLayout;
import com.iillyyaa2033.ucclient.JustAnotherUtils;
import com.iillyyaa2033.ucclient.MainActivity;
import com.iillyyaa2033.ucclient.MdNewlinePlugin;
import com.iillyyaa2033.ucclient.R;
import com.iillyyaa2033.ucmodel.EnhancedText;
import com.iillyyaa2033.ucmodel.Message;
import com.iillyyaa2033.ucmodel.MessageMetaPatch;
import com.iillyyaa2033.ucmodel.MessagePart;
import com.iillyyaa2033.ucmodel.MessagePartAttachment;
import com.iillyyaa2033.ucmodel.MessagePartInline;
import com.iillyyaa2033.ucmodel.MessagePartText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.noties.markwon.Markwon;
import io.noties.markwon.ext.strikethrough.StrikethroughPlugin;
import io.noties.markwon.ext.tasklist.TaskListPlugin;
import io.noties.markwon.image.AsyncDrawable;
import io.noties.markwon.image.glide.GlideImagesPlugin;
import io.noties.markwon.movement.MovementMethodPlugin;

@SuppressLint("InflateParams")
public class MessagesViewAdapter extends BaseAdapter implements AbsListView.OnScrollListener {

    private static final int COLOR_WHITE = Color.parseColor("#DDDDDD");
    private static final int COLOR_GREY = Color.parseColor("#666666");

    /**
     * Maximum time difference between two messages to hide message header (icon, name, date)
     */
    private static final long CONCAT_LIMIT = 15 * 60 * 1000;

    private static final int PREVIEW_SIDE = 512;
    private static final float EMOJI_SIZE = 16f;

    private final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy HH:mm", Locale.getDefault());
    private final SimpleDateFormat todaySDF = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private final SimpleDateFormat dateDividerSDF = new SimpleDateFormat("dd MMM yy", Locale.getDefault());

    private Context context;
    private Markwon markwon;

    private boolean showHidden;

    private EventsListener listener;
    private String customProxy = null;

    private int imageSide = 512; // In Dip
    private int stickerSide = 128; // In Dip

    private int unreadColor;

    private int emojiSide;
    private Drawable emojiPlaceholder;

    private View headerRoot;
    private volatile boolean isTopLoaded = false;

    private List<Message> data = new ArrayList<>();

    public MessagesViewAdapter(final Context context, EventsListener listener) {
        this.context = context;
        this.listener = listener;

        showHidden = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_show_hidden_messages", false);

        Markwon.Builder b = Markwon.builder(context)
                .usePlugin(StrikethroughPlugin.create())
                .usePlugin(TaskListPlugin.create(context))
                .usePlugin(new MdNewlinePlugin())
                .usePlugin(MovementMethodPlugin.create(ScrollingMovementMethod.getInstance()));

        if (MainActivity.FEATURE_IMAGE_LOADING) {

            b.usePlugin(GlideImagesPlugin.create(new GlideImagesPlugin.GlideStore() {

                @NonNull
                @Override
                public RequestBuilder<Drawable> load(@NonNull AsyncDrawable drawable) {
                    return Glide.with(context)
                            .load(drawable.getDestination())
                            .placeholder(emojiPlaceholder)
                            .override(emojiSide, emojiSide)
                            .centerCrop();
                }

                @Override
                public void cancel(@NonNull Target<?> target) {
                    Glide.with(context).clear(target);
                }

            }));
        }

        markwon = b.build();

        emojiSide = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, EMOJI_SIZE, context.getResources().getDisplayMetrics());
        imageSide = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, imageSide, context.getResources().getDisplayMetrics());
        stickerSide = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, stickerSide, context.getResources().getDisplayMetrics());

        TypedValue bgUnread = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.unread_background, bgUnread, true);
        unreadColor = bgUnread.data;

        ShapeDrawable drawable = new ShapeDrawable(new RectShape());
        drawable.setIntrinsicHeight(emojiSide);
        drawable.setIntrinsicWidth(emojiSide);
        drawable.getPaint().setARGB(0, 0, 0, 0);
        emojiPlaceholder = drawable;
    }


    // ========== //
    // ITEM SETUP //
    // ========== //
    @Override
    public View getView(int position, View convertViewUnused, ViewGroup parent) {

        final Message item = getItem(position);

        View root = null;
        if (position > 0) {
            Message prevItem = getItem(position - 1);

            // Ignore hidden message if previous was hidden too
            if (!showHidden && item.is(Message.FLAG_HIDDEN) && prevItem.is(Message.FLAG_HIDDEN)) {
                return new View(context);
            }

            // Minimize message if previous has same author
            if ((item.date - prevItem.date) < CONCAT_LIMIT && prevItem.uid.equals(item.uid)) {
                root = getDecorationEmpty();
            }
        }

        if (!showHidden && item.is(Message.FLAG_HIDDEN)) {
            root = getDecorationEmpty();
            LinearLayout ll = root.findViewById(R.id.content);
            ll.addView(getNote("Message(s) hidden"));
        } else {

            if (root == null) {
                root = getDecorationUser(item);
            }

            LinearLayout ll = root.findViewById(R.id.content);

            // Add parts
            for (final MessagePart part : item.parts) {
                if (part == null) continue;

                if (part instanceof MessagePartText)
                    ll.addView(getTextData(item, ((MessagePartText) part).text));
                else if (part instanceof MessagePartAttachment)
                    ll.addView(getAttachment((MessagePartAttachment) part));
                else if (part instanceof MessagePartInline)
                    ll.addView(getInline(item, (MessagePartInline) part));

            }
        }

        // Add date divider
        if (position + 1 < getCount()) {
            Message nextItem = getItem(position + 1);
            Date nextDate = new Date(nextItem.date);

            if (nextDate.getDate() != new Date(item.date).getDate()) {
                LinearLayout ll = root.findViewById(R.id.content);
                ll.addView(getDateDivider(nextDate));
            }
        }

        root.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listener.onLongClick(view, item);
                return true;
            }
        });

        if (item.is(Message.FLAG_UNREAD))
            root.setBackgroundColor(unreadColor);

        return root;
    }

    private View getDecorationUser(final Message item) {
        View root = LayoutInflater.from(context).inflate(R.layout.message_decor_user, null, false);

        TextView username = root.findViewById(R.id.username);
        TextView date = root.findViewById(R.id.date);
        ImageView avatar = root.findViewById(R.id.avatar);

        // Set username
        username.setText(item.username == null ? "???" : item.username);
        if (item.color == 0) {
            username.setTextColor(Color.parseColor("#ffffff"));
        } else {
            int r = (item.color & 0xFF0000) >> 16;
            int g = (item.color & 0xFF00) >> 8;
            int b = item.color & 0xFF;
            username.setTextColor(Color.rgb(r, g, b));
        }

        username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) listener.onUsernameClick(item);
            }
        });

        // Set avatar, if exists
        if (item.usericon != null && !item.usericon.trim().isEmpty()
                && MainActivity.FEATURE_IMAGE_LOADING) {
            Glide.with(context)
                    .load(item.usericon)
                    .into(avatar);
        } else {
            avatar.setImageResource(android.R.drawable.screen_background_dark_transparent);
        }
        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onAvatarClick(item);
            }
        });


        // Set date
        Date thisItemDate = new Date(item.date);
        String formattedDate = (DateUtils.isToday(item.date) ? todaySDF.format(thisItemDate) : sdf.format(thisItemDate));
        String formattedMarkers = item.is(Message.FLAG_EDITED) ? "*" : "";
        date.setText(String.format("%s %s", formattedMarkers, formattedDate));

        return root;
    }

    private View getDecorationEmpty() {
        return LayoutInflater.from(context).inflate(R.layout.message_decor_paddings, null, false);
    }

    private View getTextData(final Message item, EnhancedText text) {
        View root = LayoutInflater.from(context).inflate(R.layout.message_plain_text, null, false);

        TextView message = root.findViewById(R.id.message);

        // Set message text
        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onTextClick(view, item);
            }
        });

        root.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listener.onLongClick(view, item);
                return true;
            }
        });

        message.setLongClickable(false);

        markwon.setMarkdown(message, text.getMarkdowned());

        if (item.is(Message.FLAG_DELETED)) {
            message.setTextColor(COLOR_GREY);
        } else {
            message.setTextColor(COLOR_WHITE);
        }

        return root;
    }

    private View getNote(String noteText) {
        View v = LayoutInflater.from(context).inflate(R.layout.message_sysnote, null, false);

        TextView t = v.findViewById(R.id.text);

        t.setText(noteText);

        t.setLongClickable(true);
        v.setLongClickable(true);

        return v;
    }

    private View getDateDivider(Date date) {
        View v = LayoutInflater.from(context).inflate(R.layout.message_date, null, false);

        TextView t = v.findViewById(R.id.msg_header_date);
        t.setText(dateDividerSDF.format(date));

        t.setLongClickable(true);
        v.setLongClickable(true);

        return v;
    }

    private View getAttachment(MessagePartAttachment attachment) {
        View attachRoot;

        int maxSide = attachment.type == MessagePartAttachment.Type.STICKER ? stickerSide : imageSide;

        ShapeDrawable placeholder = null;
        float w = attachment.w;
        float h = attachment.h;
        if (w > 0 && h > 0) {

            float scaleCoeff = 1.0f;

            if(w > h && w > maxSide) scaleCoeff = maxSide / w;
            if(h > w && h > maxSide) scaleCoeff = maxSide / h;

            w *= scaleCoeff;
            h *= scaleCoeff;

            placeholder = new ShapeDrawable();
            placeholder.setIntrinsicWidth((int) w);
            placeholder.setIntrinsicHeight((int) h);
            placeholder.getPaint().setColor(Color.TRANSPARENT);
        } else {
            w = 1024;
            h = 512;
        }

        // Setup attachment for IMAGE
        if (attachment.type == MessagePartAttachment.Type.IMAGE || attachment.type == MessagePartAttachment.Type.STICKER) {
            attachRoot = LayoutInflater.from(context).inflate(R.layout.message_img, null, false);

            String url = null;
            if (attachment.actionUrl != null) url = attachment.actionUrl;
            if (attachment.thumbUrl != null) url = attachment.thumbUrl;

            if (url != null) {
                bindPicasso(formatProxiedUrl(attachment.thumbUrl), placeholder, attachRoot.findViewById(R.id.img), (int) w, (int) h);
            }

            bindDialogListener(attachRoot, attachment.actionUrl);

            // Setup attachment for Recursive Attachment
        } else if (attachment.type == MessagePartAttachment.Type.RECURSIVE) {
            attachRoot = LayoutInflater.from(context).inflate(R.layout.message_recursive, null, false);

            if (attachment.recursive != null) {
                LinearLayout r_root = attachRoot.findViewById(R.id.r_root);
                for (MessagePartAttachment a : attachment.recursive) {
                    r_root.addView(getAttachment(a));
                }
            }

            // Setup attachment for other view types
        } else {
            attachRoot = LayoutInflater.from(context).inflate(R.layout.message_embed_bigger, null, false);

            if (attachment.thumbUrl != null && !attachment.thumbUrl.isEmpty()) {
                bindPicasso(attachment.thumbUrl, placeholder, (ImageView) attachRoot.findViewById(R.id.thumbnail), (int) w, (int) h);
            } else {
                attachRoot.findViewById(R.id.thumbnail).setVisibility(View.GONE);
            }

            bindSimpleListener(attachRoot, attachment.actionUrl);
        }

        TextView title = attachRoot.findViewById(R.id.title);
        if(title != null) {
            if (attachment.title != null) title.setText(attachment.title);
            else title.setVisibility(View.GONE);
        }

        TextView descr = attachRoot.findViewById(R.id.descr);
        if(descr != null) {
            if (attachment.description != null && !attachment.description.trim().isEmpty())
                markwon.setMarkdown(descr, attachment.description);
            else descr.setVisibility(View.GONE);
        }

        return attachRoot;
    }

    private View getInline(final Message item, MessagePartInline inline) {
        View v = LayoutInflater.from(context).inflate(R.layout.message_inline, null, false);

        FlexboxLayout layout = v.findViewById(R.id.flex_ma_boi);

        for (final MessagePartInline.InlineButton i : inline.buttons) {
            TextView text = (TextView) LayoutInflater.from(context).inflate(R.layout.item_inline_btn, null, false);
            markwon.setMarkdown(text, i.text);
            if (i.isActive) text.setBackgroundResource(R.drawable.selector_inline_active);
            layout.addView(text);

            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onInlineClick(item, i.callbackData);
                    }
                }
            });
        }

        return v;
    }

    // TODO: move listener impl to activity
    private void bindSimpleListener(View v, final String actionUrl) {
        if (actionUrl != null) {

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    launchInCustomTab(actionUrl);
                }
            });

        }
    }

    // TODO: move listener impl to activity
    private void bindDialogListener(final View v, final String actionUrl) {
        if (actionUrl != null) {

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    launchInCustomTab(actionUrl);
                }
            });

            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    String[] items = {
                            "View",
                            "Save"
                    };

                    new AlertDialog.Builder(context)
                            .setItems(items, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Uri uri = Uri.parse(actionUrl);
                                    switch (i) {
                                        case 0:
                                            launchInCustomTab(actionUrl);
                                            break;
                                        case 1:
                                            try {
                                                JustAnotherUtils.launchAndroidDownloader(context, uri);
                                            } catch (Exception e) {
                                                Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                                                e.printStackTrace();
                                            }
                                            break;
                                        default:
                                            Toast.makeText(context, R.string.unimplemented, Toast.LENGTH_SHORT).show();
                                            break;
                                    }
                                }
                            })
                            .show();

                    return true;
                }
            });

        }
    }

    private void launchInCustomTab(String url) {

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Uri uri = Uri.parse(url);

        try {
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            builder.addDefaultShareMenuItem();
            builder.enableUrlBarHiding();
            builder.setExitAnimations(context, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            builder.setStartAnimations(context, android.R.anim.slide_in_left, android.R.anim.slide_out_right);

            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.launchUrl(context, uri);
        } catch (ActivityNotFoundException e) {

            try {
                context.startActivity(new Intent(Intent.ACTION_VIEW, uri));
            } catch (ActivityNotFoundException e1) {
                Toast.makeText(context, "Cannot pick app to view " + url, Toast.LENGTH_LONG).show();
                e1.printStackTrace();
            }
        }
    }

    private void bindPicasso(String url, Drawable placeholder, ImageView v, int w, int h) {
        if (url == null) return;
        if (url.isEmpty()) return;
        if (!MainActivity.FEATURE_IMAGE_LOADING) return;

        RequestBuilder<Drawable> rc = Glide.with(context).load(url);
        // TODO: we should use 'override' here
//        rc = rc.override(w, h);
//        rc = rc.apply(new RequestOptions().override(w, h));
        if (placeholder != null) rc = rc.placeholder(placeholder);
        rc.into(v);
    }

    private String formatProxiedUrl(String url) {
        if(url == null) return null;

        String decodedUrl = Base64.encodeToString(url.getBytes(), Base64.DEFAULT);

        if (customProxy != null && !customProxy.trim().isEmpty())
            return String.format(customProxy, decodedUrl, PREVIEW_SIDE);
        else
            return url;
    }

    // ==== //
    // DATA //
    // ==== //
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Message getItem(int position) {
        return data.get(position);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).mid.hashCode();
    }

    public int getPostition(Message item) {
        return data.indexOf(item);
    }

    public void setData(Message[] messages) {
        data.clear();
        data.addAll(Arrays.asList(messages));
        Collections.sort(data);
        notifyDataSetChanged();
    }

    public void addData(Message[] messages) {
        data.addAll(Arrays.asList(messages));
        Collections.sort(data);
        notifyDataSetChanged();
    }

    public synchronized void addOrUpdate(Message message) {
        if (message == null) return;

        boolean added = false;
        boolean changed = false;

        for (int i = 0; i < data.size(); i++) {
            if (message.equals(data.get(i))) {
                changed = data.get(i).replaceFrom(message);
                added = true;
            }
        }

        if (!added) {
            data.add(message);
            changed = true;
        }

        if (changed) {
            notifyDataSetChanged();
        }
    }

    public void markDeleted(String eid, String guildId, String channelId, String messageId) {
        boolean changed = false;

        for (Message msg : data) {
            if (msg.eid.equals(eid)
                    && msg.sid.equals(guildId)
                    && msg.cid.equals(channelId)
                    && msg.mid.equals(messageId)) {
                msg.flags = msg.flags | Message.FLAG_DELETED;
                changed = true;
            }
        }

        if (changed) {
            notifyDataSetChanged();
        }
    }

    public void updateAppearance(MessageMetaPatch[] aprcs) {

        boolean changed = false;

        for (Message cm : data) {
            for (MessageMetaPatch aprc : aprcs)
                if (cm.eid.equals(aprc.getEid())
                        && cm.sid.equals(aprc.getSid())
                        && ((aprc.getCid() == null) || cm.cid.equals(aprc.getCid()))
                        && cm.uid.equals(aprc.getUid())) {
                    cm.updateByAppearance(aprc.getName(), aprc.getIcon(), aprc.getColor());
                    changed = true;
                }
        }

        if (changed)
            notifyDataSetChanged();
    }

    public void updateReadState(String mid) {
        for (Message m : data) {
            if (m != null) {
                m.unset(Message.FLAG_UNREAD);
                if (m.mid.equals(mid)) {
                    break;
                }
            }
        }

        notifyDataSetChanged();
    }

    // =========== //
    // LIST SCROLL //
    // =========== //
    @SuppressLint("InflateParams")
    public void onBindView(ListView listView) {
        listView.setOnScrollListener(this);

        headerRoot = LayoutInflater.from(listView.getContext()).inflate(R.layout.item_message_header, null, false);

        listView.addHeaderView(headerRoot);
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        if (isTopLoaded && firstVisibleItem <= 1) {
            isTopLoaded = false;
            switchVisibility(headerRoot, true);
            listener.onScrolledToTop(getItem(0));
        }

    }

    private void switchVisibility(View root, boolean isLoading) {
        root.findViewById(R.id.loading).setVisibility(isLoading ? View.VISIBLE : View.GONE);
        root.findViewById(R.id.loading_done).setVisibility(isLoading ? View.GONE : View.VISIBLE);
    }

    public void setTopLoadingDone() {
        isTopLoaded = true;
        switchVisibility(headerRoot, false);
    }

    public void setCustomProxy(String url) {
        customProxy = url;
    }

    public interface EventsListener {

        void onTextClick(View v, Message m);

        void onLongClick(View v, Message m);

        void onUsernameClick(Message m);

        void onAvatarClick(Message m);

        void onInlineClick(Message m, String inlineData);

        void onScrolledToTop(Message message);
    }
}

package com.iillyyaa2033.ucclient.navigation;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.iillyyaa2033.ucclient.AccountsMgmtActivity;
import com.iillyyaa2033.ucclient.MainActivity;
import com.iillyyaa2033.ucclient.R;
import com.iillyyaa2033.ucclient.SettingsActivity;
import com.iillyyaa2033.ucclient.adapters.ChannelsViewAdapter;
import com.iillyyaa2033.ucclient.adapters.ServersViewAdapter;
import com.iillyyaa2033.ucclient.scheduler.SchedulerMgmtActivity;
import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.Server;
import com.iillyyaa2033.ucmodel.User;
import com.iillyyaa2033.ucserver.Adapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NavigationFragment extends Fragment {

    private View root;

    public Adapter adapter;
    public Server currentServer = null;
    private List<Server> servers = new ArrayList<>();
    private Channel currentChannel = null;
    private Channel[] channels = {};

    private ServersViewAdapter serversAdapter;
    private ChannelsViewAdapter channelsAdapter;

    public void setAdapter(Adapter adapter) {
        this.adapter = adapter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_navigation, container, false);

        setupAdapterView();
        setupServersList();
        setupChannelsList();
        setupMenu();

        serversAdapter.setData(servers);
        updateServerListVisibility();

        setCurrentServer(currentServer);
        if (currentServer != null) setChannels(currentServer.sid, channels);
        setCurrentChannel(currentChannel);

        return root;
    }

    private void setupAdapterView() {
        TextView account = root.findViewById(R.id.account);
        TextView adapter = root.findViewById(R.id.adapter);
        ImageView avatar = root.findViewById(R.id.avatar);

        if (this.adapter != null) {
            adapter.setText(this.adapter.getServiceName());
            asyncUsername(avatar, account, this.adapter);
        }
    }

    private void asyncUsername(final ImageView image, final TextView view, final Adapter adapter) {
        view.setText(R.string.loading);

        final Handler handler = new Handler();

        new Thread() {

            @Override
            public void run() {
                final User user = adapter != null ? adapter.getAccountUser() : null;
                final String username = user != null ? user.toString() : "Unknown";

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        view.setText(username);

                        if (user != null) Glide.with(getContext()).load(user.icon_res).into(image);
                    }
                });
            }
        }.start();
    }


    private void setupServersList() {
        serversAdapter = new ServersViewAdapter(getActivity());
        final RecyclerView serversList = root.findViewById(R.id.servers_list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        serversList.setLayoutManager(layoutManager);
        serversList.setAdapter(serversAdapter);

        serversAdapter.setListener(new ServersViewAdapter.OnItemClick() {
            @Override
            public void onClick(Server item) {
                ((MainActivity) getActivity()).selectServer(item);
            }

            @Override
            public void onLongClick(View v, Server item) {
                ((MainActivity) getActivity()).showServerMenu(v, item);
            }
        });
    }

    private void setupChannelsList() {
        channelsAdapter = new ChannelsViewAdapter(getActivity());
        ListView channelsList = root.findViewById(R.id.chats_list);
        channelsList.setAdapter(channelsAdapter);
        channelsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (channelsAdapter.getItem(position).type != Channel.TYPE_TEXT) {
                    ((MainActivity) getActivity()).onError(new Exception("This type of channel is unsupported for now"));
                    return;
                }

                ((MainActivity) getActivity()).selectChannel(channelsAdapter.getItem(position));
                ((MainActivity) getActivity()).closeLeftDrawer();
            }
        });
    }

    private void setupMenu() {
        root.findViewById(R.id.menu_btn).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                showAdapterMenu(view);
            }
        });
    }

    private void showAdapterMenu(View view) {
        PopupMenu popup = new PopupMenu(getContext(), view);
        Menu menu = popup.getMenu();

        if (currentServer != null) {
            Adapter adapter = MainActivity.server.getAdapter(currentServer.eid);
            if (adapter != null) {
                if (adapter.hasFeature(Adapter.Feature.JOIN_SERVER_INVITE)) {
                    menu.add(1, 1, 1, R.string.join_invite);
                }
            }
        }

        menu.add(2, 2, 2, "Manage accounts");
        menu.add(3, 3, 3, "Manage settings");
        menu.add(4, 4, 4, "Manage tasks");

        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case 1:
                        ((MainActivity) getActivity()).showInviteDialog(adapter);
                        break;
                    case 2:
                        startActivity(new Intent(getActivity(), AccountsMgmtActivity.class));
                        break;
                    case 3:
                        startActivity(new Intent(getActivity(), SettingsActivity.class));
                        break;
                    case 4:
                        startActivity(new Intent(getActivity(), SchedulerMgmtActivity.class));
                        break;
                }
                return true;
            }
        });
    }

    public String getEID() {
        return adapter.getEId();
    }

    void setCurrentServer(Server server) {
        if (server == null) return;

        this.currentServer = server;

        if (root != null)
            ((TextView) root.findViewById(R.id.server_title)).setText(server.name);

        if (serversAdapter != null) serversAdapter.setCurrentServer(server.sid);
    }

    void setServers(Server[] servers) {
        this.servers = Arrays.asList(servers);

        if (serversAdapter != null) serversAdapter.setData(servers);
        updateServerListVisibility();
    }

    void updateServerListVisibility() {
        if (root != null) {
            if (servers.size() < 2) root.findViewById(R.id.servers_list).setVisibility(View.GONE);
            else root.findViewById(R.id.servers_list).setVisibility(View.VISIBLE);
        }
    }

    void updateServers(ArrayList<Server> items) {
        if (serversAdapter != null) serversAdapter.updateServers(items);
        updateServerListVisibility();
    }

    void removeServers(ArrayList<Server> items) {
        serversAdapter.removeServers(items);
    }

    public void setChannels(String sid, Channel[] channels) {
        if (currentServer != null && currentServer.sid.equals(sid)) {
            this.channels = channels;
            if (channelsAdapter != null) channelsAdapter.setChannels(channels);
        }
    }

    public void updateChannels(List<Channel> channels) {
        if (currentServer != null) {
            ArrayList<Channel> result = new ArrayList<>();

            for (Channel channel : channels) {
                if (currentServer.sid.equals(channel.sid)) {
                    result.add(channel);
                }
            }

            if (channelsAdapter != null) channelsAdapter.update(result.toArray(new Channel[0]));
        }
    }

    public void setCurrentChannel(Channel channel) {
        if (channel == null) return;
        if (channelsAdapter != null) channelsAdapter.setCurrent(channel.cid);
    }
}

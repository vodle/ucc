package com.iillyyaa2033.ucclient.navigation;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.Server;
import com.iillyyaa2033.ucserver.Adapter;

import java.util.ArrayList;
import java.util.Collection;

public class NavigationPagerAdapter extends FragmentStatePagerAdapter {

    private NavigationFragment[] fragments = {};

    public Adapter currentAdapter = null;
    public Server currentServer = null;
    public Channel currentChannel = null;

    public NavigationPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public NavigationFragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    public void setItems(Collection<Adapter> adapters) {

        NavigationFragment[] frs = new NavigationFragment[adapters.size()];

        int i = 0;
        for(Adapter adapter : adapters){
            frs[i] = new NavigationFragment();
            frs[i].setAdapter(adapter);

            i++;
        }

        fragments = frs;
        notifyDataSetChanged();
    }

    public void updateServers(Server... servers) {
        for(NavigationFragment fragment : fragments){
            ArrayList<Server> items = new ArrayList<>();

            for(Server s : servers)
                if(fragment.adapter.getEId().equals(s.eid))
                    items.add(s);

            fragment.updateServers(items);
        }
    }

    public void setData(String eid, Server[] servers) {
        for(NavigationFragment fragment : fragments){
            if(fragment.adapter.getEId().equals(eid)){
                fragment.setServers(servers);
            }
        }
    }

    public void deleteServers(Server[] servers) {
        for(NavigationFragment fragment : fragments){
            ArrayList<Server> items = new ArrayList<>();

            for(Server s : servers)
                if(fragment.adapter.getEId().equals(s.eid))
                    items.add(s);

            fragment.removeServers(items);
        }
    }

    public void setChannels(String eid, String sid, Channel[] channels) {
        for(NavigationFragment fragment : fragments){
            if(fragment.adapter.getEId().equals(eid)){
                fragment.setChannels(sid, channels);
            }
        }
    }

    public void update(Channel[] channels) {
        for(NavigationFragment fragment : fragments){
            ArrayList<Channel> channelsForThisFragment = new ArrayList<>();

            for(Channel channel : channels){
                if(fragment.getEID().equals(channel.eid)){
                    channelsForThisFragment.add(channel);
                }
            }

            if(channelsForThisFragment.size() > 0){
                fragment.updateChannels(channelsForThisFragment);
            }
        }
    }

    public void setCurrentAdapter(int idx) {
        currentAdapter = fragments[idx].adapter;
        notifyDataSetChanged();
    }

    public void setCurrentServer(String eid, Server server) {
        for (NavigationFragment fragment : fragments) {
            if (eid.equals(fragment.adapter.getEId())) {
                fragment.setCurrentServer(server);
            }
        }
    }

    public void setCurrentChannel(Channel channel) {
        for (NavigationFragment fragment : fragments)
            if(channel.eid.equals(fragment.adapter.getEId()))
                fragment.setCurrentChannel(channel);
    }

    public Adapter getAdapter(String eid) {
        for (NavigationFragment fragment : fragments) {
            if (eid.equals(fragment.adapter.getEId())) {
                return fragment.adapter;
            }
        }

        return null;
    }

    public NavigationFragment getFragment(String eid){
        for (NavigationFragment fragment : fragments) {
            if (eid.equals(fragment.adapter.getEId())) {
                return fragment;
            }
        }

        return null;
    }
}

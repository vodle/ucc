package com.iillyyaa2033.ucclient;

public class GitlabIntegration {

    private static final String RAW_URL = "https://gitlab.com/%s/-/jobs/artifacts/%s/raw/%s?job=%s";
    private static final String REPO = "iillyyaa2033/ucc";
    private static final String BRANCH = "master";
    private static final String FOLDER = "AndroidApp/build/outputs/apk/release/";
    private static final String JOB = "build_android";

    public static String getLatestInfoUrl(){
        return String.format(RAW_URL, REPO, BRANCH, FOLDER+"output.json", JOB);
    }

    public static String getLatestApkUrl(){
        return String.format(RAW_URL, REPO, BRANCH, FOLDER+"AndroidApp-release.apk", JOB);
    }
}

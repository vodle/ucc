package com.iillyyaa2033.ucclient;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import static android.content.Context.DOWNLOAD_SERVICE;

public class JustAnotherUtils {

    public static void launchAndroidDownloader(Context context, Uri uri) throws Exception {

        DownloadManager.Request r = new DownloadManager.Request(uri);
        r.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, uri.getLastPathSegment());
        r.allowScanningByMediaScanner();
        r.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        DownloadManager dm = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
        if(dm != null) dm.enqueue(r);
        else throw new Exception("Cannot find DownloadManager");

    }
}

package com.iillyyaa2033.ucclient;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.iillyyaa2033.ucserver.AuthProc;

import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AuthProcActivity extends Activity {

    public static AuthProc target = null;
    private Handler handler;
    private HashMap<String, String> results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        results = new HashMap<>();
        handler = new Handler();
        recursiveAuthProc(target, 0);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private WebView setupWebView(WebViewClient wvc){
        CookieManager.getInstance().removeAllCookies(null);
        WebStorage.getInstance().deleteAllData();

        WebView view = new WebView(this);

        WebSettings settings = view.getSettings();
        settings.setAppCacheEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setDomStorageEnabled(true);
        settings.setSupportMultipleWindows(true);

        view.setWebViewClient(wvc);

        view.setWebChromeClient(new WebChromeClient(){

            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }

            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                return super.onConsoleMessage(consoleMessage);
            }
        });

        return view;
    }

    // ========= //
    // AUTH PROC //
    // ========= //
    private void recursiveAuthProc(AuthProc proc, int idx){
        AuthProc.AuthStep step = proc.getStep(idx);

        if(step != null){
            switch (step.name){
                case AuthProc.MessageStep.NAME:
                    apMessage(proc, idx);
                    break;
                case AuthProc.WebHeadersStep.NAME:
                    apWebHeaders(proc, idx);
                    break;
                case AuthProc.WebUrlStep.NAME:
                    apWebUrl(proc, idx);
                    break;
                case AuthProc.WebCookiesStep.NAME:
                    apWebCookies(proc, idx);
                    break;
                case AuthProc.AdapterSideStep.NAME:
                    adapterSide(proc, idx);
                    break;
                case AuthProc.FinalStep.NAME:
                    apEnd();
                    break;
                case AuthProc.FailStep.NAME:
                    Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show();
                    setResult(Activity.RESULT_CANCELED);
                    finish();
                    break;
            }
        } else {
            Toast.makeText(this, R.string.err_authproc_no_next_step, Toast.LENGTH_SHORT).show();
            setResult(Activity.RESULT_CANCELED);
            finish();
        }
    }

    private void apMessage(final AuthProc proc, final int idx){
        AuthProc.MessageStep step = (AuthProc.MessageStep) proc.getStep(idx);

        new AlertDialog.Builder(this)
            .setTitle(R.string.authproc_message)
            .setMessage(step.message)
            .setNegativeButton(android.R.string.cancel, new AlertDialog.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    setResult(Activity.RESULT_CANCELED);
                    finish();
                }
            })
            .setPositiveButton(R.string.authproc_onwards, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                recursiveAuthProc(proc, idx+1);
                }
            })
            .setCancelable(false)
            .show();
    }

    private void apWebHeaders(final AuthProc proc, final int idx){
        final AuthProc.WebHeadersStep step = (AuthProc.WebHeadersStep) proc.getStep(idx);

        String userAgent = "Mozilla/5.0 (Android 7.1.2; Mobile; rv:67.0) Gecko/67.0 Firefox/67.0";
        results.put("useragent", userAgent);

        WebView view = setupWebView(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(request.getUrl().toString(), request.getRequestHeaders());
                return true;
            }

            @Nullable
            @Override
            public WebResourceResponse shouldInterceptRequest(final WebView view, WebResourceRequest request) {

                if(request.getRequestHeaders() != null) {
                    for (String s : request.getRequestHeaders().keySet()) {

                        if(s.toLowerCase().equals(step.headerName.toLowerCase())){
                            String header = request.getRequestHeaders().get(s);
                            if(header == null || header.equals("undefined")) continue;
                            results.put(step.destination, header);

                            view.getHandler().post(new Runnable() {
                                @Override
                                public void run() {
                                    view.stopLoading();
                                    view.destroy();
                                    recursiveAuthProc(proc, idx+1);
                                }
                            });
                        }
                    }
                }

                return super.shouldInterceptRequest(view, request);
            }
        });
        view.getSettings().setUserAgentString(userAgent);

        if(step.color != null)
            view.setBackgroundColor(Color.parseColor(step.color));

        setContentView(view);

        view.loadUrl(step.startPage);
    }

    private void apWebUrl(final AuthProc proc, final int idx) {
        final AuthProc.WebUrlStep step = (AuthProc.WebUrlStep) proc.getStep(idx);

        WebView view = setupWebView(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if(inspectUrl(url)){
                    stop(view);
                } else {
                    view.loadUrl(url);
                }

                return true;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                if(inspectUrl(request.getUrl().toString())){
                    stop(view);
                } else {
                    view.loadUrl(request.getUrl().toString(), request.getRequestHeaders());
                }

                return true;
            }

            boolean inspectUrl(String url){
                if(url.startsWith(step.successUrl)){
                    results.put(step.destination, url);
                    return true;
                } else{
                    return false;
                }
            }

            void stop(WebView view){
                view.getHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        recursiveAuthProc(proc, idx+1);
                    }
                });
            }
        });

        setContentView(view);
        view.loadUrl(step.startUrl);
    }

    private void apWebCookies(final AuthProc proc, final int idx) {
        final AuthProc.WebCookiesStep step = (AuthProc.WebCookiesStep) proc.getStep(idx);

        WebView view = setupWebView(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

            if(request.getUrl().toString().startsWith(step.destUrl)){
                String cookie = CookieManager.getInstance().getCookie(request.getUrl().toString());

                results.put(step.destination, cookie);

                view.getHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        recursiveAuthProc(proc, idx+1);
                    }
                });

            }

            return super.shouldOverrideUrlLoading(view, request);
            }
        });

        setContentView(view);

        view.loadUrl(step.startUrl);
    }

    private void adapterSide(final AuthProc proc, final int idx){
        final AuthProc.AdapterSideStep step = (AuthProc.AdapterSideStep) proc.getStep(idx);

        new Thread(){
            @Override
            public void run() {
                // Do all steps
                while (step.step(new APUIC()));

                // Finish this proc
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        recursiveAuthProc(proc, idx+1);
                    }
                });
            }

        }.start();
    }

    private void apEnd(){
        Intent data = new Intent();
        for(String key : results.keySet()){
            data.putExtra(key, results.get(key));
        }
        setResult(Activity.RESULT_OK, data);
        finish();
    }

    class APUIC implements AuthProc.AdapterSideStep.IUICallback{

        private Lock lock = new ReentrantLock();
        private String msg;

        @Override
        public String getString(final String hint) {

            handler.post(new Runnable() {
                @Override
                public void run() {

                    lock.lock();

                    final EditText text = new EditText(AuthProcActivity.this);
                    text.setHint(hint);

                    new AlertDialog.Builder(AuthProcActivity.this)
                            .setView(text)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    msg = text.getText().toString();
                                    lock.unlock();
                                }
                            })
                            .show();
                }
            });

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            lock.lock();
            String m = msg;
            lock.unlock();

            return m;
        }

        @Override
        public void block(final String m) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(AuthProcActivity.this)
                            .setMessage(m)
                            .setCancelable(false)
                            .show();
                }
            });
        }

        @Override
        public void message(final String m) {

            handler.post(new Runnable() {
                @Override
                public void run() {
                    lock.lock();

                    new AlertDialog.Builder(AuthProcActivity.this)
                            .setMessage(m)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    lock.unlock();
                                }
                            })
                            .show();
                }
            });

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            lock.lock();
            lock.unlock();
        }

        @Override
        public void setResult(String key, String value) {
            results.put(key, value);
        }

    }
}

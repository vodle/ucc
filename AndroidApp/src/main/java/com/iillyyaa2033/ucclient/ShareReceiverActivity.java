package com.iillyyaa2033.ucclient;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.MimeTypeMap;

import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class ShareReceiverActivity extends Activity {

    public static String text = null;
    public static ArrayList<String> images = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sharereceiver);

        ContentResolver contentResolver = getContentResolver();
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {

            if ("text/plain".equals(type)) {
                String s = intent.getStringExtra(Intent.EXTRA_TEXT);
                if(text == null) text = s;
                else text+="\n"+s;
            } else if (type.startsWith("image/")) {
                Uri u = intent.getParcelableExtra(Intent.EXTRA_STREAM);
                File f = saveFromUri(u, contentResolver.getType(u));
                if(f != null) images.add(f.getAbsolutePath());
            }

        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {

            if (type.startsWith("image/")) {
                ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);

                for(Uri u : imageUris) {
                    File f = saveFromUri(u, contentResolver.getType(u));
                    if(f != null) images.add(f.getAbsolutePath());
                }
            }

        }

        startActivity(new Intent(this, MainActivity.class));
    }

    private File saveFromUri(Uri uri, String type) {

        String ext = MimeTypeMap.getSingleton().getExtensionFromMimeType(type);

        String outputfile = getCacheDir() + "/" + uri.getLastPathSegment() + "." + ext;

        try{
            InputStream in = getContentResolver().openInputStream(uri);
            File f = new File(outputfile);
            OutputStream outputStream = new FileOutputStream(f);

            byte[] buffer = new byte[1024];
            int length = 0;

            while((length = in.read(buffer)) > 0) {
                outputStream.write(buffer,0,length);
            }

            outputStream.close();
            in.close();

            return f;
        }catch (IOException e) {
            System.out.println("error in creating a file");
            e.printStackTrace();
        }

        return null;

    }


}

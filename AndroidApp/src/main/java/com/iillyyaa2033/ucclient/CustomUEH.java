package com.iillyyaa2033.ucclient;

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class CustomUEH implements Thread.UncaughtExceptionHandler {

    private static final boolean WRITE_ERROR = true;

    private Thread.UncaughtExceptionHandler defaultHandler = Thread.getDefaultUncaughtExceptionHandler();

    @Override
    public void uncaughtException(Thread thread, Throwable e) {

        e.printStackTrace(System.err);

        if(WRITE_ERROR) {
            StringBuilder output = new StringBuilder(e.toString() + "\n\n");

            output.append("--------- Stack trace ---------\n");
            for (StackTraceElement stackTraceElement : e.getStackTrace())
                output.append("\t").append(stackTraceElement).append("\n");

            output.append("\n");

            output.append("--------- Cause ---------\n");
            Throwable cause = e.getCause();
            if (cause != null) {
                output.append(cause.toString()).append("\n\n");

                for (StackTraceElement stackTraceElement : cause.getStackTrace())
                    output.append("\t").append(stackTraceElement).append("\n");
            }
            output.append("\n");

            try {
                File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "stacktrace.txt");
                FileOutputStream trace = new FileOutputStream(f);

                trace.write(output.toString().getBytes());
                trace.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        defaultHandler.uncaughtException(thread, e);
    }
}

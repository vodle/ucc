package ucc.telegram;

import com.iillyyaa2033.ucmodel.Channel;
import com.iillyyaa2033.ucmodel.ConnectionSettings;
import com.iillyyaa2033.ucmodel.EnhancedText;
import com.iillyyaa2033.ucmodel.Message;
import com.iillyyaa2033.ucmodel.MessagePartAttachment;
import com.iillyyaa2033.ucmodel.MessagePartText;
import com.iillyyaa2033.ucmodel.User;
import com.iillyyaa2033.ucserver.Adapter;
import com.iillyyaa2033.ucmodel.MessageMetaPatch;
import com.iillyyaa2033.utils.FileUtils;

import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

public class TgAdapter extends Adapter implements TgClient.L {

    private static final boolean DEBUG_UNHANDLED_EVENTS = false;

    private TgClient tgClient = new TgClient();
    private ArrayList<Long> sendUIDs = new ArrayList<>();

    private TdApi.User me = null;
    private User convertedMe = null;

    @Override
    public void onConnectionSettingsChanged(ConnectionSettings settings) {
        super.setSettings(settings);

        tgClient.setup(settings.getToken(), this);
    }

    @Override
    public void startRealtime() {
        callback().sendRealtimeStarting();
        tgClient.startRealtime();
        tgClient.waitRealtimeStarting();
        callback().sendRealtimeStarted();
    }

    @Override
    public boolean isRealtime() {
        return tgClient.isRealtime();
    }

    @Override
    public void stopRealtime() {
        tgClient.stopRealtime();
    }

    @Override
    public User getAccountUser() {

        if(convertedMe == null)
            convertedMe = updMe();

        return convertedMe;
    }

    private synchronized User updMe(){
        User result;

        boolean ownRT = !(tgClient != null && tgClient.isRealtime());

        TgClient client;
        if(ownRT) {
            client = new TgClient();
            client.setup(getSettings().getToken(), null);
            client.startRealtime();
            client.waitRealtimeStarting();
        } else {
            client = tgClient;
        }

        if(me == null) {
            try {
                me = (TdApi.User) client.apicall(new TdApi.GetMe());
            } catch (TgClient.RE re) {
                callback().sendError(re);
                me = null;
            }
        }

        result = convertUser(client, me);

        if(ownRT) {
            client.stopRealtime();
        }

        return result;
    }

    @Override
    public String getServiceName() {
        return "Telegram";
    }

    @Override
    public Channel[] getChannels(String serverId) {
        if(!isRealtime()) startRealtime();

        TdApi.Chat[] cs = new TdApi.Chat[0];
        try {
            cs = getChats();
        } catch (TgClient.RE re) {
            callback().sendError(re);
        }

        Channel[] rz = new Channel[cs.length];

        for(int i =0; i < cs.length; i++){
            rz[i] = convert(cs[i]);
        }

        return rz;
    }

    @Override
    public Message[] getLastMessages(String serverId, String cid) {

        ArrayList<TdApi.Message> messages;
        try {
            messages = getMessages(Long.parseLong(cid), 0, 50);
        } catch (TgClient.RE re) {
            callback().sendError(re);
            return new Message[0];
        }

        Message[] results = new Message[messages.size()];

        for(int i = 0; i < messages.size(); i++){
            TdApi.Message msg = messages.get(i);
            results[i] = convert(msg);

            if(msg.senderUserId != 0)  maybeSendAppearance(msg.senderUserId);
            else maybeSendChatAppearance(Long.parseLong(cid));
        }

        return results;
    }

    @Override
    public Message[] getMessagesBefore(String serverId, String channelId, String mid, long since, int count) {
        ArrayList<TdApi.Message> messages = null;
        try {
            messages = getMessages(Long.parseLong(channelId), Long.parseLong(mid), count);
        } catch (TgClient.RE re) {
            callback().sendError(re);
            return new Message[0];
        }

        Message[] results = new Message[messages.size()];

        for(int i = 0; i < messages.size(); i++){
            results[i] = convert(messages.get(i));
            maybeSendAppearance(messages.get(i).senderUserId);
        }

        return results;
    }

    @Override
    public Message[] getMessagesSince(String serverId, String channelId, long since, int count) {
        return new Message[0];
    }

    @Override
    public void sendMessage(String sid, String cid, EnhancedText mentionedText, String[] attachments) {

        long chat = Long.parseLong(cid);

        if(mentionedText != null && !mentionedText.text.trim().isEmpty()) {
            TdApi.InputMessageText text = new TdApi.InputMessageText(new TdApi.FormattedText(mentionedText.text, null), false, true);
            try {
                TdApi.SendMessageOptions opts = new TdApi.SendMessageOptions(false, false, null);
                tgClient.apicall(new TdApi.SendMessage(chat, 0, opts, null ,text));
            } catch (TgClient.RE re) {
                callback().sendError(re);
            }
        }

        if(attachments != null && attachments.length > 0){

            for(String path : attachments){
                File from = new File(path);
                File to = new File(TgClient.dataDirectory, System.currentTimeMillis()+".bin");
                try {
                    FileUtils.copyFileUsingStream(from, to);
                } catch (IOException e) {
                    callback().sendError(e);
                }

                TdApi.InputMessagePhoto file = new TdApi.InputMessagePhoto(new TdApi.InputFileLocal(to.getPath()), null, null, 0, 0, null, 0);
                try {
                    TdApi.SendMessageOptions opts = new TdApi.SendMessageOptions(false, false, null);
                    tgClient.apicall(new TdApi.SendMessage(chat, 0, opts, null, file));
                } catch (TgClient.RE re) {
                    callback().sendError(re);
                }
            }

        }
    }


    private ArrayList<TdApi.Message> getMessages(long chatId, long fromId, int count) throws TgClient.RE {

        ArrayList<TdApi.Message> rz = new ArrayList<>();
        int retriesLeft = count;

        while(retriesLeft > 0) {
            int limit = count - rz.size();
            TdApi.Messages messages = (TdApi.Messages) tgClient.apicall(
                    new TdApi.GetChatHistory(chatId, fromId, 0, limit, false));

            if(messages.messages.length == 0) break;

            rz.addAll(Arrays.asList(messages.messages));
            fromId = rz.get(rz.size() - 1).id;

            if(rz.size() == count) break;

            retriesLeft--;
        }

        return rz;
    }

    private TdApi.Chat[] getChats() throws TgClient.RE {
        TdApi.Chat[] results = {};

        Object object = tgClient.apicall(new TdApi.GetChats(null, Long.MAX_VALUE, 0, Integer.MAX_VALUE));
        if(object instanceof TdApi.Chats){
            long[] cs = ((TdApi.Chats) object).chatIds;

            TdApi.Chat[] rz = new TdApi.Chat[cs.length];
            for(int i = 0; i < rz.length; i++){
                Object o = tgClient.apicall(new TdApi.GetChat(cs[i]));

                if(o instanceof TdApi.Chat) rz[i] = (TdApi.Chat) o;
                else callback().sendError(new UnexpectedResultException(o));
            }

            results = rz;
        } else {
            callback().sendError(new UnexpectedResultException(object));
        }

        return results;
    }

    private void maybeSendAppearance(int uid) {
        if(!sendUIDs.contains((long) uid) && uid != 0){
            try {
                TdApi.User user = (TdApi.User) tgClient.apicall(new TdApi.GetUser(uid));
                callback().sendMessageMetaPatch(convertUA(user));
                sendUIDs.add((long) uid);
            } catch (TgClient.RE re) {
                re.printStackTrace();
            }
        }
    }

    private void maybeSendChatAppearance(long cid) {
        // I just believe that users have positive ids
        // but group - negative

        if(!sendUIDs.contains(cid) && cid != 0){
            try {
                TdApi.Chat user = (TdApi.Chat) tgClient.apicall(new TdApi.GetChat(cid));
                callback().sendMessageMetaPatch(convertUA(user));
                sendUIDs.add(cid);
            } catch (TgClient.RE e){
                callback().sendError(e);
            }
        }
    }

    private Message convert(final TdApi.Message src){
        TgClient.OnResult fileDownloadedCallback = new TgClient.OnResult() {

            @Override
            public void onResult(Object result) {
                if(result instanceof TdApi.File) {
                    if(((TdApi.File) result).local.isDownloadingCompleted) {
                        callback().sendMessageUpdate(convert(src));
                    }
                }
            }
        };

        Message message = new Message(getEId(), SERVER_ID, ""+src.chatId, ""+src.id, ""+src.senderUserId,
                ((long) src.date) * 1000, null);

        if(src.senderUserId == 0) message.uid = ""+src.chatId;

        switch (src.content.getConstructor()){
            case TdApi.MessageText.CONSTRUCTOR:
                message.append(new MessagePartText(((TdApi.MessageText) src.content).text.text));
                break;
            case TdApi.MessagePhoto.CONSTRUCTOR:
                MessagePartAttachment photoAttach = new MessagePartAttachment();

                TdApi.MessagePhoto messagePhoto = ((TdApi.MessagePhoto) src.content);

                TdApi.PhotoSize size = messagePhoto.photo.sizes[messagePhoto.photo.sizes.length-1];

                photoAttach.type = MessagePartAttachment.Type.IMAGE;
                photoAttach.h = size.height;
                photoAttach.w = size.width;
                photoAttach.thumbUrl = downloadFileIfNone(size.photo, fileDownloadedCallback);

                message.append(photoAttach);
                break;
            case TdApi.MessageSticker.CONSTRUCTOR:
                MessagePartAttachment stickerAttach = new MessagePartAttachment();

                TdApi.Sticker messageSticker = ((TdApi.MessageSticker) src.content).sticker;

                stickerAttach.type = MessagePartAttachment.Type.STICKER;
                stickerAttach.h = messageSticker.height;
                stickerAttach.w = messageSticker.width;
                stickerAttach.thumbUrl = downloadFileIfNone(messageSticker.sticker, fileDownloadedCallback);

                message.append(stickerAttach);
                break;
            default:
                message.append(new MessagePartText(("```Unimplemented yet```")));
        }

        return message;
    }

    private Channel convert(final TdApi.Chat from){

        Channel channel = new Channel(getEId(), SERVER_ID, ""+from.id,
                from.title, null, (from.lastMessage.date * 1000L));

        channel.last_msg = convert(from.lastMessage).getRawText();
        channel.priority = -from.lastMessage.date;
        channel.draw_mode = Channel.ChannelDrawMode.DEFAULT;

        if(from.photo != null) {
            channel.icon = downloadFileIfNone(from.photo.small, null);
        }

        return channel;
    }

    private User convertUser(TgClient client, TdApi.User user){
        if(user == null) return null;

        String path = null;

        if(user.profilePhoto != null){
            try {
                path = client.downloadFileIfNone(user.profilePhoto.small, null);
            } catch (TgClient.RE re) {
                callback().sendError(re);
            }
        }

        return new User(getEId(), ""+user.id, user.firstName + " "+user.lastName, path);
    }

    private MessageMetaPatch convertUA(TdApi.User user){

        MessageMetaPatch aprc = new MessageMetaPatch(getEId(), SERVER_ID, null, ""+user.id,
                user.firstName+" "+user.lastName, null, 0);

        if(user.profilePhoto != null) {
            aprc.setIcon(downloadFileIfNone(user.profilePhoto.small, null));
        }

        return aprc;
    }

    private MessageMetaPatch convertUA(TdApi.Chat chat){

        MessageMetaPatch aprc = new MessageMetaPatch(getEId(), SERVER_ID, null, ""+chat.id,
                chat.title, null, 0);

        if(chat.photo != null){
            aprc.setIcon(downloadFileIfNone(chat.photo.small, null));
        }

        return aprc;
    }

    private String downloadFileIfNone(TdApi.File file, TgClient.OnResult whenDownloaded){
        try {
            return tgClient.downloadFileIfNone(file, whenDownloaded);
        } catch (TgClient.RE re) {
            callback().sendError(re);
            return null;
        }
    }

    @Override
    public void event(TdApi.Object object) {
        switch (object.getConstructor()){
            case TdApi.UpdateNewChat.CONSTRUCTOR:
                handleChatUpdate(((TdApi.UpdateNewChat) object).chat);
                break;
            case TdApi.UpdateUser.CONSTRUCTOR:
                handleUserUpdate((TdApi.UpdateUser) object);
                break;
            case TdApi.Error.CONSTRUCTOR:
                TdApi.Error currentError = (TdApi.Error) object;
                callback().sendError(new Exception(currentError.code+" "+currentError.message));
                break;
            default:
                boolean handled = handleAnnotated(object);
                if(!handled && DEBUG_UNHANDLED_EVENTS) System.out.println("Unhandled:\n"+object);
        }
    }

    private void handleChatUpdate(final TdApi.Chat chat){

        String lm = "";
        long lmd = 0;

        if(chat.lastMessage != null){
            lm = chat.lastMessage.content.toString();
            lmd = chat.lastMessage.date;
        }

        Channel channel = new Channel(getEId(), SERVER_ID, ""+chat.id, chat.title, lm, lmd);
        channel.draw_mode = Channel.ChannelDrawMode.DEFAULT;

        String icon = null;
        if(chat.photo != null && chat.photo.small != null) {
            icon = downloadFileIfNone(chat.photo.small, null);
        }

        if(icon != null && !icon.isEmpty()) {
            channel.icon = icon;
        }

        callback().sendChannelUpdate(channel);
    }

    private void handleUserUpdate(final TdApi.UpdateUser userUpdate){

        TdApi.User tdu = userUpdate.user;

        User user = new User(getEId(), tdu.id+"", tdu.username, null);

        if(user.username.isEmpty()){
            user.username = tdu.id+"";
        }

        String icon = null;
        if(tdu.profilePhoto != null && tdu.profilePhoto.small != null) {
            icon = downloadFileIfNone(tdu.profilePhoto.small, null);
        }

        if(icon != null && !icon.isEmpty()) {
            user.icon_res = icon;
        }

        callback().sendUserUpdated(user);
    }

    private boolean handleAnnotated(TdApi.Object object){
        try {
            Method m = getClass().getDeclaredMethod("specialHandleMethod", object.getClass());
            m.invoke(this, object);
            return true;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            return false;
        }
    }

    private void specialHandleMethod(TdApi.UpdateOption s){}

    private void specialHandleMethod(TdApi.UpdateConnectionState state){
        if(state.state.getConstructor() == TdApi.ConnectionStateReady.CONSTRUCTOR)
            callback().sendRealtimeStarted();
        else
            callback().sendRealtimeStarting();
    }

    private void specialHandleMethod(TdApi.UpdateHavePendingNotifications s){}

    private void specialHandleMethod(TdApi.UpdateSupergroup s){}

    private void specialHandleMethod(TdApi.UpdateBasicGroup s){}

    private void specialHandleMethod(TdApi.UpdateChatOrder order){}

    private void specialHandleMethod(TdApi.UpdateChatLastMessage message){
        // TODO: implement me
    }

    private void specialHandleMethod(TdApi.UpdateNewMessage update){
        callback().sendMessageUpdate(convert(update.message));
    }

    private void specialHandleMethod(TdApi.UpdateMessageEdited message){}

    private void specialHandleMethod(TdApi.UpdateFile s){}

    @Override
    public void error(Throwable e) {
        callback().sendError(e);
    }

}

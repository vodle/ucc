package ucc.telegram;

import com.iillyyaa2033.ucmodel.ConnectionSettings;
import com.iillyyaa2033.ucserver.Adapter;
import com.iillyyaa2033.ucserver.AuthProc;
import com.iillyyaa2033.ucserver.Authenticator;
import com.iillyyaa2033.ucserver.Module;

import java.util.Map;

public class TgModule extends Module {

    @Override
    public Descr getDescription() {
        return new Descr("ucc.telegram.TgAdapter","Telegram","Status: WIP");
    }

    @Override
    public Authenticator getAuthenticator() {
        return new Authenticator() {
            @Override
            public AuthProc getAuthProc() {
                return new AuthProc(
                        new MyAthStep(),
                        new AuthProc.FinalStep()
                );
            }

            @Override
            public ConnectionSettings finishAuthProc(Map<String, String> results) {
                if(results.containsKey("time")){
                    return new ConnectionSettings(
                            "ucc.telegram.TgAdapter",
                            null,
                            new String[]{"Mozilla", TgClient.SYSTEM, "custom", "custom"},
                            results.get("time")
                    );
                } else {
                    return null;
                }

            }
        };
    }

    @Override
    public Adapter getAdapter() {
        return new TgAdapter();
    }

    @Override
    public void loadNatives(String path) {
        System.load(path+"/libtdjni.so");
    }
}

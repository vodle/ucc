package ucc.telegram;

public class UnexpectedResultException extends Exception {

    public UnexpectedResultException(Object result){
        super(result.toString());
    }
}

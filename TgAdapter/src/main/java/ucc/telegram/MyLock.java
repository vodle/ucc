package ucc.telegram;

class MyLock {

    private volatile boolean isLocked = false;

    synchronized void lock() {
        while (isLocked) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        isLocked = true;
    }

    synchronized void lockIfUnlocked(){
        isLocked = true;
    }

    synchronized void waitUnlock() {
        while (isLocked) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    synchronized void unlock() {
        isLocked = false;
        notify();
    }
}

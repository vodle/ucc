package ucc.telegram;

public class Log {

    public static void d(String dltd, String stop_tdLib_thread) {
        System.out.println(dltd+": "+stop_tdLib_thread);
    }

    public static void e(String dltd, String s) {
        d(dltd, s);
    }

    public static void w(String dltd, String tdjni_loaded) {
        d(dltd, tdjni_loaded);
    }

    public static void w(String dltd, String s, UnsatisfiedLinkError e) {
        d(dltd, s);
        e.printStackTrace();
    }
}

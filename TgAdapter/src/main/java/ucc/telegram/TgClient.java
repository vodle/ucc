package ucc.telegram;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.io.IOError;
import java.io.IOException;

public class TgClient implements Client.ResultHandler, Client.ExceptionHandler {

    private static final String BROWSER = "Desktop";
    static final String SYSTEM = "Unknown";

    static String dataDirectory;

    static {

        // First, we're detecting OS
        if(System.getProperty("java.runtime.name").toLowerCase().contains("android")){
            // We're definitely running on Android
            dataDirectory = "/data"+"/data/"+"com.iillyyaa2033.ucc/files/tdlib/";
        } else {
            throw new RuntimeException("Telegram running on android only (for now)\nE.g. IMPLEMENT ME");
        }


        // Next, we're setting up log level
        Client.execute(new TdApi.SetLogVerbosityLevel(1));

        File f = new File(dataDirectory,"tdlib.log");
        if(!f.exists()) f.getParentFile().mkdirs();

        if (Client.execute(new TdApi.SetLogStream(new TdApi.LogStreamFile(f.getAbsolutePath(), 1 << 27))) instanceof TdApi.Error) {
            throw new IOError(new IOException("Write access to the current directory is required"));
        }
    }

    static TdApi.TdlibParameters getClientParameters(String profile){
        TdApi.TdlibParameters parameters = new TdApi.TdlibParameters();
        parameters.databaseDirectory = dataDirectory +profile;
        parameters.useMessageDatabase = true;
        parameters.useSecretChats = true;
        parameters.apiId = 784541;
        parameters.apiHash = "d89d60db26f881b3b3e7263533bd1f80";
        parameters.systemLanguageCode = "en";
        parameters.deviceModel = BROWSER;
        parameters.systemVersion = SYSTEM;
        parameters.applicationVersion = "1.0";
        parameters.enableStorageOptimizer = true;
        parameters.useTestDc = false;

        return parameters;
    }



    private Client client;
    private String token;
    private L l;

    private MyLock authDoneLock = new MyLock();

    void setup(String token, L l){
        this.l = l;
        this.token = token;
    }

    void startRealtime(){
        authDoneLock.lockIfUnlocked();

        if(client != null) client.close();
        client = Client.create(this, this, null);
    }

    void waitRealtimeStarting(){
        authDoneLock.waitUnlock();
    }

    boolean isRealtime(){
        return client != null;
    }

    void stopRealtime(){
        if(client != null){
            client.close();
            client = null;
        }
    }

    /**
     * Blocking API call
     *
     * @param function -
     * @param <V> -
     * @return -
     */
     <V extends TdApi.Function> Object apicall(V function) throws RE {
        if(client == null) throw new RE("client is not initialized");
        if(function == null) throw new RE("function required");

        final Object[] result = new Object[1];

        final MyLock lock = new MyLock();
        lock.lock();

        final Throwable[] caughtException = {null};

        client.send(function, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.Object object) {
                result[0] = object;
                lock.unlock();
            }
        }, new Client.ExceptionHandler() {
            @Override
            public void onException(Throwable e) {
                caughtException[0] = e;
                lock.unlock();
            }
        });

        lock.waitUnlock();

        if(caughtException[0] != null){
            throw new RuntimeException("Something went wrong", caughtException[0]);
        }  else if(result[0] instanceof TdApi.Error) {
            throw new RE("Got an error "+result[0]);
        }

        return result[0];
    }

    <V extends TdApi.Function> void async(V function, final OnResult l){
        client.send(function, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.Object object) {
                if(l != null) l.onResult(object);
            }
        }, new Client.ExceptionHandler() {
            @Override
            public void onException(Throwable e) {
                e.printStackTrace();
                // TODO: consider handling this
            }
        });
    }

    String downloadFileIfNone(TdApi.File smol, TgClient.OnResult whenDownloaded) throws RE {

        if(!smol.local.isDownloadingCompleted) {

            if(!smol.local.canBeDownloaded) return null;
            if(smol.local.isDownloadingActive) return "file://"+smol.local.path;

            TdApi.Function fcn = new TdApi.DownloadFile(smol.id, 30, 0, 0, true);

/*            if(whenDownloaded != null) {
                System.out.println("Async");
                async(fcn, whenDownloaded);
                System.out.println("Async sent");
                return null; // File is loading now
            } else { */
                TdApi.File f = (TdApi.File) apicall(fcn);
                return "file://"+f.local.path;
//            }
        } else {
            return "file://"+smol.local.path;
        }
    }

    @Override
    public void onResult(TdApi.Object object) {
        if (object.getConstructor() == TdApi.UpdateAuthorizationState.CONSTRUCTOR) { // Authentication
            switch (((TdApi.UpdateAuthorizationState) object).authorizationState.getConstructor()) {
                case TdApi.AuthorizationStateWaitTdlibParameters.CONSTRUCTOR:
                    TdApi.TdlibParameters params = getClientParameters(token);
                    client.send(new TdApi.SetTdlibParameters(params), this);
                    break;
                case TdApi.AuthorizationStateWaitEncryptionKey.CONSTRUCTOR:
                    client.send(new TdApi.CheckDatabaseEncryptionKey(), this);
                    break;

                case TdApi.AuthorizationStateWaitPhoneNumber.CONSTRUCTOR:
                case TdApi.AuthorizationStateWaitCode.CONSTRUCTOR:
                case TdApi.AuthorizationStateWaitPassword.CONSTRUCTOR:
                    // TODO: send ADAPTER BROKEN event
                    onException(new Exception("Telegram: REAUTH NEEDED"));
                    authDoneLock.unlock();
                    break;
                case TdApi.AuthorizationStateClosing.CONSTRUCTOR:
                case TdApi.AuthorizationStateClosed.CONSTRUCTOR:
                case TdApi.AuthorizationStateReady.CONSTRUCTOR:
                    authDoneLock.unlock();
                    break;
            }
        } else { // Other methods
            if(l != null) l.event(object);
        }
    }

    @Override
    public void onException(Throwable e) {
        if(l != null) l.error(e);
    }

    interface L {

        void event(TdApi.Object object);
        void error(Throwable e);

    }

    interface OnResult {

        void onResult(Object result);

    }

    class RE extends Exception {

        RE(String s){
            super(s);
        }

    }
}

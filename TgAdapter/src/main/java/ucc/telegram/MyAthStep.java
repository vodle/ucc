package ucc.telegram;

import com.iillyyaa2033.ucserver.AuthProc;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;

public class MyAthStep extends AuthProc.AdapterSideStep implements Client.ExceptionHandler, Client.ResultHandler {

    private TdApi.AuthorizationState authorizationState;
//        private boolean thisAuthProcessed = false;
//        private TdApi.UpdateConnectionState connectionState;
    private TdApi.Error currentError = null;

    private final long time;
    private Client client;

    private MyLock lock = new MyLock();

    MyAthStep(){
        time = System.currentTimeMillis();

        lock.lock();

        client = Client.create(this, this,null);
        client.send(
                new TdApi.AddProxy("0x03.tmp.teleproxy.me", 443, true,
                        new TdApi.ProxyTypeMtproto("ddec0de00000000000000000000badcafe")),
                this);
    }

    @Override
    public boolean step(IUICallback callback) {

        lock.lockIfUnlocked();
        System.out.println("Step");

        if(currentError != null) {
            callback.message(currentError.message);
            return false;
        } if(authorizationState != null
//                    && !thisAuthProcessed
//                    && connectionState != null
//                    && connectionState.state instanceof TdApi.ConnectionStateReady
        ){
//                thisAuthProcessed = true;
            switch (authorizationState.getConstructor()){
                case TdApi.AuthorizationStateWaitPhoneNumber.CONSTRUCTOR:
                    String phoneNumber = callback.getString("Please enter phone number: ");
                    TdApi.PhoneNumberAuthenticationSettings settings =
                            new TdApi.PhoneNumberAuthenticationSettings(false, false, false);
                    client.send(new TdApi.SetAuthenticationPhoneNumber(phoneNumber, settings), this);
                    lock.lockIfUnlocked();
                    System.out.println("Phone sent");
                    lock.waitUnlock();
                    return true;
                case TdApi.AuthorizationStateWaitCode.CONSTRUCTOR:
                    String code = callback.getString("Please enter authentication code: ");
                    client.send(new TdApi.CheckAuthenticationCode(code), this);
                    lock.lockIfUnlocked();
                    System.out.println("Code sent");
                    lock.waitUnlock();
                    return true;
                case TdApi.AuthorizationStateWaitPassword.CONSTRUCTOR:
                    String password = callback.getString("Please enter password: ");
                    client.send(new TdApi.CheckAuthenticationPassword(password), this);
                    lock.lockIfUnlocked();
                    lock.waitUnlock();
                    return true;
                case TdApi.AuthorizationStateReady.CONSTRUCTOR:
                    callback.setResult("time", ""+time);
                    client.close();
                    return false;
            }
        }

        callback.block("Waiting...");
        lock.waitUnlock();
        return true;
    }

    @Override
    public void onResult(TdApi.Object object) {

        System.out.println(object);

        if(object instanceof TdApi.UpdateAuthorizationState) {
            TdApi.UpdateAuthorizationState state = (TdApi.UpdateAuthorizationState) object;
            authorizationState = state.authorizationState;
//            System.out.println("Auth state: "+authorizationState.getClass().getSimpleName());
//                thisAuthProcesse

            switch (state.authorizationState.getConstructor()) {
                case TdApi.AuthorizationStateWaitTdlibParameters.CONSTRUCTOR:
                    TdApi.TdlibParameters params = TgClient.getClientParameters(""+time);
                    client.send(new TdApi.SetTdlibParameters(params), this);
                    break;
                case TdApi.AuthorizationStateWaitEncryptionKey.CONSTRUCTOR:
                    client.send(new TdApi.CheckDatabaseEncryptionKey(), this);
                    break;
                case TdApi.AuthorizationStateWaitPhoneNumber.CONSTRUCTOR:
                case TdApi.AuthorizationStateWaitCode.CONSTRUCTOR:
                case TdApi.AuthorizationStateWaitPassword.CONSTRUCTOR:
                case TdApi.AuthorizationStateReady.CONSTRUCTOR:
                    lock.unlock();
                    break;
                case TdApi.AuthorizationStateClosing.CONSTRUCTOR:
                case TdApi.AuthorizationStateClosed.CONSTRUCTOR:
                    // Ignore
                    break;
                default:
                    System.out.println("Unhandled: "+state);
            }
        } else if(object instanceof TdApi.Error){
            currentError = (TdApi.Error) object;
            lock.unlock();
        } else if(object instanceof TdApi.UpdateConnectionState){
//                connectionState = (TdApi.UpdateConnectionState) object;
//                System.out.println("Connection state: "+connectionState.state.getClass().getName());
//                lock.unlock();
        } else {
            if(!(object instanceof TdApi.UpdateOption
                    || object instanceof TdApi.Ok)) {
                System.out.println("Unhandled: "+object);
            }
        }
    }

    @Override
    public void onException(Throwable e) {
        e.printStackTrace();
    }
}